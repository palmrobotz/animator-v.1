TEMPLATE = app
TARGET = Animator
DEPENDPATH += . src
INCLUDEPATH += . src

# Separate destination folder for debug and release build
CONFIG(debug, debug|release) {
	DESTDIR = build/debug
} else {
	DESTDIR = build/release
	DEFINES += QT_NO_DEBUG_OUTPUT
}

# Store intermediate file hidden in the destination directory
OBJECTS_DIR = $${DESTDIR}/.obj
MOC_DIR     = $${DESTDIR}/.moc
RCC_DIR     = $${DESTDIR}/.qrc
UI_DIR      = $${DESTDIR}/.ui

# Core header file
HEADERS += src/animator.h \
           src/core/animateproperty.h \
           src/core/animationscene.h \
           src/core/svg.h \
           src/core/timeline.h \
           src/core/animationitem.h \
    src/core/timelineoperation.h

# UI header file
HEADERS += \
           src/ui/colormixer.h \
           src/ui/graphicsitemlibwidget.h \
           src/ui/infowidget.h \
           src/ui/mainwindow.h \
           src/ui/mygraphicsview.h \
           src/ui/mypreviewarea.h \
           src/ui/strokewidget.h \
           src/ui/swatchcolor.h \
           src/ui/twincolorbox.h \
           src/ui/uioperation.h \
           src/ui/timelineui.h

# Graphic utilities header file
HEADERS += src/graphicsutil/fitcurve.h \
           src/graphicsutil/bezierpoint.h

# Graphic item header file
HEADERS += src/graphicsitem/brushGraphicsItem.h \
           src/graphicsitem/notifier.h \
           src/graphicsitem/lineGraphicsItem.h \
           src/graphicsitem/myabstractgraphicsobject.h \
           src/graphicsitem/mygraphicsobject.h \
           src/graphicsitem/graphicsobjectbuilder.h \
           src/graphicsitem/mybeziergraphicsobject.h \
           src/graphicsitem/rectgraphicsobjectbuilder.h \
           src/graphicsitem/trianglegraphicsobjectbuilder.h \
           src/graphicsitem/ellipsegraphicsobjectbuilder.h \
           src/graphicsitem/groupgraphicsitem.h

# Core source file
SOURCES += src/main.cpp \
           src/core/animateproperty.cpp \
           src/core/animationscene.cpp \
           src/core/svg.cpp \
           src/core/timeline.cpp \
           src/core/animationitem.cpp \
    src/core/timelineoperation.cpp

# UI source file
SOURCES += \
           src/ui/colormixer.cpp \
           src/ui/graphicsitemlibwidget.cpp \
           src/ui/infowidget.cpp \
           src/ui/mainwindow.cpp \
           src/ui/mygraphicsview.cpp \
           src/ui/mypreviewarea.cpp \
           src/ui/strokewidget.cpp \
           src/ui/swatchcolor.cpp \
           src/ui/twincolorbox.cpp \
           src/ui/uioperation.cpp \
           src/ui/timelineui.cpp

# Graphic utilities source file
SOURCES += src/graphicsutil/fitcurve.cpp \
           src/graphicsutil/bezierpoint.cpp

# Graphic item source file
SOURCES += src/graphicsitem/brushGraphicsItem.cpp \
           \
           src/graphicsitem/lineGraphicsItem.cpp \
           src/graphicsitem/myabstractgraphicsobject.cpp \
           src/graphicsitem/mygraphicsobject.cpp \
           src/graphicsitem/mybeziergraphicsobject.cpp \
           src/graphicsitem/rectgraphicsobjectbuilder.cpp \
           src/graphicsitem/ellipsegraphicsobjectbuilder.cpp \
           src/graphicsitem/trianglegraphicsobjectbuilder.cpp \
           src/graphicsitem/groupgraphicsitem.cpp

RESOURCES += \
           SwatchDB.qrc \
           Picture.qrc

