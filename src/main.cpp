#include <QApplication>

#include "ui/mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyleSheet(QString::fromUtf8(" QTabWidget::pane { \n"
                                      "     border-top: 2px solid #C2C7CB;\n"
                                      " }\n"
                                      "\n"
                                      " QTabWidget::tab-bar {\n"
                                      "     left: 5px; \n"
                                      " }\n"
                                      "\n"
                                      " QTabBar::tab {\n"
                                      "     background: rgb(100, 100, 100);\n"
                                      "     border: 2px solid #C4C4C3;\n"
                                      "     border-bottom-color: #C2C7CB;\n"
                                      "     border-top-left-radius: 4px;\n"
                                      "     border-top-right-radius: 4px;\n"
                                      "     min-width: 8ex;\n"
                                      "     padding: 2px;\n"
                                      "     color: rgb(160,154,158)\n"
                                      " }\n"
                                      "\n"
                                      " QTabBar::tab:selected, QTabBar::tab:hover {\n"
                                      "     background:transparent;\n"
                                      " }\n"
                                      "\n"
                                      " QTabBar::tab:selected {\n"
                                      "     background:transparent;\n"
                                      "     border-color: #9B9B9B;\n"
                                      "     border-bottom-color: #C2C7CB; \n"
                                      "     "
                                      "     color: white"
                                      " }\n"
                                      "\n"
                                      " QTabBar::tab:!selected {\n"
                                      "     margin-top: 2px;\n"
                                      " }"));

    // Create UI and show it on the screen
    MainWindow mainWindow;
    mainWindow.show();

    return a.exec();
}
