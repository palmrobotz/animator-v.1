#include <QDebug>
#include "groupgraphicsitem.h"

GroupGraphicsItem::GroupGraphicsItem(QGraphicsItem* parent)
    : MyAbstractGraphicsObject(parent)
{
    
}

GroupGraphicsItem* GroupGraphicsItem::clone()
{
    //qDebug() << "In Clone child : " << childItems().size();
    return new GroupGraphicsItem(*this);
}

QRectF GroupGraphicsItem::boundingRect() const
{
    QRectF bound;
    
    foreach (QGraphicsItem* item, childItems())
    {
        bound = bound.united(item->boundingRect());
    }
    
    return bound;
}

void GroupGraphicsItem::paint(QPainter * /*painter*/, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
}