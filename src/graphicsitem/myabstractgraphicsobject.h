#ifndef MYABSTRACTGRAPHICSOBJECT_H
#define MYABSTRACTGRAPHICSOBJECT_H

#include <QGraphicsObject>
#include <QPointer>
#include <QPointF>
#include <QBrush>
#include <QRectF>
#include <QPen>
#include <QList>

#include "../graphicsutil/bezierpoint.h"

class MyAbstractGraphicsObject : public QGraphicsObject
{
    Q_OBJECT
    
public:
    explicit MyAbstractGraphicsObject(QGraphicsItem* parent = 0, const QBrush& brush=QBrush(), const QPen& pen=QPen());
    
    virtual MyAbstractGraphicsObject* clone() = 0;

    enum ItemAnchorPosition
    {
        TOP_LEFT, TOP_CENTER, TOP_RIGHT,
        CENTER_LEFT, CENTER, CENTER_RIGHT,
        BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
    };
    
    virtual QRectF boundingRect() const = 0;
    virtual void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0) = 0;
    
    void lock();
    void unlock();
    
    const QString& itemName() const;
    void setItemName(const QString& name);
    
    ItemAnchorPosition itemAnchorPosition() const;
    void setItemAnchorPostion(ItemAnchorPosition pos);
    
    QBrush brush() const;
    void setBrush(const QBrush& brush);
    
    QPen pen() const;
    void setPen(const QPen& pen);
    
    QPen selectionPen() const;
    QBrush selectionBrush() const;
    
    QRectF rect() const;
    void setRect(const QRectF& rectangle);

protected:
    MyAbstractGraphicsObject(const MyAbstractGraphicsObject& item);
    
    QVariant itemChange(GraphicsItemChange change, const QVariant& value);

private:
    bool mCreating;
    
    QString mItemName;
    ItemAnchorPosition mItemAnchorPos;
    
    QPen mPen;
    QBrush mBrush;
    
    QPen mSelectionPen;
    QBrush mSelectionBrush;
    
    QRectF mRectSize;
};

#include "notifier.h"

typedef QPointer<Notifier> NotifierPointer;
Q_DECLARE_METATYPE(NotifierPointer);

#endif // MYABSTRACTGRAPHICSOBJECT_H
