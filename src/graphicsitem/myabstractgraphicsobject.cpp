#include <QtGui>

#include "myabstractgraphicsobject.h"

MyAbstractGraphicsObject::MyAbstractGraphicsObject(QGraphicsItem *parent, const QBrush& brush, const QPen& pen)
    : QGraphicsObject(parent)
    , mCreating(true)
    , mItemAnchorPos(TOP_LEFT)
    , mPen(pen)
    , mBrush(brush)
    , mSelectionPen(QPen(QColor(0, 0xFF,0xFF), 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin))
    , mSelectionBrush(QBrush(Qt::NoBrush))
    , mRectSize(0, 0, 0, 0)
{
    // Allow this object to be move and select
    setFlag(ItemIsSelectable, true);
    setFlag(ItemIsMovable, true);
    setFlag(ItemSendsGeometryChanges, true);
    
    // Enable cache for faster redraw
    setCacheMode(DeviceCoordinateCache);

    setBrush(mBrush);
    setPen(mPen);
}

void MyAbstractGraphicsObject::lock()
{
    // Allow this object to be move and select
    setFlag(ItemIsSelectable, false);
    setFlag(ItemIsMovable, false);
    setFlag(ItemSendsGeometryChanges, false);
}

void MyAbstractGraphicsObject::unlock()
{
    // Allow this object to be move and select
    setFlag(ItemIsSelectable, true);
    setFlag(ItemIsMovable, true);
    setFlag(ItemSendsGeometryChanges, true);
}

MyAbstractGraphicsObject::MyAbstractGraphicsObject(const MyAbstractGraphicsObject& item)
    : QGraphicsObject(item.parentItem())
    , mCreating(item.mCreating)
    , mItemName(item.mItemName)
    , mItemAnchorPos(item.mItemAnchorPos)
    , mPen(item.mPen)
    , mBrush(item.mBrush)
    , mSelectionPen(item.mSelectionPen)
    , mSelectionBrush(item.mSelectionBrush)
    , mRectSize(item.mRectSize)
{
    /* Clone child */
    MyAbstractGraphicsObject* cloneItem = 0;
    foreach (QGraphicsItem* graphicsItem, item.childItems())
    {
        cloneItem = ((MyAbstractGraphicsObject *) graphicsItem)->clone();
        
        /* Manually copy each property since QGraphicsItem doesn't have a copy constructor */
        cloneItem->setPos(graphicsItem->scenePos());
        
        cloneItem->setParentItem(this);
    }
    
    setPos(item.pos());
    
    // Allow this object to be move and select
    setFlag(ItemIsSelectable, true);
    setFlag(ItemIsMovable, true);
    setFlag(ItemSendsGeometryChanges, true);
    
    // Enable cache for faster redraw
    setCacheMode(DeviceCoordinateCache);
}

const QString& MyAbstractGraphicsObject::itemName() const
{
    return mItemName;
}

void MyAbstractGraphicsObject::setItemName(const QString &name)
{
    mItemName = name;
}

MyAbstractGraphicsObject::ItemAnchorPosition MyAbstractGraphicsObject::itemAnchorPosition() const
{
    return mItemAnchorPos;
}

void MyAbstractGraphicsObject::setItemAnchorPostion(MyAbstractGraphicsObject::ItemAnchorPosition pos)
{
    mItemAnchorPos = pos;
}

QBrush MyAbstractGraphicsObject::brush() const
{
    return mBrush;
}

void MyAbstractGraphicsObject::setBrush(const QBrush& brush)
{
    mBrush = brush;
}

QPen MyAbstractGraphicsObject::pen() const
{
    return mPen;
}

void MyAbstractGraphicsObject::setPen(const QPen& pen)
{
    mPen = pen;
}

QPen MyAbstractGraphicsObject::selectionPen() const
{
    return mSelectionPen;
}

QBrush MyAbstractGraphicsObject::selectionBrush() const
{
    return mSelectionBrush;
}

QRectF MyAbstractGraphicsObject::rect() const
{
    return mRectSize;
}

void MyAbstractGraphicsObject::setRect(const QRectF &rectangle)
{
    prepareGeometryChange();
    mRectSize = rectangle;
}

// FIXME: This assume that all item change the same property
// FIXME: Add more case
QVariant MyAbstractGraphicsObject::itemChange(GraphicsItemChange change, const QVariant& value)
{
    QVariant v;
    
    if (change == ItemPositionHasChanged && scene()
        && (v=scene()->property("notifier")).isValid())
    {
        NotifierPointer notifier = v.value<NotifierPointer>();
        notifier->increment();
        if (notifier->count() >= scene()->selectedItems().count())
        {
            notifier->notify(ItemPositionHasChanged);
        }
    }
    
    return QGraphicsItem::itemChange(change, value);
}
