
#ifndef Animator_notifyingitem_h
#define Animator_notifyingitem_h

#include <QObject>
#include <QGraphicsItem>

class Notifier : public QObject
{
    Q_OBJECT
    
public:
    Notifier() : mCount(0) {}
    
    int count() const { return mCount; }
    void increment() { mCount++; }
    
    /* Emit notification signal and reset the counter  */
    void notify(QGraphicsItem::GraphicsItemChange change) { mCount = 0; emit notification(change);}

signals:
    void notification(QGraphicsItem::GraphicsItemChange change);
    
private:
    int mCount;
};

#endif
