
#include <QtGui>

#include "mybeziergraphicsobject.h"

MyBezierGraphicsObject::MyBezierGraphicsObject(QList<BPoint> bezierPoint,  const QBrush& brush, const QPen& pen, QGraphicsItem* parent)
    : MyAbstractGraphicsObject(parent, brush, pen)
    , mBezierPoint(bezierPoint)
{
    QPainterPath path = createQPainterPath();
    setRect(path.boundingRect());
}

/*MyBezierGraphicsObject::MyBezierGraphicsObject(const MyBezierGraphicsObject& object)
{
    
}*/

MyBezierGraphicsObject* MyBezierGraphicsObject::clone()
{
    return new MyBezierGraphicsObject(*this);
}

QList<BPoint> MyBezierGraphicsObject::bezierPoint() const
{
    return mBezierPoint;
}

QRectF MyBezierGraphicsObject::boundingRect() const
{
    /* Calculate from child */
    QRectF bound;

    QPainterPath path = createQPainterPath();
    return path.boundingRect().adjusted(-pen().widthF()/2, -pen().widthF()/2, (int)((pen().widthF()/2.0)+0.5), (int)((pen().widthF()/2.0)+0.5));
}

QPainterPath MyBezierGraphicsObject::createQPainterPath() const
{
    QPainterPath path;
    path.moveTo(mBezierPoint.at(0).anchor);

    int i = 1;
    for(i=1; i<mBezierPoint.count(); i++)
    {
        QPointF endPoint = mBezierPoint.at(i).anchor;
        QPointF firstControl = mBezierPoint.at(i-1).rControl;
        QPointF secondControl = mBezierPoint.at(i).lControl;

        //if no control point between two point, create line
        if(firstControl.isNull()&&secondControl.isNull())
        {
            path.lineTo(endPoint);
        }
        //if there are two control point between two point, create cubic bezier
        else if(!(firstControl.isNull())&&!(secondControl.isNull()))
        {
            path.cubicTo(firstControl,secondControl,endPoint);
        }
        //if there is one control point between two point, create quadratic bezier
        else
        {
            if(firstControl.isNull())
            {
                path.quadTo(secondControl,endPoint);
            }
            else
            {
                path.quadTo(firstControl,endPoint);
            }
        }
    }
    
    return path;
}

/* Paint this item using the item internal bezier point list  */
void MyBezierGraphicsObject::paint(QPainter * painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    if (mBezierPoint.size() == 0)
        return;
    
    painter->save();
    painter->setPen(pen());
    painter->setBrush(brush());
    
    QPainterPath path = createQPainterPath();
    painter->drawPath(path);
    
    painter->restore();
    
    /* Draw blue border */
    if (isSelected())
    {
        // Draw the control point
        painter->save();
        painter->setPen(selectionPen());
        painter->setBrush(selectionBrush());
        painter->drawRect(rect());
        painter->restore();
    }
}
