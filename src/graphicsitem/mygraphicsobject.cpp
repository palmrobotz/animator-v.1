#include <QtGui>

#include "myabstractgraphicsobject.h"
#include "mygraphicsobject.h"

MyGraphicsObject::MyGraphicsObject(QGraphicsItem *parent, const QBrush& brush, const QPen& pen)
    : MyAbstractGraphicsObject(parent, brush, pen)
{
}

MyGraphicsObject::MyGraphicsObject(const MyGraphicsObject& item)
    : MyAbstractGraphicsObject(item)
{
}