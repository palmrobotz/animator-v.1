#ifndef LINEGRAPHICSOBJECT_H
#define LINEGRAPHICSOBJECT_H

#include <QGraphicsRectItem>
#include <QBrush>
#include <QPen>

#include "graphicsobjectbuilder.h"

class QRectF;
class MyAbstractGraphicsObject;

class LineGraphicsObjectBuilder : public GraphicsObjectBuilder
{
public:
    LineGraphicsObjectBuilder(const QBrush& brush = QBrush(), const QPen& pen = QPen());
    
    LineGraphicsObjectBuilder(const QRectF& rect, const QBrush& brush = QBrush(), const QPen& pen = QPen());
    
    LineGraphicsObjectBuilder(qreal x, qreal y, qreal width, qreal height, const QBrush& brush = QBrush(), const QPen& pen = QPen());
    
    void newMousePointEvent(const QPointF& newPoint);
    MyAbstractGraphicsObject* createGraphicsObject(MyAbstractGraphicsObject* parent = 0);
    
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    QRectF boundingRect() const;
    
private:
    /* Create rectangle path*/
    void createPath(qreal x, qreal y, qreal width, qreal height);
    
    QList<BPoint> mBezierPoint;
    
    QPointF first;
    QPointF end;
    
    QBrush mBrush;
    QPen mPen;
};

#endif // LINEGRAPHICSOBJECT_H
