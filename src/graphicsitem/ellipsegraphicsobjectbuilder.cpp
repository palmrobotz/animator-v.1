
#include <QtGui>
#include <QDebug>

#include "ellipsegraphicsobjectbuilder.h"
#include "mybeziergraphicsobject.h"

EllipseGraphicsObjectBuilder::EllipseGraphicsObjectBuilder(const QBrush& brush, const QPen& pen)
: mBrush(brush)
, mPen(pen)
{
}

EllipseGraphicsObjectBuilder::EllipseGraphicsObjectBuilder(const QRectF& rect, const  QBrush& brush, const QPen& pen)
: mBrush(brush)
, mPen(pen)
{
    createPath(rect.x(), rect.y(), rect.width(), rect.height());
    setRect(rect);
}

EllipseGraphicsObjectBuilder::EllipseGraphicsObjectBuilder(qreal x, qreal y, qreal width, qreal height, const QBrush& brush, const QPen& pen)
: mBrush(brush)
, mPen(pen)
{
    createPath(x, y, width, height);
    setRect(QRectF(x, y, width, height));
}

void EllipseGraphicsObjectBuilder::createPath(qreal x,qreal y, qreal width, qreal height)
{
    mBezierPoint.clear();
    
    BPoint point1;
    BPoint point2;
    BPoint point3;
    BPoint point4;
    
    qreal magicNumber = 0.2761423749154;
    
    point1.anchor.setX(x);
    point1.anchor.setY(y+(height/2));
    point1.lControl.setX(point1.anchor.x());
    point1.lControl.setY(point1.anchor.y()+(height*magicNumber));
    point1.rControl.setX(point1.anchor.x());
    point1.rControl.setY(point1.anchor.y()-(height*magicNumber));
    
    
    point2.anchor.setX(x+(width/2));
    point2.anchor.setY(y);
    point2.lControl.setX(point2.anchor.x()-(width*magicNumber));
    point2.lControl.setY(point2.anchor.y());
    point2.rControl.setX(point2.anchor.x()+(width*magicNumber));
    point2.rControl.setY(point2.anchor.y());
    
    point3.anchor.setX(x+width);
    point3.anchor.setY(y+(height/2));
    point3.lControl.setX(point3.anchor.x());
    point3.lControl.setY(point3.anchor.y()-(height*magicNumber));
    point3.rControl.setX(point3.anchor.x());
    point3.rControl.setY(point3.anchor.y()+(height*magicNumber));
    
    point4.anchor.setX(x+(width/2));
    point4.anchor.setY(y+height);
    point4.lControl.setX(point4.anchor.x()+(width*magicNumber));
    point4.lControl.setY(point4.anchor.y());
    point4.rControl.setX(point4.anchor.x()-(width*magicNumber));
    point4.rControl.setY(point4.anchor.y());
    
    
    mBezierPoint.append(point1);
    mBezierPoint.append(point2);
    mBezierPoint.append(point3);
    mBezierPoint.append(point4);
    mBezierPoint.append(point1);
}

void EllipseGraphicsObjectBuilder::newMousePointEvent(const QPointF& newPoint)
{
    if(first.isNull())
    {
        first = newPoint;
    }
    
    prepareGeometryChange();
    
    
    // Use x and y as width and height
    QPointF dimension;
    
    if(first.x()>newPoint.x())
    {
        dimension.setX(first.x()-newPoint.x());
    }
    else
    {
        dimension.setX(newPoint.x()-first.x());
    }
    if(first.y()>newPoint.y())
    {
        dimension.setY(first.y()-newPoint.y());
    }
    else
    {
        dimension.setY(newPoint.y()-first.y());
    }
    
    //new point is on upleft, set pos at new point
    if(newPoint.x() < first.x() && newPoint.y() < first.y())
    {
        setPos(newPoint);
        
    }
    //new point is on downleft, set pos x at new point, pos y at first point
    else if(newPoint.x()<first.x()&&newPoint.y()>first.y())
    {
        setPos(newPoint.x(),first.y());
    }
    //new point is on upright, set pos x at first point, pos y at new point
    else if(newPoint.x()>first.x()&&newPoint.y()<first.y())
    {
        setPos(first.x(),newPoint.y());
    }
    //new point is on downright, set pos x at first point,pos y at first point
    else if(newPoint.x()>first.x()&&newPoint.y()>first.y())
    {
        setPos(first);
    }
    
    createPath(0, 0, dimension.x(), dimension.y());
    qDebug() << "Dimension is : " << dimension;
    setRect(QRectF(0, 0, dimension.x(), dimension.y()));
}

MyAbstractGraphicsObject* EllipseGraphicsObjectBuilder::createGraphicsObject(MyAbstractGraphicsObject* parent)
{
    if (mBezierPoint.size() > 0)
    {
        MyBezierGraphicsObject* obj = new MyBezierGraphicsObject(mBezierPoint, mBrush, mPen, parent);
        obj->setPos(pos());
        return obj;
    }
    else
    {
        return 0;
    }
}

void EllipseGraphicsObjectBuilder::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    painter->setBrush(mBrush);
    painter->setPen(mPen);
    painter->drawEllipse(rect());
}

QRectF EllipseGraphicsObjectBuilder::boundingRect() const
{
    return rect().adjusted(-mPen.widthF()/2, -mPen.widthF()/2, (int)((mPen.widthF()/2.0)+0.5), (int)((mPen.widthF()/2.0)+0.5));
}
