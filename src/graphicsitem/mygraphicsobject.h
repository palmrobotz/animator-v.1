
#ifndef MYGRAPHICSOBJECT_H
#define MYGRAPHICSOBJECT_H

#include "myabstractgraphicsobject.h"

class QGraphicsItem;

class MyGraphicsObject : public MyAbstractGraphicsObject
{
    Q_OBJECT
    
public:
    MyGraphicsObject(QGraphicsItem* parent = 0, const QBrush& brush=QBrush(), const QPen& pen=QPen());
    
    //virtual MyGraphicsObject* clone();

protected:
    MyGraphicsObject(const MyGraphicsObject& item);
};

#endif // MYGRAPHICSOBJECT_H
