#ifndef GROUPGRAPHICSITEM_H
#define GROUPGRAPHICSITEM_H

#include "myabstractgraphicsobject.h"

class GroupGraphicsItem : public MyAbstractGraphicsObject
{
    Q_OBJECT
    
public:
    explicit GroupGraphicsItem(QGraphicsItem* parent = 0);
    
    GroupGraphicsItem* clone();
    
    QRectF boundingRect() const;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
};

#endif // GROUPGRAPHICSITEM_H
