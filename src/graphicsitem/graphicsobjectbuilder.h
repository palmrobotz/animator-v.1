#ifndef GRAPHICSOBJECTBUILDER_H
#define GRAPHICSOBJECTBUILDER_H

#include <QRectF>

#include "myabstractgraphicsobject.h"
#include "../graphicsutil/bezierpoint.h"

class QPointF;

class GraphicsObjectBuilder : public QGraphicsItem
{
public:
    virtual void newMousePointEvent(const QPointF& newPoint) = 0;
    virtual MyAbstractGraphicsObject* createGraphicsObject(MyAbstractGraphicsObject* parent = 0) = 0;

    virtual void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0) = 0;
    virtual QRectF boundingRect() const = 0;

    QRectF rect() const { return mRect; }
    void setRect(qreal x, qreal y, qreal width, qreal height)
    {
        mRect = QRectF(x, y, width, height);
    }
    void setRect(const QRectF& rect)
    {
        mRect = rect;
    }

private:
    QRectF mRect;
};

#endif // GRAPHICSOBJECTBUILDER_H
