
#include <QtGui>

#include "brushGraphicsItem.h"
#include "mybeziergraphicsobject.h"

BrushGraphicsItem::BrushGraphicsItem(const QBrush& brush,const QPen& pen)
    : mBrush(brush)
    , mPen(pen)
{
    
}

QPainterPath BrushGraphicsItem::createQPainterPath() const
{
    if (mBezierPoint.isEmpty())
    {
        return QPainterPath();
    }
    
    QPainterPath path;
    path.moveTo(mBezierPoint.at(0).anchor);
    
    int i = 1;
    for(i=1; i<mBezierPoint.count(); i++)
    {
        QPointF endPoint = mBezierPoint.at(i).anchor;
        QPointF firstControl = mBezierPoint.at(i-1).rControl;
        QPointF secondControl = mBezierPoint.at(i).lControl;
        
        //if no control point between two point, create line
        if(firstControl.isNull()&&secondControl.isNull())
        {
            path.lineTo(endPoint);
        }
        //if there are two control point between two point, create cubic bezier
        else if(!(firstControl.isNull())&&!(secondControl.isNull()))
        {
            path.cubicTo(firstControl,secondControl,endPoint);
        }
        //if there is one control point between two point, create quadratic bezier
        else
        {
            if(firstControl.isNull())
            {
                path.quadTo(secondControl,endPoint);
            }
            else
            {
                path.quadTo(firstControl,endPoint);
            }
        }
    }
    
    return path;
}


void BrushGraphicsItem::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    if (mBezierPoint.size() == 0)
        return;
    
    painter->save();
    painter->setPen(mPen);
    painter->setBrush(mBrush);
    
    QPainterPath path = createQPainterPath();
    painter->drawPath(path);
    
    painter->restore();
    
    /* Draw blue border */
    if (isSelected())
    {
        // Draw the control point
        painter->save();
        painter->setPen(mPen);
        painter->setBrush(mBrush);
        painter->drawRect(rect());
        painter->restore();
    }
}

QRectF BrushGraphicsItem::boundingRect() const
{
    /* Calculate from child */
    QRectF bound;
    
    QPainterPath path = createQPainterPath();
    return path.boundingRect().adjusted(-mPen.widthF()/2, -mPen.widthF()/2, (int)((mPen.widthF()/2.0)+0.5), (int)((mPen.widthF()/2.0)+0.5));
}

void BrushGraphicsItem::createPath(QList<QPointF> pathList)
{
    mBezierPoint.clear();

    Q_ASSERT(pathList.size()%4 == 0);

    BPoint firstPoint;
    BPoint lastPoint;
    firstPoint.anchor.setX(pathList.at(0).x());
    firstPoint.anchor.setY(pathList.at(0).y());
    firstPoint.rControl.setX(pathList.at(1).x());
    firstPoint.rControl.setY(pathList.at(1).y());
    lastPoint.anchor.setX(pathList.at(pathList.count()-1).x());
    lastPoint.anchor.setY(pathList.at(pathList.count()-1).y());
    lastPoint.lControl.setX(pathList.at(pathList.count()-2).x());
    lastPoint.lControl.setY(pathList.at(pathList.count()-2).y());
    mBezierPoint.append(firstPoint);
    mBezierPoint.append(lastPoint);

    int i = 2;
    for(i=2;i<pathList.count()-4;i=i+4)
    {
        BPoint currentPoint;
        currentPoint.anchor.setX(pathList.at(i+1).x());
        currentPoint.anchor.setY(pathList.at(i+1).y());
        currentPoint.lControl.setX(pathList.at(i).x());
        currentPoint.lControl.setY(pathList.at(i).y());
        currentPoint.rControl.setX(pathList.at(i+2).x());
        currentPoint.rControl.setY(pathList.at(i+2).y());

        mBezierPoint.insert(mBezierPoint.count()-1,currentPoint);
    }
}

void BrushGraphicsItem::newMousePointEvent(const QPointF& newPoint)
{
    prepareGeometryChange();
    
    mousePoint.push_back(mapFromParent(newPoint));
    if (mousePoint.size() > 3)
    {
        // FIXME: Not safe code
        curve.FitPointToCurve(mousePoint.data(), mousePoint.size(), 1.0);
        curvePoint = curve.bezierControlPoint();
        createPath(curvePoint);
    }
}

MyAbstractGraphicsObject* BrushGraphicsItem::createGraphicsObject(MyAbstractGraphicsObject* parent)
{
    if (mBezierPoint.size() > 0)
    {
        MyBezierGraphicsObject* obj = new MyBezierGraphicsObject(mBezierPoint, mBrush, mPen, parent);
        obj->setPos(pos());
        return obj;
    }
    else
    {
        return 0;
    }
}
