#ifndef BRUSHGRAPHICSITEM_H
#define BRUSHGRAPHICSITEM_H

#include "mygraphicsobject.h"
#include "../graphicsutil/fitcurve.h"

#include <QVector>
#include <QList>
#include <QPointF>

#include "graphicsobjectbuilder.h"

class QRectF;
class MyAbstractGraphicsObject;

class BrushGraphicsItem : public GraphicsObjectBuilder
{
public:
    BrushGraphicsItem(const QBrush& brush=QBrush(),const QPen& pen=QPen());
    
    QPainterPath createQPainterPath() const;
    
    void newMousePointEvent(const QPointF& newPoint);
    MyAbstractGraphicsObject* createGraphicsObject(MyAbstractGraphicsObject* parent = 0);
    
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    QRectF boundingRect() const;
    
private:
    void createPath(QList<QPointF> pathList);
    
    QVector<QPointF> mousePoint;
    QList<BPoint> mBezierPoint;
    
    QList<QPointF> curvePoint;
    FitCurve curve;
    
    QBrush mBrush;
    QPen mPen;
};

#endif // BRUSHGRAPHICSITEM_H
