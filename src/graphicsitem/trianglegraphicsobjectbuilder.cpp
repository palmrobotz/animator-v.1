#include <QtGui>
#include <QDebug>

#include "trianglegraphicsobjectbuilder.h"
#include "mybeziergraphicsobject.h"

TriangleGraphicsObjectBuilder::TriangleGraphicsObjectBuilder(const QBrush& brush, const QPen& pen)
    : mBrush(brush)
    , mPen(pen)
{
}

TriangleGraphicsObjectBuilder::TriangleGraphicsObjectBuilder(const QRectF& rect, const  QBrush& brush, const QPen& pen)
    : mBrush(brush)
    , mPen(pen)
{
    createPath(rect.x(), rect.y(), rect.width(), rect.height());
    setRect(rect);
}

TriangleGraphicsObjectBuilder::TriangleGraphicsObjectBuilder(qreal x, qreal y, qreal width, qreal height, const QBrush& brush, const QPen& pen)
    : mBrush(brush)
    , mPen(pen)
{
    createPath(x, y, width, height);
    setRect(QRectF(x, y, width, height));
}

void TriangleGraphicsObjectBuilder::createPath(qreal x,qreal y, qreal width, qreal height)
{
    mBezierPoint.clear();

    BPoint top;
    BPoint leftBase;
    BPoint rightBase;

    top.anchor.setX(x+(width/2));
    top.anchor.setY(y);
    leftBase.anchor.setX(x);
    leftBase.anchor.setY(y+height);
    rightBase.anchor.setX(x+width);
    rightBase.anchor.setY(y+height);
    mBezierPoint.append(top);
    mBezierPoint.append(rightBase);
    mBezierPoint.append(leftBase);
    mBezierPoint.append(top);
}

void TriangleGraphicsObjectBuilder::newMousePointEvent(const QPointF& newPoint)
{
    if(first.isNull())
    {
        first = newPoint;
    }

    prepareGeometryChange();


    // Use x and y as width and height
    QPointF dimension;

    if(first.x()>newPoint.x())
    {
        dimension.setX(first.x()-newPoint.x());
    }
    else
    {
        dimension.setX(newPoint.x()-first.x());
    }
    if(first.y()>newPoint.y())
    {
        dimension.setY(first.y()-newPoint.y());
    }
    else
    {
        dimension.setY(newPoint.y()-first.y());
    }

    //new point is on upleft, set pos at new point
    if(newPoint.x() < first.x() && newPoint.y() < first.y())
    {
        setPos(newPoint);

    }
    //new point is on downleft, set pos x at new point, pos y at first point
    else if(newPoint.x()<first.x()&&newPoint.y()>first.y())
    {
        setPos(newPoint.x(),first.y());
    }
    //new point is on upright, set pos x at first point, pos y at new point
    else if(newPoint.x()>first.x()&&newPoint.y()<first.y())
    {
        setPos(first.x(),newPoint.y());
    }
    //new point is on downright, set pos x at first point,pos y at first point
    else if(newPoint.x()>first.x()&&newPoint.y()>first.y())
    {
        setPos(first);
    }

    createPath(0, 0, dimension.x(), dimension.y());
    qDebug() << "Dimension is : " << dimension;
    setRect(QRectF(0, 0, dimension.x(), dimension.y()));
}

MyAbstractGraphicsObject* TriangleGraphicsObjectBuilder::createGraphicsObject(MyAbstractGraphicsObject* parent)
{
    if (mBezierPoint.size() > 0)
    {
        MyBezierGraphicsObject* obj = new MyBezierGraphicsObject(mBezierPoint, mBrush, mPen, parent);
        obj->setPos(pos());
        return obj;
    }
    else
    {
        return 0;
    }
}

void TriangleGraphicsObjectBuilder::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    painter->setBrush(mBrush);
    painter->setPen(mPen);
    painter->drawRect(rect());
}

QRectF TriangleGraphicsObjectBuilder::boundingRect() const
{
    return rect().adjusted(-mPen.widthF()/2, -mPen.widthF()/2, (int)((mPen.widthF()/2.0)+0.5), (int)((mPen.widthF()/2.0)+0.5));
}
