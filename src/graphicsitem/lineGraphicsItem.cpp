
#include <QtGui>
#include <QDebug>

#include "lineGraphicsItem.h"
#include "mybeziergraphicsobject.h"

LineGraphicsObjectBuilder::LineGraphicsObjectBuilder(const QBrush& brush, const QPen& pen)
: mBrush(brush)
, mPen(pen)
{
}

LineGraphicsObjectBuilder::LineGraphicsObjectBuilder(const QRectF& rect, const  QBrush& brush, const QPen& pen)
: mBrush(brush)
, mPen(pen)
{
    createPath(rect.x(), rect.y(), rect.width(), rect.height());
    setRect(rect);
}

LineGraphicsObjectBuilder::LineGraphicsObjectBuilder(qreal x, qreal y, qreal width, qreal height, const QBrush& brush, const QPen& pen)
: mBrush(brush)
, mPen(pen)
{
    createPath(x, y, width, height);
    setRect(QRectF(x, y, width, height));
}

void LineGraphicsObjectBuilder::createPath(qreal x,qreal y, qreal width, qreal height)
{
    mBezierPoint.clear();
    QPointF start;
    QPointF end;
    start.setX(x);
    start.setY(y);
    end.setX(width);
    end.setY(height);
    BPoint sPoint;
    BPoint ePoint;
    sPoint.anchor=start;
    ePoint.anchor=end;
    mBezierPoint.append(sPoint);
    mBezierPoint.append(ePoint);
}

void LineGraphicsObjectBuilder::newMousePointEvent(const QPointF& newPoint)
{
    if(first.isNull())
    {
        first = newPoint;
        setPos(first);
        return;
    }
    
    prepareGeometryChange();
    
    end = newPoint;
    
    if (end.x() > first.x())
    {
        createPath(0, 0, end.x() - first.x(), end.y() - first.y());
        setRect(QRectF(0, 0, end.x() - first.x(), end.y() - first.y()));
    }
    else
    {
        setPos(end);
        createPath(0, 0, first.x() - end.x(), first.y() - end.y());
        setRect(QRectF(0, 0, first.x() - end.x(), first.y() - end.y()));
    }
    
    
    //qDebug() << "Dimension is : " << dimension;
    //setRect(QRectF(0, 0, dimension.x(), dimension.y()));
}

MyAbstractGraphicsObject* LineGraphicsObjectBuilder::createGraphicsObject(MyAbstractGraphicsObject* parent)
{
    if (mBezierPoint.size() > 0)
    {
        MyBezierGraphicsObject* obj = new MyBezierGraphicsObject(mBezierPoint, mBrush, mPen, parent);
        obj->setPos(pos());
        return obj;
    }
    else
    {
        return 0;
    }
}

void LineGraphicsObjectBuilder::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    painter->setBrush(mBrush);
    painter->setPen(mPen);
    painter->drawLine(0, 0, rect().width(), rect().height());
}

QRectF LineGraphicsObjectBuilder::boundingRect() const
{
    return rect().adjusted(-mPen.widthF()/2, -mPen.widthF()/2, (int)((mPen.widthF()/2.0)+0.5), (int)((mPen.widthF()/2.0)+0.5));
}
