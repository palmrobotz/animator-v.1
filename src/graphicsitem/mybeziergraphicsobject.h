#ifndef MYBEZIERGRAPHICSOBJECT_H
#define MYBEZIERGRAPHICSOBJECT_H

#include <QList>
#include <QPainterPath>

#include "../graphicsitem/myabstractgraphicsobject.h"
#include "../graphicsutil/bezierpoint.h"

class QRectF;

class MyBezierGraphicsObject : public MyAbstractGraphicsObject
{
    Q_OBJECT

public:
    explicit MyBezierGraphicsObject(QList<BPoint> bezierPoint, const QBrush& brush = QBrush(),  const QPen& pen = QPen(), QGraphicsItem* parent = 0);

    virtual MyBezierGraphicsObject* clone();

    QList<BPoint> bezierPoint() const;
    
    QRectF boundingRect() const;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    
    enum { Type = UserType + 5 };
    int type() const { return Type; }
    
private:
    QPainterPath createQPainterPath() const;
    
    QList<BPoint> mBezierPoint;

};

#endif // MYBEZIERGRAPHICSOBJECT_H
