#ifndef ANIMATOR_H
#define ANIMATOR_H

class QUndoGroup;

class Animator
{
public:
    /* Enum for all tool supported */
    enum Tool { None, Rectangle, Ellipse, Pen, Brush, Triangle, Line};
};

#endif // ANIMATOR_H
