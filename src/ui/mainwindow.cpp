#include <QtGui>

#include "mainwindow.h"
#include "mypreviewarea.h"
#include "mygraphicsview.h"
#include "infowidget.h"
#include "colormixer.h"
#include "swatchcolor.h"
#include "mypreviewarea.h"
#include "strokewidget.h"
#include "graphicsitemlibwidget.h"
#include "timelineui.h"

#include "../core/timeline.h"
#include "../core/animationscene.h"
#include "../core/svg.h"

MainWindow::MainWindow(QWidget* parent, QFlags<Qt::WindowType> flags)
    : QMainWindow(parent, flags)
    , pen(QBrush(), 0, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin)
{
    /* Create default scene and create a view to show it */
    currentScene = new AnimationScene(QRectF(0, 0, 640, 480));
    previewArea = new MyPreviewArea(currentScene, this);
    timeline = new TimeLine(previewArea->graphicsView(), currentScene);
 
    /* Instantiate an undo group and add an undo stack of the default scene to the group */
    undoGroup = new QUndoGroup(this);
    undoGroup->addStack(currentScene->undoStack());
    undoGroup->setActiveStack(currentScene->undoStack());
    
    createMenus();
    createToolBars();
    createDockWidgets();

    connect(this, SIGNAL(toolChanged(Animator::Tool))
            , previewArea->graphicsView()
            , SLOT(setTool(Animator::Tool)));
    connect(this, SIGNAL(brushChange(QBrush))
            , previewArea->graphicsView()
            , SLOT(setGraphicsBrush(QBrush)));
    connect(this, SIGNAL(penChange(QPen))
            , previewArea->graphicsView()
            , SLOT(setGraphicsPen(QPen)));
    
    setCentralWidget(previewArea);
    setWindowTitle(tr("Animator"));
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    QIcon icon;
    icon.addFile("://src/pic/icon.png", QSize(), QIcon::Normal, QIcon::Off);
    setWindowIcon(icon);

}

void MainWindow::createMenus()
{
    QString menuBarSyle = "\n"
            " QMenuBar{\n"
            "    background-color: qlineargradient(spread:pad, x1:0.483318, y1:0.154, x2:0.483318, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(104, 104, 104, 255))\n"
            "}\n"
            " QMenuBar::item{\n"
            "    background: transparent;\n"
            " }\n"
            "QMenuBar::item:selected { \n"
            "    background: gray;\n"
            "}\n"

            "QMenuBar::item:pressed {\n"
            "    background: dark gray;\n"
            "}\n"
            "";

    QString menuStyle="\n"
            " QMenu{\n"
            "    background-color: #ABABAB ;\n"
            "    border: 1px solid grey;\n"
            " }\n"
            "";

    setStyleSheet(QString::fromUtf8("background-color: rgb(71, 71, 71)  ;\n"));

    newFileAction = new QAction(tr("&New"), this);
    newFileAction->setShortcut(QKeySequence::New);
    newFileAction->setStatusTip(tr("Create a new file"));
    newFileAction->setEnabled(false);
    
    openFileAction = new QAction(tr("&Open"), this);
    openFileAction->setShortcut(QKeySequence::Open);
    openFileAction->setStatusTip(tr("Open an exist file"));
    openFileAction->setEnabled(false);
    
    saveFileAction = new QAction(tr("&Save"), this);
    saveFileAction->setShortcut(QKeySequence::Save);
    saveFileAction->setStatusTip(tr("Save this file"));
    saveFileAction->setEnabled(false);
    
    exportFileAction = new QAction(tr("&Export"), this);
    // Set key sequence manually to something
    exportFileAction->setStatusTip(tr("Export HTML5 file"));
    exportFileAction->setShortcut(QKeySequence(tr("Ctrl+E")));
    connect(exportFileAction, SIGNAL(triggered())
            , this, SLOT(exportHTML5()));

    undoAction = undoGroup->createUndoAction(this);
    undoAction->setShortcut(QKeySequence::Undo);
    redoAction = undoGroup->createRedoAction(this);
    redoAction->setShortcut(QKeySequence::Redo);
    
    infoAction = new QAction(tr("&Info"),this);
    infoAction->setStatusTip(tr("Turn on info widget"));
    infoAction->setCheckable(true);
    infoAction->setChecked(true);
    connect(infoAction, SIGNAL(toggled(bool))
            , this, SLOT(setInfoDockVisible(bool)));

    colorAction = new QAction(tr("&Color"),this);
    colorAction->setStatusTip(tr("Turn on color widget"));
    colorAction->setCheckable(true);
    colorAction->setChecked(true);
    connect(colorAction, SIGNAL(toggled(bool))
            , this, SLOT(setColorDockVisible(bool)));

    swatchAction = new QAction(tr("&Swatch"),this);
    swatchAction->setStatusTip(tr("Turn on swatch widget"));
    swatchAction->setCheckable(true);
    swatchAction->setChecked(true);
    connect(swatchAction, SIGNAL(toggled(bool))
            , this, SLOT(setSwatchDockVisible(bool)));

    historyAction = new QAction(tr("&History"),this);
    historyAction->setStatusTip(tr("Turn on history widget"));
    historyAction->setCheckable(true);
    historyAction->setChecked(true);
    connect(historyAction, SIGNAL(toggled(bool))
            , this, SLOT(setHistoryDockVisible(bool)));

    strokeAction = new QAction(tr("&Stroke"),this);
    strokeAction->setStatusTip(tr("Turn on stroke widget"));
    strokeAction->setCheckable(true);
    strokeAction->setChecked(true);
    connect(strokeAction, SIGNAL(toggled(bool))
            , this, SLOT(setStrokeDockVisible(bool)));

    timelineAction = new QAction(tr("&Timeline"),this);
    timelineAction->setStatusTip(tr("Turn on timeline widget"));
    timelineAction->setCheckable(true);
    timelineAction->setChecked(true);
    connect(timelineAction, SIGNAL(toggled(bool))
            , this, SLOT(setTimelineDockVisible(bool)));

    QPalette *buttonPalette = new QPalette();
    buttonPalette->setColor(QPalette::ButtonText,Qt::white);

    QMenuBar* myMenubar = menuBar();
    myMenubar->setPalette(*buttonPalette);
    myMenubar->setStyleSheet(menuBarSyle);

    QMenu* fileMenu = myMenubar->addMenu(tr("&File") );
    fileMenu->addAction(newFileAction);
    fileMenu->addSeparator();
    fileMenu->addAction(openFileAction);
    fileMenu->addAction(saveFileAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exportFileAction);
    fileMenu->setStyleSheet(menuStyle);

    QMenu* editMenu = myMenubar->addMenu(tr("&Edit"));
    editMenu->addAction(undoAction);
    editMenu->addAction(redoAction);
    
    QMenu* viewMenu = myMenubar->addMenu(tr("&View"));
    viewMenu->addAction(infoAction);
    viewMenu->addAction(colorAction);
    viewMenu->addAction(swatchAction);
    viewMenu->addAction(historyAction);
    viewMenu->addAction(strokeAction);
    viewMenu->addSeparator();
    viewMenu->addAction(timelineAction);
    viewMenu->setStyleSheet(menuStyle);
}

void MainWindow::createToolBars()
{
    QPixmap noneImage(":/src/pic/none.png");
    QPixmap penImage(":/src/pic/pen.png");
    QPixmap rectImage(":/src/pic/rect.png");
    QPixmap ellipseImage(":/src/pic/ellipse.png");
    QPixmap triangleImage(":/src/pic/triangle.gif");
    QPixmap brushImage(":/src/pic/brush.png");
    QPixmap lineImage(":/src/pic/line.png");

    QIcon noneIcon(noneImage);
    QIcon penIcon(penImage);
    QIcon rectIcon(rectImage);
    QIcon ellipseIcon(ellipseImage);
    QIcon triangleIcon(triangleImage);
    QIcon brushIcon(brushImage);
    QIcon lineIcon(lineImage);

    noneButton = new QToolButton();
    noneButton->setIcon(noneIcon);
    noneButton->setCheckable(true);
    noneButton->setAutoRaise(false);
    noneButton->setToolTip("Select (V)");
    noneButton->setShortcut(QKeySequence(tr("V")));
    
    rectButton = new QToolButton();
    rectButton->setIcon(rectIcon);
    rectButton->setCheckable(true);
    rectButton->setAutoRaise(false);
    rectButton->setToolTip("Draw Rectangle (R)");
    rectButton->setShortcut(QKeySequence(tr("R")));
    
    ellipseButton = new QToolButton();
    ellipseButton->setIcon(ellipseIcon);
    ellipseButton->setCheckable(true);
    ellipseButton->setAutoRaise(false);
    ellipseButton->setShortcut(QKeySequence(tr("E")));
    ellipseButton->setToolTip("Draw Ellipse (E)");
    
    penButton = new QToolButton();
    penButton->setIcon(penIcon);
    penButton->setCheckable(true);
    penButton->setAutoRaise(false);
    
    brushButton = new QToolButton();
    brushButton->setIcon(brushIcon);
    brushButton->setCheckable(true);
    brushButton->setAutoRaise(false);
    brushButton->setShortcut(QKeySequence(tr("B")));
    brushButton->setToolTip("Brush Tool (B)");
    
    triangleButton = new QToolButton();
    triangleButton->setIcon(triangleIcon);
    triangleButton->setCheckable(true);
    triangleButton->setAutoRaise(false);
    triangleButton->setShortcut(QKeySequence(tr("T")));
    triangleButton->setToolTip("Draw Triangle (T)");

    lineButton = new QToolButton();
    lineButton->setText("Line");
    lineButton->setIcon(lineIcon);
    lineButton->setCheckable(true);
    lineButton->setAutoRaise(false);
    lineButton->setShortcut(QKeySequence(tr("L")));
    lineButton->setToolTip("Draw Line (L)");

    QButtonGroup* buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(noneButton);
    buttonGroup->addButton(lineButton);
    buttonGroup->addButton(rectButton);
    buttonGroup->addButton(ellipseButton);
    buttonGroup->addButton(triangleButton);
    buttonGroup->addButton(penButton);
    buttonGroup->addButton(brushButton);

    QToolBar* toolBar = new QToolBar(this);
    toolBar->setStyleSheet(QString::fromUtf8("\n"
                                             " QToolBar{\n"
                                             "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(80, 80, 80, 255))  ;\n"
                                             "    border-color: rgb(0, 0, 0);\n"
                                             " }\n"
                                             ""));

    toolBar->addWidget(noneButton);
    toolBar->addWidget(lineButton);
    toolBar->addWidget(rectButton);
    toolBar->addWidget(triangleButton);
    toolBar->addWidget(ellipseButton);
    //toolBar->addWidget(penButton);
    toolBar->addWidget(brushButton);

    addToolBar(Qt::LeftToolBarArea, toolBar);
    
    // When the toolbar button is clicked call this class private slot
    // to emit signal to notify other widget of the change
    connect(buttonGroup
            , SIGNAL(buttonClicked(QAbstractButton*))
            , this
            , SLOT(toolbarActionSelected(QAbstractButton*)));
}

void MainWindow::createDockWidgets()
{
    QString undoStyle = "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(80, 80, 80, 255))  ;\n"
            "color:white;\n"
            "";

    QString dockStyle ="\n"
            " QDockWidget::title {\n"
            "     text-align: center; /* align the text to the left */\n"
            "     	background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #a6a6a6, stop: 0.08 #7f7f7f, stop: 0.39999 #717171, stop: 0.4 #626262,stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
            "     padding-left: 5px;\n"
            " }\n"
            "";

    // Create each widget instance
    InfoWidget* infoWidget = new InfoWidget();
    connect(previewArea->graphicsView()
            , SIGNAL(selectionChanged(QList<QGraphicsItem *>))
            , infoWidget
            , SLOT(objectChanged(QList<QGraphicsItem *>)));

    ColorMixer* colorMixer = new ColorMixer();
    connect(colorMixer, SIGNAL(brushColorChange(QBrush)),this,SLOT(setBrushBuffer(QBrush)));
    connect(colorMixer, SIGNAL(penColorChange(QPen)),this,SLOT(setPenBuffer(QPen)));
    
    StrokeWidget* stroke = new StrokeWidget();
    connect(stroke,SIGNAL(strokeValueChnge(int)),this,SLOT(changePenWidth(int)));
    
    SwatchColor* swatch = new SwatchColor();
    connect(swatch,SIGNAL(swatchSlotClick(int,int,int))
            ,colorMixer,SLOT(changeRGBslider(int,int,int)));
    
    QUndoView* undoView = new QUndoView(undoGroup, this);
    undoView->setStyleSheet(undoStyle);
    
    TimeLineUI* timelineWidget = new TimeLineUI(timeline);
    
    /*GraphicsItemLibWidget* libWidget = new GraphicsItemLibWidget();
     connect(libWidget
     , SIGNAL(itemSelectedChanged(MyGraphicsObject*))
     , previewArea
     , SLOT(setCloseUpItem(MyGraphicsObject*)));*/
    
    // Wrap it in dockwidget
    
    infoDockWidget = new QDockWidget(tr("Info"), this);
    infoDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea);
    infoDockWidget->setWidget(infoWidget);
    infoDockWidget->setStyleSheet(dockStyle);
    
    colorDockWidget = new QDockWidget(tr("Color"), this);
    colorDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                     Qt::RightDockWidgetArea);
    colorDockWidget->setWidget(colorMixer);
    colorDockWidget->setStyleSheet(dockStyle);

    swatchDockWidget = new QDockWidget(tr("Swatch"), this);
    swatchDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                     Qt::RightDockWidgetArea);
    swatchDockWidget->setWidget(swatch);
    swatchDockWidget->setStyleSheet(dockStyle);

    strokeDockWidget = new QDockWidget(tr("Stroke"),this);
    strokeDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                      Qt::RightDockWidgetArea);
    strokeDockWidget->setWidget(stroke);
    strokeDockWidget->setStyleSheet(dockStyle);
    
    historyDockWidget = new QDockWidget(tr("History"), this);
    historyDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                       Qt::RightDockWidgetArea);
    historyDockWidget->setWidget(undoView);
    historyDockWidget->setStyleSheet(dockStyle);
    
    timeLineDockWidget = new QDockWidget(tr("Timeline"), this);
    timeLineDockWidget->setWidget(timelineWidget);
    timeLineDockWidget->setAllowedAreas(Qt::TopDockWidgetArea |
                                        Qt::BottomDockWidgetArea);
    timeLineDockWidget->setStyleSheet(dockStyle);
    
    // Show all dockwidget
    addDockWidget(Qt::RightDockWidgetArea, infoDockWidget);
    addDockWidget(Qt::RightDockWidgetArea, colorDockWidget);
    addDockWidget(Qt::RightDockWidgetArea, swatchDockWidget);
    addDockWidget(Qt::RightDockWidgetArea, strokeDockWidget);
    addDockWidget(Qt::RightDockWidgetArea, historyDockWidget);
    //tabifyDockWidget(colorDockWidget, swatchDockWidget);
    addDockWidget(Qt::BottomDockWidgetArea, timeLineDockWidget);

    // Show tab on top
    setTabPosition(Qt::RightDockWidgetArea, QTabWidget::North);
}

void MainWindow::createStatusBar()
{

}

void MainWindow::toolbarActionSelected(QAbstractButton* button)
{
    if (button == noneButton)
        emit toolChanged(Animator::None);
    else if (button == rectButton)
        emit toolChanged(Animator::Rectangle);
    else if (button == ellipseButton)
        emit toolChanged(Animator::Ellipse);
    else if (button == penButton)
        emit toolChanged(Animator::Pen);
    else if (button == triangleButton)
        emit toolChanged(Animator::Triangle);
    else if (button == brushButton)
        emit toolChanged(Animator::Brush);
    else if (button == lineButton)
        emit toolChanged(Animator::Line);
}

void MainWindow::exportHTML5()
{
    SvgGenerator generator(currentScene->graphicsScene()->sceneRect());
    // Set source
    generator.setAnimationData(timeline->animationData());
    
    QString svg = generator.exportSVG();

    QString fileName = QFileDialog::getSaveFileName(this, tr("Export Animation")
                                , QString(), tr("HTML5 (*.html);;SVG (*.svg)"));
    
    if (fileName.isEmpty())
        return;
    
    QFile file(fileName);
    if (file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&file);
        out << svg;
    }
    file.close();
}

void MainWindow::setInfoDockVisible(bool b)
{
    infoDockWidget->setVisible(b);
}

void MainWindow::setColorDockVisible(bool b)
{
    colorDockWidget->setVisible(b);
}

void MainWindow::setSwatchDockVisible(bool b)
{
    swatchDockWidget->setVisible(b);
}

void MainWindow::setStrokeDockVisible(bool b)
{
    strokeDockWidget->setVisible(b);
}

void MainWindow::setTimelineDockVisible(bool b)
{
    timeLineDockWidget->setVisible(b);
}

void MainWindow::setHistoryDockVisible(bool b)
{
    historyDockWidget->setVisible(b);
}

void MainWindow::setDockVisible(DockWidget dock, bool b)
{
    switch (dock)
    {
        case Info:
            infoAction->setChecked(b);
            infoDockWidget->setVisible(b);
            break;
        case Color:
            colorAction->setChecked(b);
            colorDockWidget->setVisible(b);
            break;
        case Swatch:
            swatchAction->setChecked(b);
            swatchDockWidget->setVisible(b);
            break;
        case History:
            historyAction->setChecked(b);
            historyDockWidget->setVisible(b);
            break;
        case Stroke:
            strokeAction->setChecked(b);
            strokeDockWidget->setVisible(b);
            break;
        case Timeline:
            timelineAction->setChecked(b);
            timeLineDockWidget->setVisible(b);
            break;
    }
}

void MainWindow::setBrushBuffer(QBrush brushColor)
{
    emit(brushChange(brushColor));
}

void MainWindow::setPenBuffer(QPen penColor)
{
    pen.setColor(penColor.color());
    emit(penChange(penColor));
}

void MainWindow::changePenWidth(int size)
{
    pen.setWidth(size);
    emit(penChange(pen));
}
