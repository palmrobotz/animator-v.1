#ifndef UIOPERATION_H
#define UIOPERATION_H

#include <QUndoCommand>
#include <QGraphicsScene>

class MyAbstractGraphicsObject;

class NewGraphicsItem : public QUndoCommand
{
public:
    explicit NewGraphicsItem(QGraphicsScene* scene, MyAbstractGraphicsObject* item);

    void undo();
    void redo();
    
    MyAbstractGraphicsObject* item();
    
private:
    MyAbstractGraphicsObject* createGraphicsItem();
    
    QGraphicsScene* mScene;
    MyAbstractGraphicsObject* mItem;
};

#endif // UIOPERATION_H
