#ifndef MIXER_H
#define MIXER_H

#include <QWidget>
#include <QSlider>
#include <QBrush>
#include <QPen>

#include "twincolorbox.h"

class QwwTwoColorIndicator;

class ColorMixer : public QWidget
{
    Q_OBJECT
    
public:
    ColorMixer(QWidget *parent = 0);
    ~ColorMixer();

signals:
    void  brushColorChange(QBrush brush);
    void  penColorChange(QPen pen);

private slots:
    void redChanger(int r);
    void greenChanger(int g);
    void blueChanger(int b);
    void alphaChanger(int a);
    void changeRGBslider(int r,int g,int b);

private:
    QWidget *paintBox;
    TwinColorBox *twinColor;
    QwwTwoColorIndicator* colorIndicator;
    QSlider *Rslider;
    QSlider *Gslider;
    QSlider *Bslider;
    QSlider *Aslider;

    int rVal;
    int gVal;
    int bVal;
    int aVal;
};

#endif // MIXER_H
