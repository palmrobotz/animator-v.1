
#include <QtGui>

#include "infowidget.h"
#include "../graphicsitem/myabstractgraphicsobject.h"

InfoWidget::InfoWidget(QWidget* parent)
    : QWidget(parent, 0)
{
    setupUi();
    
    connect(xSpinBox, SIGNAL(valueChanged(double)), this, SLOT(xValueChanged(double)));
    connect(ySpinBox, SIGNAL(valueChanged(double)), this, SLOT(yValueChanged(double)));
    connect(widthSpinBox, SIGNAL(valueChanged(double)), this, SLOT(widthValueChanged(double)));
    connect(heightSpinBox, SIGNAL(valueChanged(double)), this, SLOT(heightValueChanged(double)));
}

void InfoWidget::objectChanged(QList<QGraphicsItem *> selectedObject)
{
    if (selectedObject.isEmpty())
    {
        setEnabled(false);
        selectingItem.clear();
        return;
    }
    
    setEnabled(true);
    selectingItem = selectedObject;
    
    disconnect(xSpinBox, SIGNAL(valueChanged(double)), this, SLOT(xValueChanged(double)));
    disconnect(ySpinBox, SIGNAL(valueChanged(double)), this, SLOT(yValueChanged(double)));
    disconnect(widthSpinBox, SIGNAL(valueChanged(double)), this, SLOT(widthValueChanged(double)));
    disconnect(heightSpinBox, SIGNAL(valueChanged(double)), this, SLOT(heightValueChanged(double)));
    
    // Using left most point
    oldSize = QRectF();
    oldPoint = selectingItem.at(0)->scenePos();
    foreach (QGraphicsItem* item, selectingItem)
    {
        oldSize = ((MyAbstractGraphicsObject *) item)->rect().unite(oldSize);
        if (item->scenePos().x() < oldPoint.x())
            oldPoint.setX(item->scenePos().x());
        if (item->scenePos().y() < oldPoint.y())
            oldPoint.setY(item->scenePos().y());
    }
    
    xSpinBox->setValue(oldPoint.x());
    ySpinBox->setValue(oldPoint.y());
    widthSpinBox->setValue(oldSize.width());
    heightSpinBox->setValue(oldSize.height());
    
    connect(xSpinBox, SIGNAL(valueChanged(double)), this, SLOT(xValueChanged(double)));
    connect(ySpinBox, SIGNAL(valueChanged(double)), this, SLOT(yValueChanged(double)));
    connect(widthSpinBox, SIGNAL(valueChanged(double)), this, SLOT(widthValueChanged(double)));
    connect(heightSpinBox, SIGNAL(valueChanged(double)), this, SLOT(heightValueChanged(double)));
}

void InfoWidget::xValueChanged(double d)
{
    if (!selectingItem.isEmpty())
    {
        //double deltaX = d - oldPoint.x();
        //qDebug() << "Delta is : " << deltaX;
        foreach (QGraphicsItem* item, selectingItem)
        {
            //item->setPos(item->pos().x() + deltaX, item->pos().y());
            item->setPos(d, item->scenePos().y());
        }
    }
}

void InfoWidget::yValueChanged(double d)
{
    if (!selectingItem.isEmpty())
    {
        //double deltaY = d - oldPoint.y();
        foreach (QGraphicsItem* item, selectingItem)
        {
            //item->setPos(item->pos().x(), item->pos().y() + deltaY);
            item->setPos(item->scenePos().x(), d);
        }
    }
}

void InfoWidget::widthValueChanged(double d)
{
    if (d < 0)
    {
        widthSpinBox->setValue(0);
        return;
    }
    
    if (!selectingItem.isEmpty())
    {
        //double deltaWidth = d - oldSize.width();
        foreach (QGraphicsItem* item, selectingItem)
        {
            MyAbstractGraphicsObject* myItem = (MyAbstractGraphicsObject *) item;
            QRectF rect = myItem->rect();
            //rect.setWidth(rect.width()+deltaWidth);
            //myItem->setRect(rect);
            rect.setWidth(d);
            myItem->setRect(rect);
        }
    }
}

void InfoWidget::heightValueChanged(double d)
{
    if (d < 0)
    {
        heightSpinBox->setValue(0);
        return;
    }
    
    if (!selectingItem.isEmpty())
    {
        //double deltaWidth = d - oldSize.width();
        foreach (QGraphicsItem* item, selectingItem)
        {
            MyAbstractGraphicsObject* myItem = (MyAbstractGraphicsObject *) item;
            QRectF rect = myItem->rect();
            //rect.setWidth(rect.width()+deltaWidth);
            //myItem->setRect(rect);
            rect.setHeight(d);
            myItem->setRect(rect);
        }
    }
}

// TODO: Add lock behavier
void InfoWidget::setupUi()
{
    QString mainStyle ="\n"
            " QWidget{\n"
            "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(80, 80, 80, 255))  ;\n"
            " }\n"
            "";

    QString lineEditStyle = "QLineEdit{\n"
            "background: rgb(46,46,46);\n"
            "border: 1px solid gray;\n"
            "color:white"
            "}\n"
            "";

    QString labelStyle="background-color: transparent;\n"
            "color: white;\n"
            "";

    QString spinBoxStyle = "background: rgb(46,46,46);\n"
        #ifdef Q_OS_WIN
            "border: 1px solid gray;\n"
        #endif
            "color:white"
            "";

    setStyleSheet(mainStyle);

    QLabel* nameLabel = new QLabel(this);
    nameLabel->setText(tr("Name"));
    nameLabel->setStyleSheet(labelStyle);
    QLabel* xLabel = new QLabel(this);
    xLabel->setText(tr("X: "));
    xLabel->setAlignment(Qt::AlignRight);
    xLabel->setStyleSheet(labelStyle);
    QLabel* yLabel = new QLabel(this);
    yLabel->setText(tr("Y: "));
    yLabel->setAlignment(Qt::AlignRight);
    yLabel->setStyleSheet(labelStyle);
    QLabel* widthLabel = new QLabel(this);
    widthLabel->setText(tr("W: "));
    widthLabel->setAlignment(Qt::AlignRight);
    widthLabel->setStyleSheet(labelStyle);
    QLabel* heightLabel = new QLabel(this);
    heightLabel->setText(tr("H: "));
    heightLabel->setAlignment(Qt::AlignRight);
    heightLabel->setStyleSheet(labelStyle);

    // FIXME: Set the range automatically
    nameLineEdit = new QLineEdit(this);
    nameLineEdit->setAlignment(Qt::AlignRight);
    //nameLineEdit->setPalette(*textPalette);
    nameLineEdit->setStyleSheet(lineEditStyle);

    xSpinBox = new QDoubleSpinBox(this);
    xSpinBox->setStyleSheet(spinBoxStyle);
    xSpinBox->setRange(-999, 999);

    ySpinBox = new QDoubleSpinBox(this);
    ySpinBox->setRange(-999, 999);
    ySpinBox->setStyleSheet(spinBoxStyle);

    widthSpinBox = new QDoubleSpinBox(this);
    widthSpinBox->setRange(-999, 999);
    widthSpinBox->setStyleSheet(spinBoxStyle);
    widthSpinBox->setEnabled(false);
    
    heightSpinBox = new QDoubleSpinBox(this);
    heightSpinBox->setRange(-999, 999);
    heightSpinBox->setStyleSheet(spinBoxStyle);
    heightSpinBox->setEnabled(false);

    
    //QHBoxLayout* upperLayout = new QHBoxLayout();

    //upperLayout->addWidget(nameLabel);
    //upperLayout->addWidget(nameLineEdit);

    QGridLayout* lowerLayout = new QGridLayout();

    lowerLayout->addWidget(xLabel, 0, 0);
    lowerLayout->addWidget(xSpinBox, 0, 1);
    lowerLayout->addWidget(yLabel, 1, 0);
    lowerLayout->addWidget(ySpinBox, 1, 1);

    lowerLayout->addWidget(widthLabel, 0, 2);
    lowerLayout->addWidget(widthSpinBox, 0, 3);
    lowerLayout->addWidget(heightLabel, 1, 2);
    lowerLayout->addWidget(heightSpinBox, 1, 3);

    //QVBoxLayout* mainLayout = new QVBoxLayout(this);
    //mainLayout->addLayout(upperLayout);
    //mainLayout->addLayout(lowerLayout);

    QWidget* wrapWidget = new QWidget();
    //wrapWidget->setLayout(mainLayout);
    wrapWidget->setLayout(lowerLayout);
    
    QHBoxLayout* stubLayout = new QHBoxLayout();
    stubLayout->setContentsMargins(0,0,0,0);
    stubLayout->addWidget(wrapWidget);

    setLayout(stubLayout);
    setEnabled(false);
    
    setMinimumWidth(290);
    setMaximumWidth(290);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}
