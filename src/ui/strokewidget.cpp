
#include <QtGui>

#include "strokewidget.h"

StrokeWidget::StrokeWidget(QWidget* parent)
    : QWidget(parent)
{
    QString mainStyle = "\n"
            " QWidget{\n"
            "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(80, 80, 80, 255))  ;\n"
            " }\n"
            "";

    QString labelStyle="background-color: transparent;\n"
            "color: white;\n"
            "";

    QString spinBoxStyle = "background: rgb(46,46,46);\n"
        #ifdef Q_OS_WIN
            "border: 1px solid gray;\n"
        #endif
            "color:white"
            "";

    setStyleSheet(mainStyle);

    QSpinBox* strokeSpinBox = new QSpinBox();
    strokeSpinBox->setRange(1,40);
    strokeSpinBox->setStyleSheet(spinBoxStyle);

    QLabel* stroke = new QLabel(tr("Stroke"));
    stroke->setStyleSheet(labelStyle);


    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->addWidget(stroke);
    mainLayout->addWidget(strokeSpinBox);

    QWidget* wrapWidget = new QWidget();
    wrapWidget->setLayout(mainLayout);

    QHBoxLayout* stubLayout = new QHBoxLayout();
    stubLayout->setContentsMargins(0,0,0,0);
    stubLayout->addWidget(wrapWidget);

    setLayout(stubLayout);

    setMinimumWidth(290);
    setMaximumWidth(290);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    connect(strokeSpinBox,SIGNAL(valueChanged(int))
            , this, SIGNAL(strokeValueChnge(int)));

}

void StrokeWidget::setupUi()
{

}
