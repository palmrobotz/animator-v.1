#include <QtGui>
#include <QtDebug>
#include <QGraphicsItem>

#include "mygraphicsview.h"
#include "../graphicsitem/mygraphicsobject.h"
#include "../graphicsitem/rectgraphicsobjectbuilder.h"
#include "../graphicsitem/trianglegraphicsobjectbuilder.h"
#include "../graphicsitem/ellipsegraphicsobjectbuilder.h"
#include "../graphicsitem/lineGraphicsItem.h"
#include "../graphicsitem/brushGraphicsItem.h"
#include "uioperation.h"

MyGraphicsView::MyGraphicsView(AnimationScene* scene, QWidget* parent)
    : QGraphicsView(parent)
    , tool(Animator::None)
    , mCurrentMode(DRAWABLE)
    , animationScene(scene)
    , mGlobalParent(0)
{
    QGraphicsScene* graphicsScene = animationScene->graphicsScene();
    
    setScene(graphicsScene);
    setRenderHint(QPainter::Antialiasing);
    setDragMode(QGraphicsView::RubberBandDrag);
    setWindowFlags(Qt::FramelessWindowHint);
    setStyleSheet("background: transparent");

    graphicsScene->setProperty("notifier", QVariant::fromValue(NotifierPointer(&notifier)));
    connect(&notifier, SIGNAL(notification(QGraphicsItem::GraphicsItemChange)), SLOT(graphicsItemUpdate(QGraphicsItem::GraphicsItemChange)));
    connect(graphicsScene, SIGNAL(selectionChanged()), SLOT(sceneSelectionChanged()));
}

MyGraphicsView::~MyGraphicsView()
{
    // FIXME: find any reason to support this (this come from stackoverflow notify pattern)
    animationScene->graphicsScene()->disconnect(this);
}

MyGraphicsView::Mode MyGraphicsView::getMode()
{
    return mCurrentMode;
}

void MyGraphicsView::setMode(MyGraphicsView::Mode mode)
{
    mCurrentMode = mode;
}

void MyGraphicsView::toggleMode()
{
    if (mCurrentMode == DRAWABLE)
    {
        mCurrentMode = NOTDRAWABLE;
    }
    else
    {
        mCurrentMode = DRAWABLE;
    }
}

MyAbstractGraphicsObject* MyGraphicsView::globalParent()
{
    return mGlobalParent;
}

void MyGraphicsView::setGlobalParent(MyAbstractGraphicsObject* parent)
{
    // If there has been a global parent before and we want to clear it
    if (mGlobalParent == parent)
    {
        qDebug() << "Class - MyGraphicsView Func - setGlobalParent :: mGlobalParent == parent";
    }
    else if (parent == 0 && mGlobalParent != 0)
    {
        animationScene->graphicsScene()->removeItem(mGlobalParent);
        mGlobalParent = 0;
    }
    else
    {
        mGlobalParent = parent;
        if (mGlobalParent != 0)
        {
            animationScene->graphicsScene()->addItem(mGlobalParent);
        }
    }
}

void MyGraphicsView::setGraphicsBrush(QBrush brush)
{
    brushGraphic = brush;
}

void MyGraphicsView::setGraphicsPen(QPen pen)
{
    penGraphic = pen;
}

void MyGraphicsView::setTool(Animator::Tool selectedTool)
{
    tool = selectedTool;
}

void MyGraphicsView::graphicsItemUpdate(QGraphicsItem::GraphicsItemChange change)
{
    /*if (graphicsScene.selectedItems().isEmpty())
    {
        return;
    }
    
    // Calculate the centroid point
    QPointF centroid(0.0, 0.0);
    qreal area = 0.0;
    foreach (QGraphicsItem* item, graphicsScene.selectedItems())
    {
        const QRectF r = item->boundingRect();
        const qreal a = r.width() * r.height();
        centroid += item->pos() * a;
        area += a;
    }
    centroid /= area;
    
    qDebug() << "Centroid is at " << centroid.x() << "," <<centroid.y();*/
    if (animationScene->graphicsScene()->selectedItems().isEmpty())
    {
        return;
    }
    
    emit selectedObjectChanged(animationScene->graphicsScene()->selectedItems(), change);
}

void MyGraphicsView::mousePressEvent(QMouseEvent *event)
{
    startPoint = mapToScene(event->pos());
    
    if (tool == Animator::None)
    {
        QGraphicsView::mousePressEvent(event);
        return;
    }
    
    if (mCurrentMode == NOTDRAWABLE)
    {
        return;
    }
    
    if (tool == Animator::Rectangle)
    {
        itemBuilder = new RectGraphicsObjectBuilder(brushGraphic, penGraphic);
        itemBuilder->setPos(startPoint);
        animationScene->graphicsScene()->addItem(itemBuilder);
        return;
    }
    else if (tool == Animator::Triangle)
    {
        itemBuilder = new TriangleGraphicsObjectBuilder(brushGraphic, penGraphic);
        itemBuilder->setPos(startPoint);
        animationScene->graphicsScene()->addItem(itemBuilder);
        return;
    }
    else if (tool == Animator::Ellipse)
    {
        itemBuilder = new EllipseGraphicsObjectBuilder(brushGraphic, penGraphic);
        itemBuilder->setPos(startPoint);
        animationScene->graphicsScene()->addItem(itemBuilder);
        return;
    }
    else if (tool == Animator::Line)
    {
        itemBuilder = new LineGraphicsObjectBuilder(brushGraphic, penGraphic);
        itemBuilder->setPos(startPoint);
        animationScene->graphicsScene()->addItem(itemBuilder);
        return;
    }
    else if (tool == Animator::Brush)
    {
        itemBuilder = new BrushGraphicsItem(brushGraphic, penGraphic);
        itemBuilder->setPos(startPoint);
        animationScene->graphicsScene()->addItem(itemBuilder);
        return;
    }
}

void MyGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    endPoint = mapToScene(event->pos());
    //qDebug() << "End Point is " << event->pos() << " map to " << endPoint;
    
    if (tool == Animator::None)
    {
        QGraphicsView::mouseMoveEvent(event);
        return;
    }
    
    if (mCurrentMode == NOTDRAWABLE)
    {
        return;
    }
    
    if (tool == Animator::Rectangle || tool == Animator::Triangle || tool == Animator::Ellipse || tool == Animator::Line || tool == Animator::Brush)
    {
        itemBuilder->newMousePointEvent(endPoint);
        return;
    }
}

void MyGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (tool == Animator::None)
    {
        QGraphicsView::mouseReleaseEvent(event);
        return;
    }
    
    if (mCurrentMode == NOTDRAWABLE)
    {
        return;
    }
    
    if (tool == Animator::Rectangle || tool == Animator::Triangle || tool == Animator::Ellipse || tool == Animator::Line || tool == Animator::Brush)
    {
        QGraphicsScene* currentScene = animationScene->graphicsScene();
        currentScene->removeItem(itemBuilder);
        
        //qDebug() << "Class - MyGraphicsView Func - mouseReleaseEvent :: Child size : " << mGlobalParent->childItems().size();
        
        // Create the object and with the global parent as the parent
        MyAbstractGraphicsObject* object = itemBuilder->createGraphicsObject(/*mGlobalParent*/);

        emit newObjectCreated(object);
        return;
    }
}

void MyGraphicsView::sceneSelectionChanged()
{
    emit selectionChanged(animationScene->graphicsScene()->selectedItems());
}
