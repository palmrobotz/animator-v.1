#ifndef GRAPHICSITEMLIBWIDGET_H
#define GRAPHICSITEMLIBWIDGET_H

#include <QWidget>
#include <QToolButton>

class QTreeWidget;
class QPushButton;

class QGraphicsView;
class QGraphicsScene;
class QTreeWidgetItem;
class MyGraphicsObject;

// TODO: Add folder and edit name functionality
class GraphicsItemLibWidget : public QWidget
{
    Q_OBJECT

public:
    GraphicsItemLibWidget(QWidget* parent = 0);

signals:
    /* Emit to notify when user double click on an item
     * Use mainly with preview window to go into close up mode (emit from double click) */
    void itemDoubleClickChanged(MyGraphicsObject* item);
    /* Emit when single click */
    void itemClickedChanged(MyGraphicsObject* item);
    /*  Emit when useItemButton is press */
    void useItemClick();
    
public slots:
    void insertItem();
    void removeItem();
    //void addFolder();
    void itemSelectionChanged();
    void itemDoubleClicked(QTreeWidgetItem * item, int column);
    
    void stopCloseUp();
    /* Update preview scene this method will clone scale and add this item to the preview scene Sent null will clear the scene*/
    void updatePreview(MyGraphicsObject* item = 0);
    
private:
    void setupUi();
    
    int numOfItem;
    bool mCloseUp;
    
    QGraphicsView* previewView;
    QGraphicsScene* previewScene;
    
    QTreeWidget* itemTreeWidget;
    
    QToolButton* addItemButton;
    QToolButton* deleteItemButton;
    QToolButton* useItemButton;
    //QPushButton* addFolderButton;
};

#endif // GRAPHICSITEMLIBWIDGET_H
