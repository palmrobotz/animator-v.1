#ifndef MYPREVIEWAREA_H
#define MYPREVIEWAREA_H

#include <QWidget>

class MyGraphicsView;
class MyGraphicsObject;
class AnimationScene;
class QGraphicsScene;

class QToolBar;
class QToolButton;
class QButtonGroup;

// TODO: Add ruler and click to open functionality
class MyPreviewArea : public QWidget
{
    Q_OBJECT

public:
    MyPreviewArea(AnimationScene* animationScene, QWidget* parent = 0);

    MyGraphicsView* graphicsView() const;

signals:
    /* Emit when start and stop close up */
    void closeUpStarted(MyGraphicsObject *);
    void closeUpEnded(MyGraphicsObject *);
    
public slots:
    /* Start close up mode and show tool bar */
    void startCloseUp();
    /* End close up mode and hide tool bar */
    void endCloseUp();
    /* Set new close up item automatically show tool bar */
    void setCloseUpItem(MyGraphicsObject* item);
    // TODO: add set preview item
    
private:
    void setupUi();
    void createToolbar();
    
    MyGraphicsView* mGraphicsView;
    
    QToolBar* toolbar;
    QButtonGroup* buttonGroup;
    QToolButton* previewButton;
    QToolButton* closeupButton;
    
    /* Current animation scene */
    AnimationScene* currentScene;
    /* Scene use for close up mode */
    QGraphicsScene* closeupScene;
    /* Current close up graphics item */
    MyGraphicsObject* closeupItem;
};

#endif // MYPREVIEWAREA_H
