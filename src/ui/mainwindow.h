#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QBrush>
#include <QPen>

#include "../animator.h"

class AnimationScene;
class MyGraphicsView;
class MyPreviewArea;
class TimeLine;

class QGraphicsScene;
class QAction;
class QUndoGroup;
class QAbstractButton;
class QToolButton;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0, QFlags<Qt::WindowType> flags = 0);

    enum DockWidget { Info, Color, Swatch, History, Stroke, Timeline };

signals:
    /* Emit to notify other widget about the tool change */
    void toolChanged(Animator::Tool selectedTool);
    void brushChange(QBrush brush);
    void penChange(QPen pen);

private slots:
    /* Listen to the toolbar button press */
    void toolbarActionSelected(QAbstractButton* button);
    
    /* Menubar respond */
    void exportHTML5();
    
    /* Turn dockwidget on or off */
    void setInfoDockVisible(bool b);
    void setColorDockVisible(bool b);
    void setSwatchDockVisible(bool b);
    void setHistoryDockVisible(bool b);
    void setStrokeDockVisible(bool b);
    void setTimelineDockVisible(bool b);
    void setDockVisible(DockWidget dock, bool b);
    
    void setBrushBuffer(QBrush brushColor);
    void setPenBuffer(QPen penColor);
    void changePenWidth(int size);
    
private:
    /* Call by the constructor to create the UI */
    void createMenus();
    void createToolBars();
    void createDockWidgets();
    void createStatusBar();

    QPen pen;

    /* Each widget instance */
    //MyGraphicsView* graphicsView;
    MyPreviewArea* previewArea;
    TimeLine* timeline;
    
    QAction* newFileAction;
    QAction* openFileAction;
    QAction* saveFileAction;
    QAction* exportFileAction;
    
    QAction* undoAction;
    QAction* redoAction;
    
    QAction* infoAction;
    QAction* colorAction;
    QAction* swatchAction;
    QAction* historyAction;
    QAction* strokeAction;
    QAction* timelineAction;
    
    // the rest action here

    QDockWidget* infoDockWidget;
    QDockWidget* colorDockWidget;
    QDockWidget* swatchDockWidget;
    QDockWidget* historyDockWidget;
    QDockWidget* strokeDockWidget;
    QDockWidget* timeLineDockWidget;

    Animator::Tool tool;
    
    QToolButton* noneButton;
    QToolButton* rectButton;
    QToolButton* ellipseButton;
    QToolButton* penButton;
    QToolButton* brushButton;
    QToolButton* triangleButton;
    QToolButton* lineButton;

    /* Aplication undo group */
    QUndoGroup* undoGroup;
    
    /* Current animation scene contain instant of
     * - QGraphicsScene - QUndoStack -Timeline */
    AnimationScene* currentScene;
};

#endif // MAINWINDOW_H
