#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsItem>
#include <QGraphicsView>
#include <QBrush>
#include <QPen>
#include <QVector>
#include <QPointF>
#include <QRectF>

#include "mainwindow.h"
#include "../graphicsitem/myabstractgraphicsobject.h"
#include "../graphicsitem/rectgraphicsobjectbuilder.h"
#include "../graphicsitem/notifier.h"
#include "../core/animationscene.h"
#include "../animator.h"

class QPolygonF;
class QGraphicsItem;
class QGraphicsScene;
class GraphicsObjectBuilder;

class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit MyGraphicsView(AnimationScene* scene, QWidget *parent = 0);
    ~MyGraphicsView();

    enum Mode
    {
        DRAWABLE,
        NOTDRAWABLE
    };
    
    Mode getMode();
    
    // FIXME: Implement this
    void resizeScene(QRectF newSize);
    void resizeScene(qreal width, qreal height);

    /* Get/Set item to use as global parent for all new create object */
    MyAbstractGraphicsObject* globalParent();
    // Automatically add this item to current scene
    void setGlobalParent(MyAbstractGraphicsObject* parent);
    
signals:
    // Use with info widget
    void selectionChanged(QList<QGraphicsItem *> selectedObject);
    
    void selectedObjectChanged(QList<QGraphicsItem *> selectedObject, QGraphicsItem::GraphicsItemChange change);
    void newObjectCreated(MyAbstractGraphicsObject* object);
    
public slots:
    void setTool(Animator::Tool selectedTool);
    void setMode(Mode mode);
    void toggleMode();
    void sceneSelectionChanged();
    /* Got fired when the selection is changed or the position of all
     * selected object has changed (by the notify interface) */
    void graphicsItemUpdate(QGraphicsItem::GraphicsItemChange change);
    void setGraphicsBrush(QBrush brush);
    void setGraphicsPen(QPen pen);
    
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    
private:
    Animator::Tool tool;
    Mode mCurrentMode;
    
    // Use as a bounding box point for rectangle or ellipse
    QPointF startPoint;
    QPointF endPoint;
    // For use as anchor point in bezier curve
    QVector<QPointF> tmpPoint;
    QVector<QPointF> tmpControlPoint;
    
    // Item builder
    GraphicsObjectBuilder* itemBuilder;
    
    // Area of this movie
    QRectF sceneAreaRect;
    //QGraphicsRectItem* sceneAreaGraphicsItem;

    AnimationScene* animationScene;
    MyAbstractGraphicsObject* mGlobalParent;
    
    //QGraphicsScene graphicsScene;
    Notifier notifier;

    QBrush brushGraphic;
    QPen penGraphic;

};

#endif // MYGRAPHICSVIEW_H
