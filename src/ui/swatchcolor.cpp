#include <QtGui>
#include <QtAlgorithms>
#include <QDebug>

#include "swatchcolor.h"

SwatchColor::SwatchColor(QWidget *parent)
    : QWidget(parent), slotNumber(0)
{
    setStyleSheet(QString::fromUtf8("\n"
                                    " QWidget{\n"
                                    "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(80, 80, 80, 255))  ;\n"
                                    " }\n"
                                    ""));


    swatchLayout = new QGridLayout(this);

    createCombobox();

    importColor("Standard");
    createSlots(slotNumber);
    arrangeGrid(slotNumber);

    connectAllColor(slotNumber);
    QObject::connect(colorCombo,SIGNAL(currentIndexChanged(QString)),
                     this,SLOT(changeColorSet(QString)));

    QWidget* wrapWidget = new QWidget();
    wrapWidget->setLayout(swatchLayout);

    QHBoxLayout* stubLayout = new QHBoxLayout();
    stubLayout->setContentsMargins(0,0,0,0);
    stubLayout->addWidget(wrapWidget);

    setLayout(stubLayout);
    
    setMinimumWidth(290);
    setMaximumWidth(290);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void SwatchColor::importColor(QString filename)
{
    colorSet.clear();   //very very important to clear old color slot
    slot.clear();       //very very important to clear old color slot

    QVector<QString> textData;
    QString fianalFilename = ":/"+filename+".txt";
    QFile inputFile(fianalFilename);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        QString firstLine = in.readLine();
        slotNumber= firstLine.toInt();
        while(!in.atEnd())
        {
           QVector<int> rgbVal;
           QString line = in.readLine();
           QStringList splitLine = line.split(QRegExp("\\s"));

           QString r =splitLine.at(0);
           rgbVal.push_back(r.toInt());

           QString g =splitLine.at(1);
           rgbVal.push_back(g.toInt());

           QString b =splitLine.at(2);
           rgbVal.push_back(b.toInt());

           colorSet.push_back(rgbVal);
        }     
    }
    inputFile.close();

}

void SwatchColor::createSlots(int slotNumber)
{
    for (int i=0;i<slotNumber;i++)
    {
        slot.push_back(new ColorSlot());
    }
    setSwatchColor(slotNumber,colorSet);
}

void SwatchColor::changeColorSet(QString filename)
{
    foreach (ColorSlot* cs, slot)
    {
        swatchLayout->removeWidget(cs);
    }
    qDeleteAll(slot);
    slot.clear();
    colorSet.clear();
    
    importColor(filename);
    createSlots(slotNumber);
    arrangeGrid(slotNumber);
    connectAllColor(slotNumber);
}

void SwatchColor::arrangeGrid(int slotNumber)
{
#ifdef Q_OS_WIN
    swatchLayout->setSpacing(0);
#endif

#ifdef Q_WS_X11
    swatchLayout->setSpacing(0);
#endif

    swatchLayout->addWidget(colorCombo,0,0,1,16);
    
    int x=0;
    int y=0;
    for (int i=0;i<slotNumber;i++)
    {
        if (y < 16)
        {
            swatchLayout->addWidget(slot[i], x+1, y);
            y++;
        }
        if (y >= 16)
        {
            x++;
            y=0;
        }
    }

}

void SwatchColor::setSwatchColor(int slotNumber,QVector< QVector <int> > colorSet)
{
    for (int i=0;i<slotNumber;i++)
    {
        slot[i]->ColorSlot::setColor(colorSet[i][0],colorSet[i][1],colorSet[i][2]);
    }
}

void SwatchColor::createCombobox()
{
    colorCombo = new QComboBox(this);
    colorCombo->clear();
    colorCombo->insertItem(0,"Standard");
    colorCombo->insertItem(1,"Skintone");
    colorCombo->insertItem(2,"Pastel");

    colorCombo->setStyleSheet(QString::fromUtf8("background-color: rgb(46,46,46);color: white;"));

}

void SwatchColor::connectAllColor(int slotNumber)
{
    for(int i=0;i<slotNumber;i++)
    {
        connect(slot[i],SIGNAL(slotClick(int,int,int)),this,SIGNAL(swatchSlotClick(int,int,int)));
    }
}

/*---------------ColorSlot Class----------------*/

ColorSlot::ColorSlot(QPushButton *parent)
    :QPushButton(parent), rVal(255), gVal(255), bVal(255)
{
    this->setStyleSheet(QString::fromUtf8("background:rgb(255, 255, 255);\n"
                                             "border: 1px solid Black;\n"));
    
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    
    QObject::connect(this,SIGNAL(clicked()),this,SLOT(clickSendRGB()));
}

QSize ColorSlot::sizeHint() const
{
    return QSize(17, 15);
}

void ColorSlot::clickSendRGB()
{
    emit(slotClick(rVal,gVal,bVal));
}

void ColorSlot::setColor(int r,int g,int b)
{
    QString style;
    rVal = r;
    gVal = g;
    bVal = b;
    style.sprintf("background:rgb(%d, %d, %d);\n"
                  "border: 1px solid Black;\n",r,g,b);
    this->setStyleSheet(style);

}

/*---------------ColorSlot Class----------------*/
