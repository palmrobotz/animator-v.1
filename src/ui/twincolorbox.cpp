#include <QtGui>
#include <QDebug>
#include "twincolorbox.h"

TwinColorBox::TwinColorBox(QWidget *parent)
    : QWidget(parent),chosenBox(0)
    ,rBgTwin(0),gBgTwin(0),bBgTwin(0),aBgTwin(255)
    ,rStTwin(255),gStTwin(255),bStTwin(255),aStTwin(255)
{
    bgTemp = new QWidget(this);
    bgTemp->setGeometry(QRect(0, 0, 30, 30));
    bgTemp->setStyleSheet(QString::fromUtf8("background-image: url(://src/pic/greyWhite.jpg);\n"
                                            "border: 2px solid gray;\n"
                                            "border-radius: 4px"));

    stTemp = new QWidget(this);
    stTemp->setGeometry(QRect(15, 15, 30, 30));
    stTemp->setStyleSheet(QString::fromUtf8("background-image: url(://src/pic/greyWhite.jpg);\n"
                                            "border: 2px solid gray;\n"
                                            "border-radius: 4px"));

    bgColor = new QWidget(this);
    bgColor->setGeometry(QRect(0, 0, 30, 30));
    bgColor->setStyleSheet(QString::fromUtf8("background:rgba(0, 0, 0,255);\n"
                                       "border: 2px solid gray;\n"
                                       "border-radius: 4px"));
    stColor = new QWidget(this);
    stColor->setGeometry(QRect(15, 15, 30, 30));
    stColor->setStyleSheet(QString::fromUtf8("background:rgba(255, 255, 255,255);\n"
                         "border: 2px solid gray;\n"
                         "border-radius: 4px"));


    bgBox = new BgBox(this);
    bgBox->BgBox::setGeometry(QRect(0, 0, 30, 30));

    stBox = new StBox(this);
    stBox->StBox::setGeometry(QRect(15, 15, 30, 30));

    bgTemp->raise();
    bgColor->raise();
    bgBox->raise();

    connect(bgBox,SIGNAL(BgMouseClickEvent()),this,SLOT(raiseBG()));
    connect(stBox,SIGNAL(StMouseClickEvent()),this,SLOT(raiseST()));

    connect(bgBox,SIGNAL(BgClickR(int)),this,SIGNAL(twinClickR(int)));
    connect(bgBox,SIGNAL(BgClickG(int)),this,SIGNAL(twinClickG(int)));
    connect(bgBox,SIGNAL(BgClickB(int)),this,SIGNAL(twinClickB(int)));
    connect(bgBox,SIGNAL(BgClickA(int)),this,SIGNAL(twinClickA(int)));

    connect(stBox,SIGNAL(StClickR(int)),this,SIGNAL(twinClickR(int)));
    connect(stBox,SIGNAL(StClickG(int)),this,SIGNAL(twinClickG(int)));
    connect(stBox,SIGNAL(StClickB(int)),this,SIGNAL(twinClickB(int)));
    connect(stBox,SIGNAL(StClickA(int)),this,SIGNAL(twinClickA(int)));

}

void TwinColorBox::changeRTwin(int r)
{
    QString s;
    if(chosenBox==0)
    {
        rBgTwin=r;
        bgBox->BGchangeR(r);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rBgTwin,gBgTwin,bBgTwin,aBgTwin);
        bgColor->setStyleSheet(s);

        QBrush brush(QColor(rBgTwin,gBgTwin,bBgTwin,aBgTwin));
        emit bgBrushChange(brush);
    }
    else
    {
        rStTwin=r;
        stBox->StchangeR(r);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rStTwin,gStTwin,bStTwin,aStTwin);
        stColor->setStyleSheet(s);

        QPen pen;
        pen.setColor(QColor(rStTwin,gStTwin,bStTwin,aStTwin));
        emit stPenChange(pen);
    }
}

void TwinColorBox::changeGTwin(int g)
{
    QString s;
    if(chosenBox==0)
    {
        gBgTwin=g;
        bgBox->BGchangeG(g);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rBgTwin,gBgTwin,bBgTwin,aBgTwin);
        bgColor->setStyleSheet(s);
        QBrush brush(QColor(rBgTwin,gBgTwin,bBgTwin,aBgTwin));
        emit bgBrushChange(brush);
    }
    else
    {
        gStTwin=g;
        stBox->StchangeG(g);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rStTwin,gStTwin,bStTwin,aStTwin);
        stColor->setStyleSheet(s);

        QPen pen;
        pen.setColor(QColor(rStTwin,gStTwin,bStTwin,aStTwin));
        emit stPenChange(pen);

    }
}

void TwinColorBox::changeBTwin(int b)
{
    QString s;
    if(chosenBox==0)
    {
        bBgTwin=b;
        bgBox->BGchangeB(b);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rBgTwin,gBgTwin,bBgTwin,aBgTwin);
        bgColor->setStyleSheet(s);
        QBrush brush(QColor(rBgTwin,gBgTwin,bBgTwin,aBgTwin));
        emit bgBrushChange(brush);
    }
    else
    {
        bStTwin=b;
        stBox->StchangeB(b);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rStTwin,gStTwin,bStTwin,aStTwin);
        stColor->setStyleSheet(s);

        QPen pen ;
        pen.setColor(QColor(rStTwin,gStTwin,bStTwin,aStTwin));
        emit stPenChange(pen);

    }
}

void TwinColorBox::changeATwin(int a)
{
    QString s;
    if(chosenBox==0)
    {
        aBgTwin=a;
        bgBox->BGchangeA(a);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rBgTwin,gBgTwin,bBgTwin,aBgTwin);
        bgColor->setStyleSheet(s);
        QBrush brush(QColor(rBgTwin,gBgTwin,bBgTwin,aBgTwin));
        emit bgBrushChange(brush);
    }
    else
    {
        aStTwin=a;
        stBox->StchangeA(a);
        s.sprintf("background:rgba(%d, %d, %d, %d);\n"
               "border: 2px solid gray;\n"
               "border-radius: 4px",rStTwin,gStTwin,bStTwin,aStTwin);
        stColor->setStyleSheet(s);

        QPen pen ;
        pen.setColor(QColor(rStTwin,gStTwin,bStTwin,aStTwin));
        emit stPenChange(pen);
    }
}

void TwinColorBox::raiseBG()
{
    //Move bgBox to front
    bgTemp->raise();
    bgColor->raise();
    bgBox->raise();

    //Indicate that Background Box has been chosen.
    chosenBox = 0;
}

void TwinColorBox::raiseST()
{
    //Move stBox to front
    stTemp->raise();
    stColor->raise();
    stBox->raise();

    //Indicate that Stroke Box has been chosen.
    chosenBox = 1;
}

//////////////////////////////////////////////////////////////

BgBox::BgBox(QWidget *parent)
    : QWidget(parent)
    ,bgR(0),bgG(0),bgB(0),bgA(255)
{
}

void BgBox::mousePressEvent(QMouseEvent * event)
{
    /*// store click position
    lastPoint = event->pos();
    qDebug()<<"clicked BG";
    // set the flag meaning "click begin"
    mouseMode = true;*/

    emit BgMouseClickEvent();
    emit BgClickR(bgR);
    emit BgClickG(bgG);
    emit BgClickB(bgB);
    emit BgClickA(bgA);
}

void BgBox::BGchangeR(int r)
{
    bgR=r;
}

void BgBox::BGchangeG(int g)
{
    bgG=g;
}

void BgBox::BGchangeB(int b)
{
    bgB=b;
}

void BgBox::BGchangeA(int a)
{
    bgA=a;
}

/*void BgBox:: mouseReleaseEvent ( QMouseEvent * event )
{    // check if cursor not moved since click beginning
    if ((mouseMode) && (event->pos() == lastPoint))
       {    // do something: for example emit Click signal
            //emit mouseClickEvent;
            qDebug()<<"emit BG";
            emit BgMouseClickEvent();
       }
}*/

//////////////////////////////////////////////////////////////

StBox::StBox(QWidget *parent)
    : QWidget(parent)
    ,stR(255),stG(255),stB(255),stA(255)
{
}

void StBox:: mousePressEvent ( QMouseEvent * event )
{
    /*// store click position
    lastPoint = event->pos();
    qDebug()<<"clicked ST";
    // set the flag meaning "click begin"
    mouseMode = true;*/

    emit StMouseClickEvent();
    emit StClickR(stR);
    emit StClickG(stG);
    emit StClickB(stB);
    emit StClickA(stA);
}

void StBox::StchangeR(int r)
{
    stR=r;
}

void StBox::StchangeG(int g)
{
    stG=g;
}

void StBox::StchangeB(int b)
{
    stB=b;
}

void StBox::StchangeA(int a)
{
    stA=a;
}

/*void StBox:: mouseReleaseEvent ( QMouseEvent * event )
{    // check if cursor not moved since click beginning
    if ((mouseMode) && (event->pos() == lastPoint))
       {    // do something: for example emit Click signal
            //emit mouseClickEvent;
            qDebug()<<"emit ST";
            emit StMouseClickEvent();
       }
}*/

