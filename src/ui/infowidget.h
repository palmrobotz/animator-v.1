
#ifndef INFOWIDGET_H
#define INFOWIDGET_H

#include <QWidget>

class QGraphicsItem;
class QLineEdit;
class QDoubleSpinBox;

/* The info dock widget */
class InfoWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit InfoWidget(QWidget* parent = 0);

public slots:
    void objectChanged(QList<QGraphicsItem *> selectedObject);
    /* Respond to spinbox */
    void xValueChanged(double d);
    void yValueChanged(double d);
    void widthValueChanged(double d);
    void heightValueChanged(double d);
    
private:
    void setupUi();
    
    QLineEdit* nameLineEdit;
    QDoubleSpinBox* xSpinBox;
    QDoubleSpinBox* ySpinBox;
    QDoubleSpinBox* widthSpinBox;
    QDoubleSpinBox* heightSpinBox;
    
    // Use for compare with new value to find delta
    QPointF oldPoint;
    QRectF oldSize;
    
    //QGraphicsItem* selectingItem;
    QList<QGraphicsItem *> selectingItem;
};

#endif // INFOWIDGET_H
