#ifndef TWINCOLORBOX_H
#define TWINCOLORBOX_H

#include <QWidget>
#include <QPoint>
#include <QBrush>
#include <QPen>

class BgBox: public QWidget
{
    Q_OBJECT

public:
    BgBox(QWidget *parent = 0);
    void BGchangeR(int r);
    void BGchangeG(int g);
    void BGchangeB(int b);
    void BGchangeA(int a);

signals:
   void BgMouseClickEvent();
   void BgClickR(int r);
   void BgClickG(int g);
   void BgClickB(int b);
   void BgClickA(int a);

protected:
    void mousePressEvent(QMouseEvent * event );
   // void mouseReleaseEvent(QMouseEvent *event);

private:
    int bgR;
    int bgG;
    int bgB;
    int bgA;

private:
    QPoint lastPoint;
    bool mouseMode;
};

class StBox: public QWidget
{
    Q_OBJECT

public:
    StBox(QWidget *parent = 0);
    void StchangeR(int r);
    void StchangeG(int g);
    void StchangeB(int b);
    void StchangeA(int a);

private:
    int stR;
    int stG;
    int stB;
    int stA;

signals:
   void StMouseClickEvent();
   void StClickR(int r);
   void StClickG(int g);
   void StClickB(int b);
   void StClickA(int a);


protected:
    void mousePressEvent(QMouseEvent * event );
    //void mouseReleaseEvent(QMouseEvent *event);

private:
    QPoint lastPoint;
    bool mouseMode;
};

class TwinColorBox : public QWidget
{
    Q_OBJECT
signals:
    void  twinClickR(int r);
    void  twinClickG(int g);
    void  twinClickB(int b);
    void  twinClickA(int a);
    void  bgBrushChange(QBrush brush);
    void  stPenChange(QPen pen);

public slots:
    void raiseBG();
    void raiseST();
    void changeRTwin(int r);
    void changeGTwin(int r);
    void changeBTwin(int r);
    void changeATwin(int r);

public:
    TwinColorBox(QWidget *parent = 0);
    BgBox* bgBox;
    StBox* stBox;
    QWidget* bgColor;
    QWidget* stColor;
    QWidget* bgTemp;
    QWidget* stTemp;
    int chosenBox; // 0=Background Box, 1=Stroke Box

    int rBgTwin;
    int gBgTwin;
    int bBgTwin;
    int aBgTwin;

    int rStTwin;
    int gStTwin;
    int bStTwin;
    int aStTwin;

};


#endif // TWINCOLORBOX_H
