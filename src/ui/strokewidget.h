#ifndef STROKEWIDGET_H
#define STROKEWIDGET_H

#include <QWidget>

class StrokeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StrokeWidget(QWidget* parent = 0);

private:
    void setupUi();

signals:
    void strokeValueChnge(int value);
};

#endif // STROKEWIDGET_H
