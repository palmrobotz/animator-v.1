#include <QtGui>

#include "mypreviewarea.h"
#include "mygraphicsview.h"
#include "../graphicsitem/mygraphicsobject.h"
#include "../core/animationscene.h"

MyPreviewArea::MyPreviewArea(AnimationScene* animationScene, QWidget* parent)
    : QWidget(parent, 0)
    , currentScene(animationScene)
    , closeupScene(new QGraphicsScene(this))
    , closeupItem(0)
{
    setupUi();
    
    // Use preview button in the toolbar to dismiss close up mode
    connect(previewButton, SIGNAL(released()), this, SLOT(endCloseUp()));
}

MyGraphicsView* MyPreviewArea::graphicsView() const
{
    return mGraphicsView;
}

void MyPreviewArea::setupUi()
{
    QString buttonStyle = "QToolButton{\n"
            "color:white;\n"
            "background: rgb(46,46,46);\n"
            "}\n"
            "QToolButton[enabled=\"false\"] {\n"
            "	color: gray;\n"
            "background: rgb(60,60,60);\n"
            "}";

    // Set closeup scene background to white
    closeupScene->setBackgroundBrush(QBrush(Qt::white));
    
    previewButton = new QToolButton();
    previewButton->setText("Preview");
    previewButton->setStyleSheet(buttonStyle);
    previewButton->setCheckable(true);
    previewButton->setAutoRaise(false);
    
    closeupButton = new QToolButton();
    closeupButton->setText("Item");
    closeupButton->setStyleSheet(buttonStyle);
    closeupButton->setCheckable(true);
    closeupButton->setChecked(true);
    closeupButton->setAutoRaise(false);
    
    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(previewButton);
    buttonGroup->addButton(closeupButton);
    
    toolbar = new QToolBar(this);
    toolbar->addWidget(previewButton);
    toolbar->addWidget(closeupButton);
    toolbar->setVisible(false);
    
    mGraphicsView = new MyGraphicsView(currentScene);
    mGraphicsView->setStyleSheet("background-color: rgb(30, 30, 30);");
    
    QVBoxLayout* stubLayout = new QVBoxLayout();
    stubLayout->setContentsMargins(0, 0, 0, 0);
    stubLayout->addWidget(toolbar);
    stubLayout->addWidget(mGraphicsView);
    
    setLayout(stubLayout);
}

void MyPreviewArea::startCloseUp()
{
    // Check if close up item has been set
    if (closeupItem == 0)
    {
        qDebug() << "MyPreviewArea: Show toolbar without any close up item";
        toolbar->setVisible(false);
        return;
    }
    
    // Set up and show toolbar
    closeupButton->setText(closeupItem->itemName());
    closeupButton->setChecked(true);
    toolbar->setVisible(true);
    
    // Set global parent
    mGraphicsView->setGlobalParent(closeupItem);
    //mGraphicsView->centerOn(0, 0);
    
    // Set scene and view
    closeupScene->clear();
    closeupScene->addItem(closeupItem);
    mGraphicsView->setScene(closeupScene);
    
    emit closeUpStarted(closeupItem);
}

void MyPreviewArea::endCloseUp()
{
    // hide toolbar
    toolbar->setVisible(false);
    
    // reset scene
    closeupScene->removeItem(closeupItem);
    mGraphicsView->setScene(currentScene->graphicsScene());
    // set center back
    
    // clean global parent
    mGraphicsView->setGlobalParent(0);
    
    emit closeUpEnded(closeupItem);
}

void MyPreviewArea::setCloseUpItem(MyGraphicsObject* item)
{
    closeupItem = item;
    
    startCloseUp();
}
