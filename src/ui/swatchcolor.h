#ifndef SWATCHCOLOR_H
#define SWATCHCOLOR_H

#include <QtGui>

class ColorSlot : public QPushButton
{
    Q_OBJECT

signals:
    void slotClick(int r,int g,int b);
public slots:
    void clickSendRGB();

public:
    ColorSlot(QPushButton *parent=0);
    void setColor(int r,int g,int b);
    
    QSize sizeHint() const;
    
private:
    int rVal;
    int gVal;
    int bVal;
};

class SwatchColor : public QWidget
{
    Q_OBJECT

signals:
    void swatchSlotClick(int r, int g, int b);

public:
    SwatchColor(QWidget *parent = 0);
    
    void setSwatchColor(int slotNumber,QVector< QVector <int> > colorSet);
    void createSlots(int slotNumber);
    void arrangeGrid(int slotNumber);
    void importColor(QString filename);
    void createCombobox();
    void connectAllColor(int slotNumber);

public slots:
    void changeColorSet(QString filename);

private:
    int slotNumber;
    
    QVector<ColorSlot*> slot;
    QGridLayout *swatchLayout;
    QVector<QVector<int> > colorSet;
    QComboBox* colorCombo;
    QVBoxLayout* finalLayout;
};


#endif // SWATCHCOLOR_H
