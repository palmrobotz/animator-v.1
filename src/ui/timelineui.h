#ifndef TIMELINEUI_H
#define TIMELINEUI_H

#include <QtGui>

class TimeLine;
class TimelinePlayHead;
class AnimationItem;

class TimeLineHelper
{
public:
    static int xToTime(int x)
    {
        return (x/10) * 200;
    }
    
    static int timeToX(int time)
    {
        return (time/200) * 10;
    }
};

class TimeLineUI : public QWidget
{
    Q_OBJECT

public:
    TimeLineUI(TimeLine* timeline, QWidget *parent=0);
    
public slots:
    // Got update from playhead and scene selection
    void playHeadPosChanged();
    void activeLayerChanged();
    // Connect with invalidate signal to redraw when need
    void updateTimeLine();
    
    void play();
    void stop();
    
private:
    void addLayer(int id);
    void addBlock(int layer, AnimationItem* item);
    
    QGraphicsScene* scene;
    
    QToolButton* playButton;
    QToolButton* pauseButton;
    
    QPropertyAnimation* propertyAnimation;
    
    TimeLine* mTimeline;
    QScrollBar* scrollBar;
    TimelinePlayHead* playHead;
};

//////////////////////////////////////////////////////////////
//////          TimeLineView                            //////
//////////////////////////////////////////////////////////////
class TimeLineView : public QGraphicsView
{
    Q_OBJECT
public:
    TimeLineView(QGraphicsScene* scene,QGraphicsView* parent=0);

protected:
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    QGraphicsScene* sceneV;
};


//////////////////////////////////////////////////////////////
//////          LayerItem                               //////
//////////////////////////////////////////////////////////////
class LayerItem : public QGraphicsRectItem
{

public:
    LayerItem(int layerNumber, int width, QGraphicsRectItem* parent =0);

    void updateWidth();
    int layerId();

    enum { Type = UserType + 2 };
    int type() const { return Type; }
    
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    int mLayerId;
};

//////////////////////////////////////////////////////////////
//////          TimeLineItem                            //////
//////////////////////////////////////////////////////////////

class TimelineItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    
public:
    TimelineItem(int layer, int startTime, int endTime, QGraphicsItem *parent=0);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    
    void addCircle(int t); // t as time in ms

    enum { Type = UserType + 1 };
    int type() const { return Type; }
    
    int layerId() const;
    int startTime() const;
    void allowYchange(bool condition);

    QRectF rect() const;
    void setRect(qreal x, qreal y, qreal width, qreal height);
    
    QRectF boundingRect() const;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);

signals:
    void widthChange(int startTime, double x);
    void xChanged(int startTime, double x);

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    int mLayerId;
    int mStartTime;
    bool yChangeAble;
    bool edgeMoveMode;

    QList<int> circlePosX;
    
    QPointF beforePoint;
    QPointF afterPoint;
    QPointF yAbsolutePoint;
    
    QRectF mRect;
};


//////////////////////////////////////////////////////////////
//////          TimeLinePlayHead                        //////
//////////////////////////////////////////////////////////////
class TimelinePlayHead : public QGraphicsObject
{
public:
    TimelinePlayHead(int time, QGraphicsItem* parent =0);
    enum { Type = UserType + 3 };
protected:
    int type() const
    {
       return Type;
    }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const ;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    double height;
    bool moveMode;
};

#endif // TIMELINEUI_H




//////////////////////////////////////////////////////////////
//////          CircleItem                              //////
//////////////////////////////////////////////////////////////
/*class CircleItem : public QGraphicsEllipseItem
{
public:
    CircleItem(QGraphicsEllipseItem* parent=0);
    enum { Type = UserType + 4 };
protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    int type() const
    {
       return Type;
    }
};
*/
