#include <QtGui>
#include <QDebug>

#include "colormixer.h"
#include "twincolorbox.h"

ColorMixer::ColorMixer(QWidget *parent)
    : QWidget(parent)
    , rVal(0),gVal(0),bVal(0),aVal(0)
{
    QString mainStyle = "\n"
            " QWidget{\n"
            "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(70, 70, 70, 255), stop:1 rgba(80, 80, 80, 255))  ;\n"
            " }\n"
            "";

    QString labelStyle="background-color: transparent;\n"
            "color: white;\n"
            "";

    QString spinBoxStyle = "background: rgb(46,46,46);\n"
        #ifdef Q_OS_WIN
            "border: 1px solid gray;\n"
        #endif
            "color:white"
            "";

    QString RsliderStyle =" QSlider::groove:horizontal {\n"
            "     border: 1px solid #999999;\n"
            "     height: 8px; \n"
            "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(0, 0, 0), stop:1 rgb(255, 0, 0));\n"
            "     margin: 2px 0;\n"
            " }\n"
            "\n"
            " QSlider::handle:horizontal {\n"
            "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
            "     border: 1px solid #5c5c5c;\n"
            "     width: 8px;\n"
            "     margin: -2px 0;\n"
            "     border-radius: 3px;\n"
            " }";

    QString GsliderStyle = " QSlider::groove:horizontal {\n"
            "     border: 1px solid #999999;\n"
            "     height: 8px; \n"
            "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(0, 0, 0), stop:1 rgb(0, 255, 0));\n"
            "     margin: 2px 0;\n"
            " }\n"
            "\n"
            " QSlider::handle:horizontal {\n"
            "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
            "     border: 1px solid #5c5c5c;\n"
            "     width: 8px;\n"
            "     margin: -2px 0;\n"
            "     border-radius: 3px;\n"
            " }";

    QString BsliderStyle =" QSlider::groove:horizontal {\n"
            "     border: 1px solid #999999;\n"
            "     height: 8px; \n"
            "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(0, 0, 0), stop:1 rgb(0, 0, 255));\n"
            "     margin: 2px 0;\n"
            " }\n"
            "\n"
            " QSlider::handle:horizontal {\n"
            "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
            "     border: 1px solid #5c5c5c;\n"
            "     width: 8px;\n"
            "     margin: -2px 0;\n"
            "     border-radius: 3px;\n"
            " }";

    QString AsliderStyle =" QSlider::groove:horizontal {\n"
            "     border: 1px solid #999999;\n"
            "     height: 8px; \n"
            "     background: rgb(50,50,50);\n"
            "     margin: 2px 0;\n"
            " }\n"
            "\n"
            " QSlider::handle:horizontal {\n"
            "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
            "     border: 1px solid #5c5c5c;\n"
            "     width: 8px;\n"
            "     margin: -2px 0;\n"
            "     border-radius: 3px;\n"
            " }";

    setStyleSheet(mainStyle);


    QLabel *red = new QLabel("Red");
    red->setStyleSheet(labelStyle);
    QLabel *green = new QLabel("Green");
    green->setStyleSheet(labelStyle);
    QLabel *blue = new QLabel("Blue");
    blue->setStyleSheet(labelStyle);
    QLabel *alpha = new QLabel("Opacity");
    alpha->setStyleSheet(labelStyle);

    twinColor = new TwinColorBox(this);
    twinColor->setMinimumSize(QSize(45, 45));
    twinColor->setMaximumSize(QSize(45, 45));

    // Red Mixer
    QSpinBox *Rspiner = new QSpinBox(this);
    Rspiner->setStyleSheet(spinBoxStyle);
    Rslider = new QSlider(Qt::Horizontal);
    Rslider->setStyleSheet(RsliderStyle);
    Rspiner->setRange(0,255);
    Rslider->setRange(0,255);
    QObject::connect(Rspiner,SIGNAL(valueChanged(int)),
                     Rslider,SLOT(setValue(int)));
    QObject::connect(Rslider,SIGNAL(valueChanged(int))
                     ,Rspiner,SLOT(setValue(int)));
    QObject::connect(Rspiner,SIGNAL(valueChanged(int))
                     ,this,SLOT(redChanger(int)));
    QObject::connect(Rspiner,SIGNAL(valueChanged(int))
                     ,twinColor,SLOT(changeRTwin(int)));
    QObject::connect(twinColor,SIGNAL(twinClickR(int))
                     ,Rspiner,SLOT(setValue(int)));

    // Green Mixer
    QSpinBox *Gspiner = new QSpinBox(this);
    Gspiner->setStyleSheet(spinBoxStyle);
    Gslider = new QSlider(Qt::Horizontal);
    Gslider->setStyleSheet(GsliderStyle);
    Gspiner->setRange(0,255);
    Gslider->setRange(0,255);
    QObject::connect(Gspiner,SIGNAL(valueChanged(int)),
                     Gslider,SLOT(setValue(int)));
    QObject::connect(Gslider,SIGNAL(valueChanged(int))
                     ,Gspiner,SLOT(setValue(int)));
    QObject::connect(Gspiner,SIGNAL(valueChanged(int))
                     ,this,SLOT(greenChanger(int)));
    QObject::connect(Gspiner,SIGNAL(valueChanged(int))
                     ,twinColor,SLOT(changeGTwin(int)));
    QObject::connect(twinColor,SIGNAL(twinClickG(int))
                     ,Gspiner,SLOT(setValue(int)));

    // Blue Mixer
    QSpinBox *Bspiner = new QSpinBox(this);
    Bspiner->setStyleSheet(spinBoxStyle);
    Bslider = new QSlider(Qt::Horizontal);
    Bslider->setStyleSheet(BsliderStyle);
    Bspiner->setRange(0,255);
    Bslider->setRange(0,255);
    QObject::connect(Bspiner,SIGNAL(valueChanged(int)),
                     Bslider,SLOT(setValue(int)));
    QObject::connect(Bslider,SIGNAL(valueChanged(int)),
                     Bspiner,SLOT(setValue(int)));
    QObject::connect(Bspiner,SIGNAL(valueChanged(int))
                     ,this,SLOT(blueChanger(int)));
    QObject::connect(Bspiner,SIGNAL(valueChanged(int))
                     ,twinColor,SLOT(changeBTwin(int)));
    QObject::connect(twinColor,SIGNAL(twinClickB(int))
                     ,Bspiner,SLOT(setValue(int)));

    // Alpha Mixer
    QSpinBox *Aspiner = new QSpinBox(this);
    Aspiner->setStyleSheet(spinBoxStyle);
    Aslider = new QSlider(Qt::Horizontal);
    Aspiner->setRange(0,255);
    Aslider->setRange(0,255);
    Aslider->setStyleSheet(AsliderStyle);
    QObject::connect(Aspiner,SIGNAL(valueChanged(int)),
                     Aslider,SLOT(setValue(int)));
    QObject::connect(Aslider,SIGNAL(valueChanged(int)),
                     Aspiner,SLOT(setValue(int)));
    QObject::connect(Aspiner,SIGNAL(valueChanged(int))
                     ,this,SLOT(alphaChanger(int)));
    QObject::connect(Aspiner,SIGNAL(valueChanged(int))
                     ,twinColor,SLOT(changeATwin(int)));
    QObject::connect(twinColor,SIGNAL(twinClickA(int))
                     ,Aspiner,SLOT(setValue(int)));
    Aspiner->setValue(255);


    // Layout Set
    QGridLayout* AllLayout = new QGridLayout;
    AllLayout->addWidget(red,0,1);
    AllLayout->addWidget(Rslider,0,2);
    AllLayout->addWidget(Rspiner,0,3);
    AllLayout->addWidget(green,1,1);
    AllLayout->addWidget(Gslider,1,2);
    AllLayout->addWidget(Gspiner,1,3);
    AllLayout->addWidget(blue,2,1);
    AllLayout->addWidget(Bslider,2,2);
    AllLayout->addWidget(Bspiner,2,3);
    AllLayout->addWidget(alpha,3,1);
    AllLayout->addWidget(Aslider,3,2);
    AllLayout->addWidget(Aspiner,3,3);
    AllLayout->addWidget(twinColor,0,0,4,1);

    QWidget* wrapWidget = new QWidget();
    wrapWidget->setLayout(AllLayout);

    QHBoxLayout* stubLayout = new QHBoxLayout();
    stubLayout->setContentsMargins(0,0,0,0);
    stubLayout->addWidget(wrapWidget);

    setLayout(stubLayout);

    setMinimumWidth(290);
    setMaximumWidth(290);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

ColorMixer::~ColorMixer()
{

}

void ColorMixer::redChanger(int r)
{
    rVal=r;

    QString s;
    QString gSlider;
    QString bSlider;

    s.sprintf("background:rgba(%d,%d,%d,%d)", rVal, gVal, bVal, aVal);
    gSlider.sprintf(" QSlider::groove:horizontal {\n"
                   "     border: 1px solid #999999;\n"
                   "     height: 8px; \n"
                   "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(%d, 0, %d), stop:1 rgb(%d, 255, %d));\n"
                   "     margin: 2px 0;\n"
                   " }\n"
                   "\n"
                   " QSlider::handle:horizontal {\n"
                   "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
                   "     border: 1px solid #5c5c5c;\n"
                   "     width: 8px;\n"
                   "     margin: -2px 0;\n"
                   "     border-radius: 3px;\n"
                   " }",rVal,bVal,rVal,bVal);
    bSlider.sprintf(" QSlider::groove:horizontal {\n"
                   "     border: 1px solid #999999;\n"
                   "     height: 8px; \n"
                   "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(%d, %d, 0), stop:1 rgb(%d, %d, 255));\n"
                   "     margin: 2px 0;\n"
                   " }\n"
                   "\n"
                   " QSlider::handle:horizontal {\n"
                   "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
                   "     border: 1px solid #5c5c5c;\n"
                   "     width: 8px;\n"
                   "     margin: -2px 0;\n"
                   "     border-radius: 3px;\n"
                   " }",rVal,gVal,rVal,gVal);

    qDebug() << rVal << "," << gVal << "," << bVal << "," << aVal;
    Gslider->setStyleSheet(gSlider);
    Bslider->setStyleSheet(bSlider);

    connect(twinColor,SIGNAL(bgBrushChange(QBrush)),this,SIGNAL(brushColorChange(QBrush)));
    connect(twinColor,SIGNAL(stPenChange(QPen)),this,SIGNAL(penColorChange(QPen)));
}

void ColorMixer::greenChanger(int g)
{
    gVal=g;

    QString s;
    QString rSlider;
    QString bSlider;

    s.sprintf("background:rgba(%d,%d,%d,%d)", rVal, gVal, bVal, aVal);
    rSlider.sprintf(" QSlider::groove:horizontal {\n"
                   "     border: 1px solid #999999;\n"
                   "     height: 8px; \n"
                   "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(0, %d, %d), stop:1 rgb(255, %d, %d));\n"
                   "     margin: 2px 0;\n"
                   " }\n"
                   "\n"
                   " QSlider::handle:horizontal {\n"
                   "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
                   "     border: 1px solid #5c5c5c;\n"
                   "     width: 8px;\n"
                   "     margin: -2px 0;\n"
                   "     border-radius: 3px;\n"
                   " }",gVal,bVal,gVal,bVal);
    bSlider.sprintf(" QSlider::groove:horizontal {\n"
                   "     border: 1px solid #999999;\n"
                   "     height: 8px; \n"
                   "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(%d, %d, 0), stop:1 rgb(%d, %d, 255));\n"
                   "     margin: 2px 0;\n"
                   " }\n"
                   "\n"
                   " QSlider::handle:horizontal {\n"
                   "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
                   "     border: 1px solid #5c5c5c;\n"
                   "     width: 8px;\n"
                   "     margin: -2px 0;\n"
                   "     border-radius: 3px;\n"
                   " }",rVal,gVal,rVal,gVal);

    qDebug() << rVal << "," << gVal << "," << bVal << "," << aVal;
    Rslider->setStyleSheet(rSlider);
    Bslider->setStyleSheet(bSlider);

    connect(twinColor,SIGNAL(bgBrushChange(QBrush)),this,SIGNAL(brushColorChange(QBrush)));
    connect(twinColor,SIGNAL(stPenChange(QPen)),this,SIGNAL(penColorChange(QPen)));
}

void ColorMixer::blueChanger(int b)
{
    bVal=b;

    QString s;
    QString rSlider;
    QString gSlider;

    s.sprintf("background:rgba(%d,%d,%d,%d)", rVal, gVal, bVal, aVal);
    rSlider.sprintf(" QSlider::groove:horizontal {\n"
                   "     border: 1px solid #999999;\n"
                   "     height: 8px; \n"
                   "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(0, %d, %d), stop:1 rgb(255, %d, %d));\n"
                   "     margin: 2px 0;\n"
                   " }\n"
                   "\n"
                   " QSlider::handle:horizontal {\n"
                   "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
                   "     border: 1px solid #5c5c5c;\n"
                   "     width: 8px;\n"
                   "     margin: -2px 0;\n"
                   "     border-radius: 3px;\n"
                   " }",gVal,bVal,gVal,bVal);
    gSlider.sprintf(" QSlider::groove:horizontal {\n"
                   "     border: 1px solid #999999;\n"
                   "     height: 8px; \n"
                   "     background: qlineargradient(x1:0,  x2:1, stop:0 rgb(%d, 0, %d), stop:1 rgb(%d, 255, %d));\n"
                   "     margin: 2px 0;\n"
                   " }\n"
                   "\n"
                   " QSlider::handle:horizontal {\n"
                   "     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
                   "     border: 1px solid #5c5c5c;\n"
                   "     width: 8px;\n"
                   "     margin: -2px 0;\n"
                   "     border-radius: 3px;\n"
                   " }",rVal,bVal,rVal,bVal);


    qDebug() << rVal << "," << gVal << "," << bVal << "," << aVal;
    Rslider->setStyleSheet(rSlider);
    Gslider->setStyleSheet(gSlider);

    connect(twinColor,SIGNAL(bgBrushChange(QBrush)),this,SIGNAL(brushColorChange(QBrush)));
    connect(twinColor,SIGNAL(stPenChange(QPen)),this,SIGNAL(penColorChange(QPen)));
}

void ColorMixer::alphaChanger(int a)
{
    aVal=a;

    QString s;
    s.sprintf("background:rgba(%d,%d,%d,%d)", rVal, gVal, bVal, aVal);

    qDebug() << rVal << "," << gVal << "," << bVal << "," << aVal;

    connect(twinColor,SIGNAL(bgBrushChange(QBrush)),this,SIGNAL(brushColorChange(QBrush)));
    connect(twinColor,SIGNAL(stPenChange(QPen)),this,SIGNAL(penColorChange(QPen)));
}

void ColorMixer::changeRGBslider(int r, int g, int b)
{
    Rslider->setValue(r);
    Gslider->setValue(g);
    Bslider->setValue(b);
    Aslider->setValue(255);
}
