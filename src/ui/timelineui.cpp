#include <QtGui>
#include <QDebug>

#include "../core/timeline.h"
#include "../core/animateproperty.h"
#include "../core/animationitem.h"

#include "timelineui.h"

TimeLineUI::TimeLineUI(TimeLine* timeline, QWidget * parent)
    : QWidget(parent)
    , mTimeline(timeline)
    , propertyAnimation(0)
{
    QString toolButtonStyle = "QToolButton{\n"
            "color:white;\n"
            "background: rgb(46,46,46);\n"
            "}\n"
            "QPushButton[enabled=\"false\"] {\n"
            "	color: gray;\n"
            "background: rgb(60,60,60);\n"
            "}";

    scene = new QGraphicsScene(QRectF(0,0,500,100));
    connect(scene, SIGNAL(selectionChanged()), this, SLOT(activeLayerChanged()));

    TimeLineView* ui = new TimeLineView(scene);

    playHead = new TimelinePlayHead(0);
    playHead->setZValue(2);
    connect(playHead, SIGNAL(xChanged()), this, SLOT(playHeadPosChanged()));
    scene->addItem(playHead);
    
    QHBoxLayout* timeLineData = new QHBoxLayout();
    timeLineData->addWidget(ui);

    QPixmap playImage("://src/pic/play.gif");
    QPixmap pauseImage("://src/pic/pause.gif");
    QPixmap stopImage("://src/pic/stop.gif");

    QIcon playIcon(playImage);
    QIcon pauseIcon(pauseImage);
    QIcon stopIcon(stopImage);

    playButton = new QToolButton();
    playButton->setIcon(playIcon);
    playButton->setStyleSheet(toolButtonStyle);
    playButton->setShortcut(QKeySequence("Ctrl+P"));
    playButton->setToolTip(tr("Play (Ctrl+P)"));
    connect(playButton, SIGNAL(pressed()), this, SLOT(play()));
    
    pauseButton = new QToolButton();
    pauseButton->setIcon(pauseIcon);
    pauseButton->setStyleSheet(toolButtonStyle);
    pauseButton->setVisible(false);
    pauseButton->setShortcut(QKeySequence("Ctrl+P"));
    pauseButton->setToolTip(tr("Pause (Ctrl+P)"));
    connect(pauseButton, SIGNAL(pressed()), this, SLOT(stop()));
    
    QToolButton* stopButton = new QToolButton();
    stopButton->setIcon(stopIcon);
    stopButton->setStyleSheet(toolButtonStyle);

    QToolButton* addLayerButton = new QToolButton();
    addLayerButton->setText("+");
    addLayerButton->setMaximumSize(26,26);
    addLayerButton->setMinimumSize(26,26);
    addLayerButton->setStyleSheet(toolButtonStyle);
    addLayerButton->setShortcut(QKeySequence("Ctrl++"));
    addLayerButton->setToolTip(tr("Add layer (Ctrl++)"));
    connect(addLayerButton, SIGNAL(pressed()), mTimeline,SLOT(addLayer()));

    QToolButton* deleteLayerButton = new QToolButton();
    deleteLayerButton->setText("-");
    deleteLayerButton->setMaximumSize(26,26);
    deleteLayerButton->setMinimumSize(26,26);
    deleteLayerButton->setStyleSheet(toolButtonStyle);
    deleteLayerButton->setShortcut(QKeySequence("Ctrl+-"));
    deleteLayerButton->setToolTip(tr("Remove layer (Ctrl+-)"));
    connect(deleteLayerButton, SIGNAL(pressed()), mTimeline, SLOT(removeLayer()));
    
    QToolButton* swapUpButton = new QToolButton();
    swapUpButton->setText("^");
    swapUpButton->setStyleSheet(toolButtonStyle);
    swapUpButton->setMaximumSize(26,26);
    swapUpButton->setMinimumSize(26,26);
    swapUpButton->setShortcut(QKeySequence("Ctrl+Up"));
    swapUpButton->setToolTip(tr("Swap layer up (Ctrl+Up)"));
    connect(swapUpButton, SIGNAL(pressed()), mTimeline, SLOT(moveLayerUp()));
    
    QToolButton* swapDownButton = new QToolButton();
    swapDownButton->setText("v");
    swapDownButton->setStyleSheet(toolButtonStyle);
    swapDownButton->setMaximumSize(26,26);
    swapDownButton->setMinimumSize(26,26);
    swapDownButton->setShortcut(QKeySequence("Ctrl+Down"));
    swapDownButton->setToolTip(tr("Swap layer down (Ctrl+Down)"));
    connect(swapDownButton, SIGNAL(pressed()), mTimeline, SLOT(moveLayerDown()));
    
    QVBoxLayout* timeLineButton = new QVBoxLayout();
    timeLineButton->addWidget(playButton);
    timeLineButton->addWidget(pauseButton);
    timeLineButton->addWidget(addLayerButton);
    timeLineButton->addWidget(deleteLayerButton);
    timeLineButton->addWidget(swapUpButton);
    timeLineButton->addWidget(swapDownButton);

    QHBoxLayout* stub = new QHBoxLayout();
    stub->addLayout(timeLineButton);
    stub->addLayout(timeLineData);

    setLayout(stub);

    updateTimeLine();

    connect(mTimeline, SIGNAL(invalidate()), this, SLOT(updateTimeLine()));
}

void TimeLineUI::playHeadPosChanged()
{
    qDebug() << "Class - TimeLineUI Func - playHeadPosChanged :: New Pos = " << playHead->scenePos();
    
    int time = TimeLineHelper::xToTime(playHead->scenePos().x());
    mTimeline->setCurrentTime(time);
    
    qDebug() << "Class - TimeLineUI Func - playHeadPosChanged :: Set time to " << time << "\n";
}

void TimeLineUI::activeLayerChanged()
{
    foreach (QGraphicsItem* item, scene->selectedItems())
    {
        if (item->type() == LayerItem::Type)
        {
            LayerItem* layerItem = (LayerItem *) item;
            qDebug() << "Class - TimeLineUI Func - activeLayerChanged :: Select Layer " << layerItem->layerId() << "\n";
            mTimeline->setActiveLayer(layerItem->layerId());
            break;
        }
    }
}

void TimeLineUI::updateTimeLine()
{
    // Prevent playhead from scene->clear()
    scene->removeItem(playHead);
    
    // Clear Scene (Remove and delete all item)
    scene->clear();
    
    // Get the data from the timeline model
    QList<QList<AnimationItem*> > animationData = mTimeline->animationData();
    
    int i = 0;
    for (i=0; i<animationData.size(); i++)
    {
        addLayer(i);
        foreach (AnimationItem* block, animationData.at(i))
        {
            addBlock(i, block);
        }
    }
    
    scene->addItem(playHead);
}

void TimeLineUI::play()
{
    if (propertyAnimation == 0)
    {
        playButton->setVisible(false);
        pauseButton->setVisible(true);
        
        int startX = playHead->scenePos().x();
        int endX = TimeLineHelper::timeToX(mTimeline->endTime());
        
        propertyAnimation = new QPropertyAnimation(playHead, "x");
        propertyAnimation->setStartValue(startX);
        propertyAnimation->setEndValue(endX);
        propertyAnimation->setDuration(TimeLineHelper::xToTime(endX) - TimeLineHelper::xToTime(startX));
        propertyAnimation->start();
        
        connect(propertyAnimation, SIGNAL(finished()), this, SLOT(stop()));
    }
}

void TimeLineUI::stop()
{
    if (propertyAnimation != 0)
    {
        playButton->setVisible(true);
        pauseButton->setVisible(false);
        
        propertyAnimation->stop();
        disconnect(propertyAnimation);
        delete propertyAnimation;
        propertyAnimation = 0;
    }
}

void TimeLineUI::addLayer(int id)
{
    LayerItem* layer = new LayerItem(id, scene->width());
    scene->addItem(layer);
}

void TimeLineUI::addBlock(int layer, AnimationItem* block)
{
    TimelineItem* item = new TimelineItem(layer, block->startTime(), block->endTime());
    foreach (AnimationProperty* frame, block->frames())
    {
        item->addCircle(frame->time());
    }
    
    connect(item, SIGNAL(widthChange(int , double)), mTimeline, SLOT(setBlockDuration(int, double)));
    connect(item, SIGNAL(xChanged(int , double)), mTimeline, SLOT(setBlockStartTime(int, double)));
    
    scene->addItem(item);
}

//////////////////////////////////////////////////////////////
//////          TimeLineView                            //////
//////////////////////////////////////////////////////////////
TimeLineView::TimeLineView(QGraphicsScene* scene, QGraphicsView *parent)
    :QGraphicsView(parent)
{
    sceneV=scene;
    setScene(scene);
    setAlignment(Qt::AlignTop|Qt::AlignLeft);
    setInteractive(true);
}

void TimeLineView::resizeEvent(QResizeEvent *event)
{
    if((sceneV->width() < event->size().width())||(sceneV->height() < event->size().height()))
    {
    sceneV->setSceneRect(0,0,event->size().width(),event->size().height());

    QList<QGraphicsItem*> itemList = sceneV->items();
    for(int i=0;i<itemList.size();i++)
    {
        if(itemList[i]->type()==QGraphicsItem::UserType+2)
        {
            LayerItem*layer = (LayerItem*)itemList[i];
            layer->updateWidth();
        }
    }
    }
    QGraphicsView::resizeEvent(event);
}

void TimeLineView::mousePressEvent(QMouseEvent *event)
{
        sceneV->invalidate();
        QGraphicsView::mousePressEvent(event);
}

//////////////////////////////////////////////////////////////
//////          LayerItem                               //////
//////////////////////////////////////////////////////////////
LayerItem::LayerItem(int layerNumber, int width, QGraphicsRectItem *parent)
    : QGraphicsRectItem(parent)
    , mLayerId(layerNumber)
{
    QBrush brush(QColor(55,55,55));
    setBrush(brush);
    setPen(QPen(QColor(50,50,50)));
    setPos(0, (layerNumber+1)*20);
    setFlag(QGraphicsItem::ItemIsSelectable);
    setRect(0, 0, width, 20);
}

int LayerItem::layerId()
{
    return mLayerId;
}

void LayerItem::updateWidth()
{
   setRect(0, 0, scene()->width(), 20);
}

void LayerItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    scene()->invalidate();
    QGraphicsRectItem::mousePressEvent(event);
}

//////////////////////////////////////////////////////////////
//////          TimeLineItem                            //////
//////////////////////////////////////////////////////////////

TimelineItem::TimelineItem(int layer, int startTime, int endTime, QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , mLayerId(layer)
    , mStartTime(startTime)
    , yChangeAble(false)
    , edgeMoveMode(false)
{
    setPos(TimeLineHelper::timeToX(startTime), (layer+1)*20);
    qDebug() << "Size is :: " << TimeLineHelper::timeToX(endTime) - TimeLineHelper::timeToX(startTime);
    setRect(0, 0, TimeLineHelper::timeToX(endTime) - TimeLineHelper::timeToX(startTime), 20);

    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFlag(QGraphicsItem::ItemIsSelectable);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges);
    //setAcceptDrops(true);
}

void TimelineItem::addCircle(int t)
{
    for(int i=0; i<circlePosX.length();i++)
    {
        if(circlePosX[i] == t)
        {
            return;
        }
    }
    
    circlePosX.append(t);

    QGraphicsEllipseItem* circle = new QGraphicsEllipseItem(this);
    circle->setBrush(Qt::yellow);
    circle->setRect(0, 0, 5, 5);
    circle->setPos((TimeLineHelper::timeToX(t)-scenePos().x()) + 2.5, 7.5);
    circle->setStartAngle(0);
    circle->setSpanAngle(360*16);
    circle->setZValue(1);
}

void TimelineItem::allowYchange(bool condition)
{
    yChangeAble = condition;
}

int TimelineItem::layerId() const
{
    return mLayerId;
}

int TimelineItem::startTime() const
{
    return mStartTime;
}

QRectF TimelineItem::rect() const
{
    return mRect;
}

void TimelineItem::setRect(qreal x, qreal y, qreal width, qreal height)
{
    mRect = QRectF(x,y,width, height);
    // Width change is not emit here since it is more efficient to emit it in mouse release so we release it only one time
}

QRectF TimelineItem::boundingRect() const
{
    int penWidth = 1;
    return mRect.adjusted(-penWidth/2, -penWidth/2, (int)((penWidth/2.0)+0.5), (int)((penWidth/2.0)+0.5));
}

void TimelineItem::paint(QPainter * painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    painter->setBrush(QColor(123, 221, 31));
    painter->drawRoundedRect(mRect, 5, 5);
}

void TimelineItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF pressPos = event->pos();
    if(pressPos.x() >= (this->rect().width()-5))
    {
        edgeMoveMode = true;
    }

    QList<QGraphicsItem*> itemList = scene()->items();
    for(int i=0; i<itemList.size(); i++)
    {
        //change layer size
        if(itemList[i]->type() == LayerItem::Type)
        {
            if(itemList[i]->scenePos().y() == this->scenePos().y())
            {
                itemList[i]->setSelected(true);
            }
            else
            {
                itemList[i]->setSelected(false);
            }
        }
    }
    scene()->invalidate();
    QGraphicsItem::mousePressEvent(event);
}

void TimelineItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (edgeMoveMode)
    {
        edgeMoveMode = false;
        
        // Lock it to 10px block
        setRect(rect().x(), rect().y(), ((int)rect().width()/10)*10, rect().height());
        
        emit widthChange(mStartTime, rect().width() + scenePos().x());
    }
    else
    {
        // Lock it to 10px block
        setPos(((int)scenePos().x()/10)*10, scenePos().y());
        
        emit xChanged(mStartTime, scenePos().x());
        //return;
        //mStartTime = TimeLineHelper::xToTime(scenePos().x());
        //QGraphicsItem::mouseReleaseEvent(event);
    }
}

// FIXME: Haven't check
void TimelineItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF movePos = event->pos();
    
    //find highest lowest X circle
    double highestX=circlePosX[0];
    double lowestX =circlePosX[0];
    for(int i=0;i<circlePosX.length();i++)
    {
        if(circlePosX[i] > highestX)
        {
            highestX = circlePosX[i];
        }
    }
    
    double minWidth = (highestX+5)-(lowestX-5);
    if(minWidth==0)
    {
        minWidth=10;
    }

    //qDebug()<<minWidth;
    //qDebug()<<"rect :"<<rect().width();

    //qDebug()<<circlePosX;
    
    if (edgeMoveMode == true && rect().width() >= minWidth && movePos.x() >= minWidth)
    {
        prepareGeometryChange();
        setRect(0, 0, movePos.x(), rect().height());
    }
    else if (edgeMoveMode == true && rect().width() < minWidth)
    {
        //qDebug()<<"Lock";
        prepareGeometryChange();
        setRect(0, 0, highestX, rect().height());
    }
    else
    {
        QGraphicsItem::mouseMoveEvent(event);
    }
    
    scene()->invalidate();
}

// FIXME: Haven't check
QVariant TimelineItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if(change == ItemPositionChange && scene())
    {
        afterPoint = value.toPointF();

        // Not allow moving in the y axis
        if(afterPoint.y() != pos().y() && yChangeAble==false)
        {
            afterPoint.setY(pos().y());
        }
        // Wrap left
        if(afterPoint.x() < 0)
        {
            afterPoint.setX(0);
        }
        if(afterPoint.x() + rect().width() > scene()->sceneRect().width())
        {
            scene()->setSceneRect(0, 0, afterPoint.x()+rect().width()+100, 100);
            QList<QGraphicsItem*> itemList = scene()->items();
            for(int i=0;i<itemList.size();i++)
            {
                //change layer size
                if(itemList[i]->type()==UserType + 2)
                {
                    LayerItem* layer0=(LayerItem*)itemList[i];
                    layer0->updateWidth();

                }
            }
        }
        return afterPoint;
    }
    return QGraphicsItem::itemChange(change, value);
}

//////////////////////////////////////////////////////////////
//////          TimeLinePlayHead                        //////
//////////////////////////////////////////////////////////////
TimelinePlayHead::TimelinePlayHead(int time, QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , height(0)
    , moveMode(false)
{
    setPos(TimeLineHelper::timeToX(time), 0);
    
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges);
}

void TimelinePlayHead::paint(QPainter *painter, const QStyleOptionGraphicsItem */*option*/, QWidget *widget)
{
    painter->save();

    //qDebug() << "TimeLinePlayHead" << widget->size();

    painter->setPen(Qt::red);
    painter->drawRect(0, 0, 10, 20);
    if(scene()->height()+20 > widget->height())
    {
        height = scene()->height()+20;
    }
    else
    {
        height = widget->height();
    }
    painter->drawLine(QPointF(5, 20), QPointF(5, height));

    painter->restore();
}

void TimelinePlayHead::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    moveMode=true;
    
    QGraphicsItem::mousePressEvent(event);
}

void TimelinePlayHead::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF movePos = event->scenePos();

    if (event->scenePos().y() > 20)
        return;
    
//qDebug()<<move;
    if(moveMode == true && movePos.y() != 0)
    {
        //prepareGeometryChange();
        if(movePos.x() < 0)
        {
            setPos(0,0);
        }
        else if(movePos.x() > scene()->width())
        {
            setPos(scene()->width()-10,0);
        }
        else
        {
            setPos(movePos.x(),0);
        }
    }
    else
    {
        QGraphicsItem::mouseMoveEvent(event);
    }
}

void TimelinePlayHead::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    moveMode=false;
    QGraphicsItem::mouseReleaseEvent(event);
}

QRectF TimelinePlayHead::boundingRect() const
{
    return QRectF(0, 0, 10, height);
}
