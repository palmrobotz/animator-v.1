
#include <QtGui>

#include "graphicsitemlibwidget.h"
#include "../graphicsitem/mygraphicsobject.h"
//#include "../graphicsitem/rectGraphicsItem.h"

class TreeWidgetItem : public QTreeWidgetItem
{
public:
    TreeWidgetItem(MyGraphicsObject* graphicsItem, QTreeWidget * parent)
        : QTreeWidgetItem(parent, TypeNumber())
        , mGraphicsItem(graphicsItem)
    {
        setText(1, mGraphicsItem->itemName());
    };
    
    TreeWidgetItem(MyGraphicsObject* graphicsItem, QTreeWidget * parent, QTreeWidgetItem * preceding)
        : QTreeWidgetItem(parent, preceding, TypeNumber())
        , mGraphicsItem(graphicsItem)
    {
        setText(1, mGraphicsItem->itemName());
    };
    
    TreeWidgetItem(MyGraphicsObject* graphicsItem, QTreeWidgetItem * parent)
        : QTreeWidgetItem(parent, TypeNumber())
        , mGraphicsItem(graphicsItem)
    {
        setText(1, mGraphicsItem->itemName());
    };
    
    TreeWidgetItem(MyGraphicsObject* graphicsItem, QTreeWidgetItem * parent, QTreeWidgetItem * preceding)
        : QTreeWidgetItem(parent, preceding, TypeNumber())
        , mGraphicsItem(graphicsItem)
    {
        setText(1, mGraphicsItem->itemName());
    };

    MyGraphicsObject* graphicsItem() const
    {
        return mGraphicsItem;
    }
    
    static int TypeNumber()
    {
        return QTreeWidgetItem::UserType+1;
    }
    
private:
    MyGraphicsObject* mGraphicsItem;
};

GraphicsItemLibWidget::GraphicsItemLibWidget(QWidget* parent)
    : QWidget(parent)
    , numOfItem(0)
    , mCloseUp(false)
{
    setupUi();
    
    connect(addItemButton, SIGNAL(released()), this, SLOT(insertItem()));
    connect(deleteItemButton, SIGNAL(released()), this, SLOT(removeItem()));
    connect(useItemButton, SIGNAL(released()), this, SIGNAL(useItemClick()));
    connect(itemTreeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));
    connect(itemTreeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem *, int)));
}

void GraphicsItemLibWidget::setupUi()
{
    QString buttonStyle = "QPushButton{\n"
            "color:white;\n"
            "background: rgb(46,46,46);\n"
            "}\n"
            "QPushButton[enabled=\"false\"] {\n"
            "	color: gray;\n"
            "background: rgb(60,60,60);\n"
            "}";

    QString toolButtonStyle = "QToolButton{\n"
            "color:white;\n"
            "background: rgb(46,46,46);\n"
            "}\n"
            "QPushButton[enabled=\"false\"] {\n"
            "	color: gray;\n"
            "background: rgb(60,60,60);\n"
            "}";

    QString headerStyle = "QTreeView QHeaderView:section{\n"
                          "color:white;\n"
                          "background:rgb(46,46,46);\n"
                          "}\n";

    // Left Widget
    previewView = new QGraphicsView();
    previewScene = new QGraphicsScene();
    previewView->setScene(previewScene);
    previewView->setMinimumWidth(150);

    // Right Widget
    itemTreeWidget = new QTreeWidget();
    itemTreeWidget->setColumnCount(2);
    itemTreeWidget->setHeaderLabels(QStringList() << "" << "Name");
    itemTreeWidget->setStyleSheet(headerStyle);

    itemTreeWidget->setColumnWidth(0,20);
    itemTreeWidget->setColumnWidth(1,120);
    //itemTreeWidget->setMaximumWidth(140);

    addItemButton = new QToolButton();
    addItemButton->setText("+");
    addItemButton->setStyleSheet(toolButtonStyle);
    addItemButton->setMaximumSize(20,20);
    addItemButton->setMinimumSize(20,20);
    deleteItemButton = new QToolButton();
    deleteItemButton->setText("-");
    deleteItemButton->setMaximumSize(20,20);
    deleteItemButton->setMinimumSize(20,20);
    deleteItemButton->setStyleSheet(toolButtonStyle);
    useItemButton = new QToolButton();
    useItemButton->setText("->");
    useItemButton->setMaximumSize(20,20);
    useItemButton->setMinimumSize(20,20);
    useItemButton->setStyleSheet(toolButtonStyle);
    //addFolderButton = new QPushButton("F");
    
    QVBoxLayout* buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(addItemButton);
    buttonLayout->addWidget(deleteItemButton);
    buttonLayout->addWidget(useItemButton);
    //buttonLayout->addWidget(addFolderButton);
    buttonLayout->addStretch();
    buttonLayout->setContentsMargins(2, 0, 0, 0);
    buttonLayout->setSpacing(2);
    QHBoxLayout* rightLayout = new QHBoxLayout;
    rightLayout->addWidget(itemTreeWidget);
    rightLayout->addLayout(buttonLayout);
    rightLayout->setContentsMargins(0, 0, 0, 0);
    rightLayout->setSpacing(0);
    
    QWidget* rightWidget = new QWidget;
    rightWidget->setLayout(rightLayout);
    //rightWidget->setMinimumWidth(250);
    
    // Combine them in a splitter
    QSplitter* splitter = new QSplitter;
    splitter->addWidget(previewView);
    splitter->addWidget(rightWidget);
    
    QHBoxLayout* stubLayout = new QHBoxLayout(this);
    stubLayout->addWidget(splitter);
    //stubLayout->setContentsMargins(0, 0, 0, 0);
    
    setLayout(stubLayout);
}

void GraphicsItemLibWidget::insertItem()
{
    // Get current select item
    //QList<QTreeWidgetItem *> selectedItem = itemTreeWidget->selectedItems();
    
    // If one item is selected add to be child of the selected item
    /*if (selectedItem.size() == 1)
    {
        MyGraphicsObject* graphicsItem = new MyGraphicsObject();
        TreeWidgetItem* treeItem = new TreeWidgetItem(graphicsItem, selectedItem[0]);
    }*/
    // 0 or more than 1 item is being select, insert as root level item
    //else
    //{
        //MyGraphicsObject* graphicsItem = new MyGraphicsObject();
    
    /*MyGraphicsObject* graphicsItem = new MyGraphicsObject();
        graphicsItem->setItemName(QString("Untitled %1").arg(numOfItem));
        numOfItem++;
    
        new TreeWidgetItem(graphicsItem, itemTreeWidget);*/
    
    //}
}

void GraphicsItemLibWidget::removeItem()
{
    QList<QTreeWidgetItem *> selectedItem = itemTreeWidget->selectedItems();
    foreach (QTreeWidgetItem * item, selectedItem)
    {
        if (item->parent() == 0)
        {
            itemTreeWidget->takeTopLevelItem(itemTreeWidget->indexOfTopLevelItem(item));
        }
        else
        {
            item->parent()->takeChild(item->parent()->indexOfChild(item));
        }
    }
    updatePreview();
}

void GraphicsItemLibWidget::itemSelectionChanged()
{
    QList<QTreeWidgetItem *> selectedItem = itemTreeWidget->selectedItems();
    
    if (selectedItem.size() == 1)
    {
        QTreeWidgetItem* treeWidgetItem = selectedItem[0];
        if (treeWidgetItem->type() == TreeWidgetItem::TypeNumber())
        {
            MyGraphicsObject* item = ((TreeWidgetItem *) treeWidgetItem)->graphicsItem();
            updatePreview(item);
            emit itemClickedChanged(item);
        }
    }
}

void GraphicsItemLibWidget::itemDoubleClicked(QTreeWidgetItem * item, int column)
{
    if (mCloseUp)
        return;
    
    if ((column == 0) && (item != 0) && (item->type() == TreeWidgetItem::TypeNumber()))
    {
        MyGraphicsObject* graphicsItem = ((TreeWidgetItem *) item)->graphicsItem();
        
        // Disable tool button
        addItemButton->setEnabled(false);
        deleteItemButton->setEnabled(false);
        useItemButton->setEnabled(false);
        
        mCloseUp = true;
        
        updatePreview(graphicsItem);
        emit itemDoubleClickChanged(graphicsItem);
    }
}

void GraphicsItemLibWidget::stopCloseUp()
{
    addItemButton->setEnabled(true);
    deleteItemButton->setEnabled(true);
    useItemButton->setEnabled(true);
    mCloseUp = false;
}

void GraphicsItemLibWidget::updatePreview(MyGraphicsObject* item)
{
    // Remove previous item from the preview scene
    previewScene->clear();
    
    if (item != 0)
    {
        // Clone the item, Scale it then add to the scene
        //MyGraphicsObject* cloneItem = item->clone();
        // FIXME: Scale base on item size
        //cloneItem->setScale(0.25);
        //previewScene->addItem(cloneItem);
    }
}
