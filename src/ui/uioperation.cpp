
#include <QtGui>

#include "uioperation.h"
#include "../graphicsitem/mygraphicsobject.h"

NewGraphicsItem::NewGraphicsItem(QGraphicsScene * scene, MyAbstractGraphicsObject * item)
    : mScene(scene)
    , mItem(item)
{
    setText("Create new item");
}

void NewGraphicsItem::undo()
{
    mScene->removeItem(mItem);
}

void NewGraphicsItem::redo()
{
    mScene->addItem(mItem);
}

MyAbstractGraphicsObject* NewGraphicsItem::item()
{
    return mItem;
}
