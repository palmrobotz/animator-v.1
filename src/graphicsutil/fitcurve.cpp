
#include <QtGui>
#include <cmath>
#include <cstdlib>
#include "fitcurve.h"

/* FitPointToCurve :
 *     Fit a Bezier curve to a set of digitized points
 * Parameter :
 *     d     - array of digitized points
 *     nPts  - number of digitized points
 *     error - user-defined squared error
 */
void FitCurve::FitPointToCurve(QPointF* d, int nPts, double error)
{
    /* Clear bezier point buffer */
    bezierPoint.clear();
    
    /*  Unit tangent vectors at endpoints */
    Vector2F tHat1, tHat2;
    
    tHat1 = ComputeLeftTangent(d, 0);
    tHat2 = ComputeRightTangent(d, nPts - 1);
    FitCubic(d, 0, nPts - 1, tHat1, tHat2, error);
}

QList<QPointF> FitCurve::bezierControlPoint()
{
    return bezierPoint;
}

/* DrawBezierCurve :
 *     This function is called to draw a part of the bezier curve
 * Parameter :
 *     n     - number of point in the array curve
 *     curve - array of point
 */
void FitCurve::DrawBezierCurve(int n, BezierCurve curve)
{
    int i=0;
    for (i=0; i<=n; i++)
    {
        bezierPoint.push_back(curve[i]);
    }
}

/* FitCubic :
 *     Fit a Bezier curve to a (sub)set of digitized points
 * Parameter :
 *     d - array of digitized points
 *     first, last  - indices of first and last pts in region
 *     tHat1, tHat2 - unit tangent vectors at endpoints
 *     error        - user-defined squared error
 */
void FitCurve::FitCubic(QPointF* d, int first, int last, Vector2F tHat1, Vector2F tHat2, double error)
{
    BezierCurve	bezCurve; /*Control points of fitted Bezier curve*/
    double* u;          /*  Parameter values for point  */
    double* uPrime;     /*  Improved parameter values */
    double	maxError;	/*  Maximum fitting error	 */
    int		splitPoint;	/*  Point to split point set at	 */
    int		nPts;		/*  Number of points in subset  */
    double	iterationError;     /*Error below which you try iterating  */
    int		maxIterations = 4;  /*  Max times to try iterating  */
    Vector2F   tHatCenter;   	/* Unit tangent vector at splitPoint */
    int	i;
    
    iterationError = error * error;
    nPts = last - first + 1;
    
    /*  Use heuristic if region only has two points in it */
    if (nPts == 2)
    {
	    double dist = V2DistanceBetween2Points(&d[last], &d[first]) / 3.0;
        
		bezCurve = (QPointF *) malloc(4 * sizeof(QPointF));
		bezCurve[0] = d[first];
		bezCurve[3] = d[last];
		V2Add(&bezCurve[0], V2Scale(&tHat1, dist), &bezCurve[1]);
		V2Add(&bezCurve[3], V2Scale(&tHat2, dist), &bezCurve[2]);
		DrawBezierCurve(3, bezCurve);
		free((void *)bezCurve);
		return;
    }
    
    /*  Parameterize points, and attempt to fit curve */
    u = ChordLengthParameterize(d, first, last);
    bezCurve = GenerateBezier(d, first, last, u, tHat1, tHat2);
    
    /*  Find max deviation of points to fitted curve */
    maxError = ComputeMaxError(d, first, last, bezCurve, u, &splitPoint);
    if (maxError < error) {
		DrawBezierCurve(3, bezCurve);
		free((void *)u);
		free((void *)bezCurve);
		return;
    }
    
    
    /*  If error not too large, try some reparameterization  */
    /*  and iteration */
    if (maxError < iterationError) {
		for (i = 0; i < maxIterations; i++) {
	    	uPrime = Reparameterize(d, first, last, u, bezCurve);
	    	free((void *)bezCurve);
	    	bezCurve = GenerateBezier(d, first, last, uPrime, tHat1, tHat2);
	    	maxError = ComputeMaxError(d, first, last,
                                       bezCurve, uPrime, &splitPoint);
	    	if (maxError < error) {
                DrawBezierCurve(3, bezCurve);
                free((void *)u);
                free((void *)bezCurve);
                free((void *)uPrime);
                return;
            }
            free((void *)u);
            u = uPrime;
        }
    }
    
    /* Fitting failed -- split at max error point and fit recursively */
    free((void *)u);
    free((void *)bezCurve);
    tHatCenter = ComputeCenterTangent(d, splitPoint);
    FitCubic(d, first, splitPoint, tHat1, tHatCenter, error);
    V2Negate(&tHatCenter);
    FitCubic(d, splitPoint, last, tHatCenter, tHat2, error);
}

/* GenerateBezier :
 *     Use least-squares method to find Bezier control points for region.
 * Parameter:
 *     d - array of digitized points
 *     first, last  - indices defining region
 *     uPrime       - parameter values for region
 *     tHat1, tHat2 - unit tangent vectors at endpoints
 * Return:
 *     Point of bezier curve of this region of curve as an array of QPointF
 */
BezierCurve FitCurve::GenerateBezier(QPointF* d, int first, int last, double* uPrime,
                           Vector2F tHat1, Vector2F tHat2)
{
    int 	i;
    Vector2F 	A[MAXPOINTS][2];	/* Precomputed rhs for eqn	*/
    int 	nPts;			/* Number of pts in sub-curve */
    double 	C[2][2];			/* Matrix C		*/
    double 	X[2];			/* Matrix X			*/
    double 	det_C0_C1,		/* Determinants of matrices	*/
    det_C0_X,
    det_X_C1;
    double 	alpha_l,		/* Alpha values, left and right	*/
    alpha_r;
    Vector2F 	tmp;			/* Utility variable		*/
    BezierCurve	bezCurve;	/* RETURN bezier curve ctl pts	*/
    
    bezCurve = (QPointF *)malloc(4 * sizeof(QPointF));
    nPts = last - first + 1;
    
    
    /* Compute the A's	*/
    for (i = 0; i < nPts; i++) {
		Vector2F		v1, v2;
		v1 = tHat1;
		v2 = tHat2;
		V2Scale(&v1, B1(uPrime[i]));
		V2Scale(&v2, B2(uPrime[i]));
		A[i][0] = v1;
		A[i][1] = v2;
    }
    
    /* Create the C and X matrices	*/
    C[0][0] = 0.0;
    C[0][1] = 0.0;
    C[1][0] = 0.0;
    C[1][1] = 0.0;
    X[0]    = 0.0;
    X[1]    = 0.0;
    
    for (i = 0; i < nPts; i++) {
        C[0][0] += V2Dot(&A[i][0], &A[i][0]);
		C[0][1] += V2Dot(&A[i][0], &A[i][1]);
        /*					C[1][0] += V2Dot(&A[i][0], &A[i][1]);*/
		C[1][0] = C[0][1];
		C[1][1] += V2Dot(&A[i][1], &A[i][1]);
        
		tmp = V2SubII(d[first + i],
                      V2AddII(
                              V2ScaleIII(d[first], B0(uPrime[i])),
                              V2AddII(
                                      V2ScaleIII(d[first], B1(uPrime[i])),
                                      V2AddII(
                                              V2ScaleIII(d[last], B2(uPrime[i])),
                                              V2ScaleIII(d[last], B3(uPrime[i]))))));
        
        
        X[0] += V2Dot(&A[i][0], &tmp);
        X[1] += V2Dot(&A[i][1], &tmp);
    }
    
    /* Compute the determinants of C and X	*/
    det_C0_C1 = C[0][0] * C[1][1] - C[1][0] * C[0][1];
    det_C0_X  = C[0][0] * X[1]    - C[1][0] * X[0];
    det_X_C1  = X[0]    * C[1][1] - X[1]    * C[0][1];
    
    /* Finally, derive alpha values	*/
    alpha_l = (det_C0_C1 == 0) ? 0.0 : det_X_C1 / det_C0_C1;
    alpha_r = (det_C0_C1 == 0) ? 0.0 : det_C0_X / det_C0_C1;
    
    /* If alpha negative, use the Wu/Barsky heuristic (see text) */
    /* (if alpha is 0, you get coincident control points that lead to
     * divide by zero in any subsequent NewtonRaphsonRootFind() call. */
    double segLength = V2DistanceBetween2Points(&d[last], &d[first]);
    double epsilon = 1.0e-6 * segLength;
    if (alpha_l < epsilon || alpha_r < epsilon)
    {
		/* fall back on standard (probably inaccurate) formula, and subdivide further if needed. */
		double dist = segLength / 3.0;
		bezCurve[0] = d[first];
		bezCurve[3] = d[last];
		V2Add(&bezCurve[0], V2Scale(&tHat1, dist), &bezCurve[1]);
		V2Add(&bezCurve[3], V2Scale(&tHat2, dist), &bezCurve[2]);
		return (bezCurve);
    }
    
    /*  First and last control points of the Bezier curve are */
    /*  positioned exactly at the first and last data points */
    /*  Control points 1 and 2 are positioned an alpha distance out */
    /*  on the tangent vectors, left and right, respectively */
    bezCurve[0] = d[first];
    bezCurve[3] = d[last];
    V2Add(&bezCurve[0], V2Scale(&tHat1, alpha_l), &bezCurve[1]);
    V2Add(&bezCurve[3], V2Scale(&tHat2, alpha_r), &bezCurve[2]);
    return (bezCurve);
}


/* Reparameterize:
 *     Given set of points and their parameterization, try to find
 *     a better parameterization.
 * Parameter:
 *     d - array of digitized points
 *     first, last - indices defining region
 *     u           - current parameter values
 *     bezCurve    - current fitted curve
 * Return:
 *     better parameterize value
 */
double * FitCurve::Reparameterize(QPointF* d, int first, int last, double* u, BezierCurve bezCurve)
{
    int 	nPts = last-first+1;
    int 	i;
    double	*uPrime;		/*  New parameter values	*/
    
    uPrime = (double *) malloc(nPts * sizeof(double));
    for (i = first; i <= last; i++) {
		uPrime[i-first] = NewtonRaphsonRootFind(bezCurve, d[i], u[i-first]);
    }
    return (uPrime);
}

/* NewtonRaphsonRootFind :
 *     Use Newton-Raphson iteration to find better root.
 * Parameter:
 *     Q - current fitted curve
 *     P - digitized point
 *     u - parameter value for "P"
 * Return:
 *     better root
 */
double FitCurve::NewtonRaphsonRootFind(BezierCurve Q, QPointF P, double u)
{
    double      numerator, denominator;
    QPointF 	Q1[3], Q2[2];	 /*  Q' and Q''			*/
    QPointF	Q_u, Q1_u, Q2_u; /*u evaluated at Q, Q', & Q''	*/
    double 		uPrime;          /*  Improved u			*/
    int 		i;
    
    /* Compute Q(u)	*/
    Q_u = BezierII(3, Q, u);
    
    /* Generate control vertices for Q'	*/
    for (i = 0; i <= 2; i++) {
		Q1[i].setX( (Q[i+1].x() - Q[i].x()) * 3.0 );
		Q1[i].setY( (Q[i+1].y() - Q[i].y()) * 3.0 );
    }
    
    /* Generate control vertices for Q'' */
    for (i = 0; i <= 1; i++) {
		Q2[i].setX( (Q1[i+1].x() - Q1[i].x()) * 2.0 );
		Q2[i].setY( (Q1[i+1].y() - Q1[i].y()) * 2.0 );
    }
    
    /* Compute Q'(u) and Q''(u)	*/
    Q1_u = BezierII(2, Q1, u);
    Q2_u = BezierII(1, Q2, u);
    
    /* Compute f(u)/f'(u) */
    numerator = (Q_u.x() - P.x()) * (Q1_u.x()) + (Q_u.y() - P.y()) * (Q1_u.y());
    denominator = (Q1_u.x()) * (Q1_u.x()) + (Q1_u.y()) * (Q1_u.y()) +
    (Q_u.x() - P.x()) * (Q2_u.x()) + (Q_u.y() - P.y()) * (Q2_u.y());
    if (denominator == 0.0f) return u;
    
    /* u = u - f(u)/f'(u) */
    uPrime = u - (numerator/denominator);
    return (uPrime);
}

/*
 * Bezier :
 *     Evaluate a Bezier curve at a particular parameter value
 * Parameter:
 *     degree - the degree of the bezier curve
 *     V - array of control points
 *     t - parametric value to find point for
 * Return:
 *     Point on a bezier curve at a particular parameter value
 */
QPointF FitCurve::BezierII(int degree, QPointF* V, double t)
{
    int 	i, j;
    QPointF 	Q;	        /* Point on curve at parameter t	*/
    QPointF 	*Vtemp;		/* Local copy of control points		*/
    
    /* Copy array	*/
    Vtemp = (QPointF *)malloc((unsigned)((degree+1)
                                          * sizeof (QPointF)));
    for (i = 0; i <= degree; i++) {
		Vtemp[i] = V[i];
    }
    
    /* Triangle computation	*/
    for (i = 1; i <= degree; i++) {
		for (j = 0; j <= degree-i; j++) {
	    	Vtemp[j].setX( (1.0 - t) * Vtemp[j].x() + t * Vtemp[j+1].x() );
	    	Vtemp[j].setY( (1.0 - t) * Vtemp[j].y() + t * Vtemp[j+1].y() );
		}
    }
    
    Q = Vtemp[0];
    free((void *)Vtemp);
    return Q;
}

/* B0, B1, B2, B3 :
 * Bezier multipliers
 */

double FitCurve::B0(double u)
{
    double tmp = 1.0 - u;
    return (tmp * tmp * tmp);
}

double FitCurve::B1(double u)
{
    double tmp = 1.0 - u;
    return (3 * u * (tmp * tmp));
}

double FitCurve::B2(double u)
{
    double tmp = 1.0 - u;
    return (3 * u * u * tmp);
}

double FitCurve::B3(double u)
{
    return (u * u * u);
}

/* ComputeLeftTangent, ComputeRightTangent, ComputeCenterTangent :
 * Approximate unit tangents at endpoints and "center" of digitized curve
 */

Vector2F FitCurve::ComputeLeftTangent(QPointF* d, int end)
{
    Vector2F	tHat1;
    tHat1 = V2SubII(d[end+1], d[end]);
    tHat1 = *V2Normalize(&tHat1);
    return tHat1;
}

Vector2F FitCurve::ComputeRightTangent(QPointF* d, int end)
{
    Vector2F	tHat2;
    tHat2 = V2SubII(d[end-1], d[end]);
    tHat2 = *V2Normalize(&tHat2);
    return tHat2;
}

Vector2F FitCurve::ComputeCenterTangent(QPointF* d, int center)
{
    Vector2F	V1, V2, tHatCenter;
    
    V1 = V2SubII(d[center-1], d[center]);
    V2 = V2SubII(d[center], d[center+1]);
    tHatCenter.setX( (V1.x() + V2.x())/2.0 );
    tHatCenter.setY( (V1.y() + V2.y())/2.0 );
    tHatCenter = *V2Normalize(&tHatCenter);
    return tHatCenter;
}

/* ChordLengthParameterize :
 *     Assign parameter values to digitized points
 *	   using relative distances between points.
 * Parameter:
 *     d - array of digitized points
 *     first, last - indices defining region
 */
double* FitCurve::ChordLengthParameterize(QPointF* d, int first, int last)
{
    int		i;
    double	*u;			/*  Parameterization		*/
    
    u = (double *) malloc((unsigned)(last-first+1) * sizeof(double));
    
    u[0] = 0.0;
    for (i = first+1; i <= last; i++) {
		u[i-first] = u[i-first-1] +
        V2DistanceBetween2Points(&d[i], &d[i-1]);
    }
    
    for (i = first + 1; i <= last; i++) {
		u[i-first] = u[i-first] / u[last-first];
    }
    
    return(u);
}

/* ComputeMaxError :
 *     Find the maximum squared distance of digitized points
 *     to fitted curve.
 * Parameter:
 *     d - array of digitized points
 *     first, last - indices defining region
 *     bezCurve    - fitted Bezier curve
 *     u           - parameterization of points
 *     splitPoint  - point of maximum error
 * Return:
 *     Maximum error
 */
double FitCurve::ComputeMaxError(QPointF* d, int first, int last, BezierCurve bezCurve, double* u, int* splitPoint)
{
    int		i;
    double	maxDist;		/*  Maximum error		*/
    double	dist;		/*  Current error		*/
    QPointF	P;			/*  Point on curve		*/
    Vector2F	v;			/*  Vector from point to curve	*/
    
    *splitPoint = (last - first + 1)/2;
    maxDist = 0.0;
    for (i = first + 1; i < last; i++) {
		P = BezierII(3, bezCurve, u[i-first]);
		v = V2SubII(P, d[i]);
		dist = V2SquaredLength(&v);
		if (dist >= maxDist) {
	    	maxDist = dist;
	    	*splitPoint = i;
		}
    }
    return (maxDist);
}

Vector2F FitCurve::V2AddII(Vector2F a, Vector2F b)
{
    Vector2F c;
    c.setX( a.x() + b.x() );  c.setY( a.y() + b.y() );
    return (c);
}

Vector2F FitCurve::V2ScaleIII(Vector2F v, double s)
{
    Vector2F result;
    result.setX( v.x() * s ); result.setY( v.y() * s );
    return (result);
}

Vector2F FitCurve::V2SubII(Vector2F a, Vector2F b)
{
    Vector2F	c;
    c.setX( a.x() - b.x() ); c.setY( a.y() - b.y() );
    return (c);
}

/* returns squared length of input vector */
double FitCurve::V2SquaredLength(Vector2F* a)
{
    return((a->x() * a->x())+(a->y() * a->y()));
}

/* returns length of input vector */
double FitCurve::V2Length(Vector2F* a)
{
	return(sqrt(V2SquaredLength(a)));
}

/* negates the input vector and returns it */
Vector2F* FitCurve::V2Negate(Vector2F* v)
{
	v->setX( -v->x() );  v->setY( -v->y() );
	return(v);
}

/* normalizes the input vector and returns it */
Vector2F* FitCurve::V2Normalize(Vector2F* v)
{
    double len = V2Length(v);
	if (len != 0.0) { v->setX( v->x() / len );  v->setY( v->y() / len); }
	return(v);
}

/* scales the input vector to the new length and returns it */
Vector2F* FitCurve::V2Scale(Vector2F* v, double newlen)
{
    double len = V2Length(v);
	if (len != 0.0) { v->setX( v->x() * newlen/len );  v->setY( v->y() * newlen/len ); }
	return(v);
}

/* return vector sum c = a+b */
Vector2F* FitCurve::V2Add(Vector2F* a, Vector2F* b, Vector2F* c)
{
	c->setX( a->x()+b->x() );  c->setY( a->y()+b->y() );
	return(c);
}

/* return vector difference c = a-b */
Vector2F* FitCurve::V2Sub(Vector2F* a, Vector2F* b, Vector2F* c)
{
	c->setX( a->x()-b->x() );  c->setY( a->y()-b->y() );
	return(c);
}

/* return the dot product of vectors a and b */
double FitCurve::V2Dot(Vector2F* a, Vector2F* b)
{
	return((a->x()*b->x())+(a->y()*b->y()));
}

/* make a linear combination of two vectors and return the result. */
/* result = (a * ascl) + (b * bscl) */
Vector2F* FitCurve::V2Combine(Vector2F* a, Vector2F* b, Vector2F* result, double ascl, double bscl)
{
	result->setX( (ascl * a->x()) + (bscl * b->x()) );
	result->setY( (ascl * a->y()) + (bscl * b->y()) );
	return(result);
}

/* multiply two vectors together component-wise */
Vector2F* FitCurve::V2Mul(Vector2F* a, Vector2F* b, Vector2F* result)
{
	result->setX( a->x() * b->x() );
	result->setY( a->y() * b->y() );
	return(result);
}

/* return the distance between two points */
double FitCurve::V2DistanceBetween2Points(QPointF* a, QPointF* b)
{
    double dx = a->x() - b->x();
    double dy = a->y() - b->y();
	return(sqrt((dx*dx)+(dy*dy)));
}

/* return the vector perpendicular to the input vector a */
Vector2F* V2MakePerpendicular(Vector2F* a, Vector2F* ap)
{
	ap->setX( -a->y() );
	ap->setY( a->x() );
	return(ap);
}

/* create, initialize, and return a new vector */
Vector2F* FitCurve::V2New(double x, double y)
{
    Vector2F* v = (Vector2F*) malloc(sizeof(Vector2F));
	v->setX(x);  v->setY(y);
	return(v);
}

/* create, initialize, and return a duplicate vector */
Vector2F* FitCurve::V2Duplicate(Vector2F* a)
{
    Vector2F *v = (Vector2F*) malloc(sizeof(Vector2F));
	v->setX( a->x() );  v->setY( a->y() );
	return(v);
}