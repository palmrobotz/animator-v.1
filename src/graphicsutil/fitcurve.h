#ifndef FITCURVE_H
#define FITCURVE_H

#include <QList>
#include <QPointF>

typedef QPointF Vector2F;
typedef QPointF* BezierCurve;

/* Control point format (A is anchor point and C is the control point)
 * A1 C1 C2 A2 A2 C3 C4 A3 ...
 */
class FitCurve
{
public:
    void FitPointToCurve(QPointF* d, int nPts, double error);
    QList<QPointF> bezierControlPoint();
    
private:
    QList<QPointF> bezierPoint;
    static const int MAXPOINTS = 1000;
    
    void DrawBezierCurve(int n, BezierCurve curve);
    void FitCubic(QPointF* d, int first, int last, Vector2F tHat1, Vector2F tHat2, double error);
    BezierCurve GenerateBezier(QPointF* d, int first, int last, double* uPrime, Vector2F tHat1, Vector2F tHat2);
    double *Reparameterize(QPointF* d, int first, int last, double* u, BezierCurve bezCurve);
    double NewtonRaphsonRootFind(BezierCurve Q, QPointF P, double u);
    QPointF BezierII(int degree, QPointF* V, double t);

    double B0(double u);
    double B1(double u);
    double B2(double u);
    double B3(double u);

    Vector2F ComputeLeftTangent(QPointF* d, int end);
    Vector2F ComputeRightTangent(QPointF* d, int end);
    Vector2F ComputeCenterTangent(QPointF* d, int center);

    double* ChordLengthParameterize(QPointF* d, int first, int last);
    double ComputeMaxError(QPointF* d, int first, int last, BezierCurve bezCurve, double* u, int* splitPoint);

    Vector2F V2AddII(Vector2F a, Vector2F b);
    Vector2F V2ScaleIII(Vector2F v, double s);
    Vector2F V2SubII(Vector2F a, Vector2F b);
    double V2SquaredLength(Vector2F* a);
    double V2Length(Vector2F* a);
    Vector2F* V2Negate(Vector2F* v);
    Vector2F* V2Normalize(Vector2F* v);
    Vector2F* V2Scale(Vector2F* v, double newlen);
    Vector2F* V2Add(Vector2F* a, Vector2F* b, Vector2F* c);
    Vector2F* V2Sub(Vector2F* a, Vector2F* b, Vector2F* c);
    double V2Dot(Vector2F* a, Vector2F* b);
    Vector2F* V2Combine(Vector2F* a, Vector2F* b, Vector2F* result, double ascl, double bscl);
    Vector2F* V2Mul(Vector2F* a, Vector2F* b, Vector2F* result);
    double V2DistanceBetween2Points(QPointF* a, QPointF* b);
    Vector2F* V2MakePerpendicular(Vector2F* a, Vector2F* ap);
    Vector2F* V2New(double x, double y);
    Vector2F* V2Duplicate(Vector2F* a);
};

#endif // FITCURVE_H
