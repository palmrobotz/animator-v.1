#ifndef BEZIERPOINT_H
#define BEZIERPOINT_H

#include <QPointF>

typedef struct
{
    QPointF anchor;
    QPointF lControl;
    QPointF rControl;
}BPoint;

#endif // BEZIERPOINT_H
