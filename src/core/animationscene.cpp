
#include <QUndoStack>
#include <QGraphicsScene>

#include "animationscene.h"
//#include "timeline.h"

AnimationScene::AnimationScene(const QRectF& size)
    : mGraphicsScene(new QGraphicsScene(size))
    , mUndoStack(new QUndoStack())
    , mSceneSize(0, 0, 640, 480)
{
    mGraphicsScene->addRect(mSceneSize, QPen(Qt::gray), QBrush(Qt::white));
}

AnimationScene::AnimationScene(const QRectF& size, const QString &name)
    : mSceneName(name)
    , mGraphicsScene(new QGraphicsScene(size))
    , mUndoStack(new QUndoStack())
    , mSceneSize(0, 0, 640, 480)
{
    mGraphicsScene->addRect(mSceneSize, QPen(Qt::gray), QBrush(Qt::white));
}

AnimationScene::~AnimationScene()
{
    delete mGraphicsScene;
    delete mUndoStack;
}

void AnimationScene::clearScene() const
{
    mGraphicsScene->clear();
    mGraphicsScene->addRect(mSceneSize, QPen(Qt::gray), QBrush(Qt::white));
}

void AnimationScene::setSceneSize(int width, int height)
{
    mSceneSize.setRect(0, 0, width, height);
    clearScene();
    emit currentSceneSizeHasChanged();
}

QRectF AnimationScene::sceneSize() const
{
    return mSceneSize;
}

QString AnimationScene::sceneName() const
{
    return mSceneName;
}

QGraphicsScene* AnimationScene::graphicsScene() const
{
    return mGraphicsScene;
}

QUndoStack* AnimationScene::undoStack() const
{
    return mUndoStack;
}
