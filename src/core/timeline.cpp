#include <QtGui>
#include <QDebug>

#include "timeline.h"
#include "timelineoperation.h"

#include "../ui/timelineui.h"
#include "../ui/mygraphicsview.h"
#include "animateproperty.h"
#include "animationitem.h"
#include "../graphicsitem/groupgraphicsitem.h"

TimeLine::TimeLine(MyGraphicsView* currentView, AnimationScene* currentScene)
    : mGraphicsView(currentView)
    , mCurrentAnimationScene(currentScene)
    , mActiveLayer(0)
    , mCurrentTime(0)
    , mAnimationGroupCreated(0)
    , mCurrentItem(0)
{
    // Create the default first layer
    mAnimation.append(QList<AnimationItem*>());
    
    setCurrentTime(0);
    
    // Notify timeline when the view has change
    connect(currentView
            , SIGNAL(selectedObjectChanged(QList<QGraphicsItem *>, QGraphicsItem::GraphicsItemChange))
            , this
            , SLOT(graphicsObjectChanged(QList<QGraphicsItem *>, QGraphicsItem::GraphicsItemChange)));
    connect(currentView
            , SIGNAL(newObjectCreated(MyAbstractGraphicsObject*))
            , this
            , SLOT(newGraphicsObjectCreated(MyAbstractGraphicsObject*)));
}

int TimeLine::getActiveLayer()
{
    return mActiveLayer;
}

int TimeLine::getCurrentTime()
{
    return mCurrentTime;
}

QList<QList<AnimationItem*> > TimeLine::animationData()
{
    return mAnimation;
}

void TimeLine::addAnimationItem(int layer, AnimationItem* item)
{
    for (int i=0; i<mAnimation.at(layer).size(); i++)
    {
        AnimationItem* ai = mAnimation.at(layer).at(i);
        if (ai->startTime() > item->startTime())
        {
            mAnimation[layer].insert(i, item);
            return;
        }
    }
    
    mAnimation[layer].append(item);
}

bool TimeLine::removeAnimationItem(int layer, AnimationItem* item)
{
    return mAnimation[layer].removeOne(item);
}

bool TimeLine::isInAnimationBlock() const
{
    if (mActiveLayer == -1)
    {
        qDebug() << "Class - Timeline Func - isInAnimationBlock :: No any active layer";
        return false;
    }
    
    foreach (AnimationItem* item , mAnimation.at(mActiveLayer))
    {
        if (item->isContainTime(mCurrentTime))
        {
            return true;
        }
    }
    
    return false;
}

bool TimeLine::isKeyFrame() const
{
    if (mActiveLayer == -1)
    {
        qDebug() << "Class - Timeline Func - isKeyFrame :: No any active layer";
        return false;
    }
    
    foreach (AnimationItem* item , mAnimation.at(mActiveLayer))
    {
        if (item->isKeyFrame(mCurrentTime))
        {
            return true;
        }
    }
    
    return false;
}

int TimeLine::endTime() const
{
    int endTime = 0;
    
    foreach (QList<AnimationItem*> layer, mAnimation)
    {
        if (layer.last()->endTime() > endTime)
        {
            endTime = layer.last()->endTime();
        }
    }
    
    return endTime;
}

void TimeLine::invalidateView()
{
    emit invalidate();
}

void TimeLine::addLayer()
{
    // Append to the list
    if (mActiveLayer == -1)
    {
        mAnimation.append(QList<AnimationItem*>());
    }
    // Insert after current active layer
    else
    {
        mAnimation.insert(mActiveLayer+1, QList<AnimationItem*>());
    }
    
    emit invalidate();
}

void TimeLine::removeLayer()
{
    mAnimation.removeAt(mActiveLayer);
    emit invalidate();
}

void TimeLine::setActiveLayer(int layer)
{
    qDebug() << "Active layer is : " << layer;
    if (mActiveLayer != layer)
    {
        mActiveLayer = layer;
        setCurrentTime(mCurrentTime);
    }
}

void TimeLine::setCurrentTime(int time)
{
    if (time < 0)
    {
        qDebug() << "Time set into timeline is < 0";
    }
    
    mCurrentTime = time;

    mCurrentAnimationScene->clearScene();
    mGraphicsView->setMode(MyGraphicsView::NOTDRAWABLE);
    mCurrentItem = 0;
    
    int i=0, j=0;
    for (i=0; i<mAnimation.size(); i++) // For each layer
    {
        for (j=0; j<mAnimation.at(i).size(); j++) // For each item in layer
        {
            AnimationItem* item = mAnimation.at(i).at(j);
            if (item->isContainTime(mCurrentTime))
            {
                if (i == mActiveLayer)
                {
                    if (item->isKeyFrame(mCurrentTime))
                    {
                        qDebug() << "Class - TimeLine Func - setCurrentTime :: Current time : " << mCurrentTime << " is a keyframe.";
                        
                        mGraphicsView->setMode(MyGraphicsView::DRAWABLE);
                        MyAbstractGraphicsObject* gObj = item->item()->clone();
                        mCurrentAnimationScene->graphicsScene()->clearSelection();
                        mCurrentAnimationScene->graphicsScene()->addItem(gObj);
                        gObj->setSelected(true);
                        mCurrentItem = item;
                    }
                    else
                    {
                        qDebug() << "Class - TimeLine Func - setCurrentTime :: Current time : " << mCurrentTime << " is not a keyframe.";
                        
                        mGraphicsView->setMode(MyGraphicsView::NOTDRAWABLE);
                        
                        MyAbstractGraphicsObject* graphicsObj = 0;
                        item->animationGroup(&graphicsObj, mCurrentTime);
                        
                        mCurrentAnimationScene->graphicsScene()->clearSelection();
                        mCurrentAnimationScene->graphicsScene()->addItem(graphicsObj);
                        graphicsObj->setSelected(true);
                        mCurrentItem = item;
                    }
                }
                else
                {
                    qDebug() << "Class - TimeLine Func - setCurrentTime :: Difference layer";
                    
                    MyAbstractGraphicsObject* graphicsObj = 0;
                    item->animationGroup(&graphicsObj, mCurrentTime);
                    
                    graphicsObj->lock();
                    mCurrentAnimationScene->graphicsScene()->addItem(graphicsObj);
                }
            }
        }
    }
    
    if (mCurrentItem == 0 && mActiveLayer != -1)
    {
        mGraphicsView->setMode(MyGraphicsView::DRAWABLE);
    }
    
    qDebug() << "\n";
}

// FIXME: May be incorrect
void TimeLine::setBlockDuration(int startTime, double x)
{
    if (mActiveLayer == -1)
    {
        qDebug() << "Class - TimeLine Func - setBlockDuration :: mActive layer shouldn't be -1";
        return;
    }
    
    foreach (AnimationItem* item, mAnimation.at(mActiveLayer))
    {
        if (item->startTime() == startTime)
        {
            //item->setEndTime(TimeLineHelper::xToTime(x));
            //emit invalidate();
            ChangeBlockSize* changeSizeCommand = new ChangeBlockSize(this, item, item->endTime(), TimeLineHelper::xToTime(x));
            mCurrentAnimationScene->undoStack()->push(changeSizeCommand);
            
            return;
        }
    }
    
    qDebug() << "Class - TimeLine Func - setBlockDuration :: Can't find item with that start time";
}

void TimeLine::setBlockStartTime(int startTime, double x)
{
    if (mActiveLayer == -1)
    {
        qDebug() << "Class - TimeLine Func - setBlockStartTime :: mActive layer shouldn't be -1";
        return;
    }
    
    foreach (AnimationItem* item, mAnimation.at(mActiveLayer))
    {
        if (item->startTime() == startTime)
        {
            //int newTime = TimeLineHelper::xToTime(x);
            //item->setStartTime(newTime);
            //emit invalidate();
            
            ChangeBlockStartTime* changeBlockCommand = new ChangeBlockStartTime(this, item, item->startTime(), TimeLineHelper::xToTime(x));
            mCurrentAnimationScene->undoStack()->push(changeBlockCommand);
            
            return;
        }
    }
    
    qDebug() << "Class - TimeLine Func - setBlockStartTime :: Can't find item with that start time";
}

// FIXME: Accept other change case
void TimeLine::graphicsObjectChanged(QList<QGraphicsItem *> selectedObject, QGraphicsItem::GraphicsItemChange change)
{
    if (mCurrentItem == 0)
    {
        qDebug() << "Class - TimeLine Func - GraphicsObjectChanged :: Changed when no item (item = NULL)";
        return;
    }
    if (selectedObject.size() > 1)
    {
        qDebug() << "Class - TimeLine Func - GraphicsObjectChanged :: More than one object changed at the same time";
        return;
    }
    else if (selectedObject.size() == 0)
    {
        qDebug() << "Class - TimeLine Func - GraphicsObjectChanged :: No object selected";
        return;
    }
        
    qDebug() << "Class - TimeLine Func - GraphicsObjectChanged :: Create Frame";
    if (change == QGraphicsItem::ItemPositionHasChanged)
    {
        AnimationProperty* property = mCurrentItem->newFrame(mCurrentTime, change, selectedObject.at(0)->scenePos());
        //emit invalidate();
        AddFrameCommand* command = new AddFrameCommand(this, mCurrentItem, property);
        mCurrentAnimationScene->undoStack()->push(command);
    }
}

void TimeLine::newGraphicsObjectCreated(MyAbstractGraphicsObject* object)
{
    // Create block
    if (mCurrentItem == 0 && mActiveLayer != -1)
    {
        AnimationItem* newItem = new AnimationItem(object, mCurrentTime, mCurrentTime+200);
        newItem->addFrame(mCurrentTime, QGraphicsItem::ItemPositionHasChanged , object->scenePos());
        newItem->addFrame(mCurrentTime, QGraphicsItem::ItemRotationHasChanged, object->rotation());
        newItem->addFrame(mCurrentTime, QGraphicsItem::ItemScaleHasChanged, object->scale());
        
        // Set to the current block
        mCurrentItem = newItem;
        
        // This view invalidate the view and setCurrentTime(Update graphics view automatically)
        AddNewBockCommand* command = new AddNewBockCommand(this, newItem);
        mCurrentAnimationScene->undoStack()->push(command);
    }
}

void TimeLine::moveLayerUp()
{
    if (mActiveLayer == -1 || mActiveLayer == 0)
    {
        return;
    }
    
    mAnimation.swap(mActiveLayer, mActiveLayer-1);
    emit invalidate();
}

void TimeLine::moveLayerDown()
{
    if (mActiveLayer == -1 || mActiveLayer == mAnimation.size()-1)
    {
        return;
    }
    
    mAnimation.swap(mActiveLayer, mActiveLayer+1);
    emit invalidate();
}

void TimeLine::removeAnimationBlock()
{
    
}

QParallelAnimationGroup* TimeLine::createAnimationGroup()
{
    QParallelAnimationGroup* mainAnimationGroup = new QParallelAnimationGroup();
    
    int i=0;
    AnimationItem* prevBlock;
    AnimationItem* block;
    foreach (QList<AnimationItem*> layer, mAnimation)
    {
        QSequentialAnimationGroup* seqAnimationGroup = new QSequentialAnimationGroup();
        
        for (i=0; i<layer.size(); i++)
        {
            if (i == 0)
            {
                block = layer.at(i);
                
                if (block->startTime() != 0)
                {
                    seqAnimationGroup->addPause(block->startTime());
                }
            }
            else
            {
                prevBlock = layer.at(i-1);
                block = layer.at(i);
                if (prevBlock->endTime() != block->startTime())
                {
                    seqAnimationGroup->addPause(block->startTime()-prevBlock->endTime());
                }
            }
            
            QSequentialAnimationGroup* subAnimationGroup = block->animationGroup();
            seqAnimationGroup->addAnimation(subAnimationGroup);
        }
        
        mainAnimationGroup->addAnimation(seqAnimationGroup);
    }
    
    return mainAnimationGroup;
}
