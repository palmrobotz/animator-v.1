#ifndef ANIMATIONSCENE_H
#define ANIMATIONSCENE_H

#include <QObject>
#include <QRectF>

class QGraphicsScene;
class QUndoStack;
//class TimeLine;

/*! \class AnimationScene
 * \brief AnimationScene is a class for represent each scene of the animation. 
 * 
 * Each scene has its own QGraphicsScene, QUndoStack and TimeLine instance
 */
class AnimationScene : public QObject
{
    Q_OBJECT
    
public:
    AnimationScene(const QRectF& size);
    AnimationScene(const QRectF& size, const QString& name);

    ~AnimationScene();
    
    QString sceneName() const;
    QGraphicsScene* graphicsScene() const;
    QUndoStack* undoStack() const;
    
    /* Properly clear the graphics scene
     * (Delete all item and add a rectangle bound area) */
    void clearScene() const;
    /* Get the scene size */
    QRectF sceneSize() const;
    
signals:
    /* Emit to notify that this scene size has changed */
    void currentSceneSizeHasChanged();
    
public slots:
    void setSceneSize(int width, int height);

private:
    QString mSceneName;
    QGraphicsScene* mGraphicsScene;
    QUndoStack* mUndoStack;
    //TimeLine* mTimeLine;
    
    QRectF mSceneSize;
};

#endif // ANIMATIONSCENE_H
