
#include "animateproperty.h"

AnimationProperty::AnimationProperty()
    : mAnimationType(TWEEN)
    , mEasingCurve(QEasingCurve::Linear)
    , mPropertyChanged(NONE)
    , mTime(0)
    , mPosition(0, 0)
    , mRotation(0)
    , mScale(1)
{
}

AnimationProperty::AnimationType AnimationProperty::animationType() const
{
    return mAnimationType;
}

QEasingCurve::Type AnimationProperty::easingCurve() const
{
    return mEasingCurve;
}

AnimationProperty::PropertiesChanged AnimationProperty::propertyChanged() const
{
    return mPropertyChanged;
}

int AnimationProperty::time() const
{
    return mTime;
}

QPointF AnimationProperty::position() const
{
    return mPosition;
}

double AnimationProperty::rotation() const
{
    return mRotation;
}

double AnimationProperty::scale() const
{
    return mScale;
}

void AnimationProperty::setEasingCurve(QEasingCurve::Type curve)
{
    mEasingCurve = curve;
}

void AnimationProperty::setTime(int time)
{
    mTime = time;
}

void AnimationProperty::setPosition(QPointF position)
{
    mPosition = position;
    mPropertyChanged |= POSITION;
}

void AnimationProperty::setRotation(double rotation)
{
    mRotation = rotation;
    mPropertyChanged |= ROTATION;
}

void AnimationProperty::setScale(double scale)
{
    mScale = scale;
    mPropertyChanged |= SCALE;
}