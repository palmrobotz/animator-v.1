#include <QtGui>

#include "svg.h"

#include "../graphicsitem/myabstractgraphicsobject.h"
#include "../graphicsitem/mybeziergraphicsobject.h"

#include "../core/animationitem.h"
#include "../core/animateproperty.h"

SvgGenerator::SvgGenerator(const QRectF& size)
{
    init(size.width(), size.height(), size.x(), size.y());
}

SvgGenerator::SvgGenerator(int width, int height, int x, int y)
{
    init(width, height, x, y);
}

void SvgGenerator::init(int width, int height, int x, int y)
{
    group = 0;
    bPath = 0;
    /* Create header */
    list.append("<?xml version=\"1.0\"?>");
    list.append("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">");
    QString svgHeader;
    svgHeader.sprintf("<svg width=\"%d\" height=\"%d\" viewBox=\"%d %d %d %d\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.2\" baseProfile=\"tiny\">",width,height,x,y,width,height);
    list.append(svgHeader);
    list.append("</svg>");
}

void SvgGenerator::setAnimationData(QList<QList<AnimationItem*> > mAnimation)
{
    foreach (QList<AnimationItem*> layer, mAnimation)
    {
        foreach (AnimationItem* block, layer)
        {
            MyAbstractGraphicsObject* mAbsGraphicsObj = block->item();
            Q_ASSERT (mAbsGraphicsObj->type() == MyBezierGraphicsObject::Type);
            MyBezierGraphicsObject* mBezierGraphicsObj = (MyBezierGraphicsObject *) mAbsGraphicsObj;
            
            AnimationProperty* property = 0;
            AnimationProperty* prevProperty = 0;
            
            int i=0;
            for (i=0; i<block->frames().size(); i++)
            {
                property = block->frames().at(i);
                
                if (i == 0)
                {
                    path(mBezierGraphicsObj->bezierPoint()
                         , property->position().x()
                         , property->position().y()
                         , mBezierGraphicsObj->brush().color().name().toAscii()
                         , mBezierGraphicsObj->pen().color().name().toAscii()
                         , mBezierGraphicsObj->pen().width() == 0 ? 1 : mBezierGraphicsObj->pen().width()
                         , mBezierGraphicsObj->pen().color().alphaF()
                         , mBezierGraphicsObj->brush().color().alphaF());
                    continue;
                }
                
                prevProperty = block->frames().at(i-1);
                
                double begin = prevProperty->time() / 1000.0;
                double duration = (property->time()/1000.0) - begin;
                
                if (property->propertyChanged() & AnimationProperty::POSITION)
                {
                    translate(property->position().x() - prevProperty->position().x()
                              , property->position().y() - prevProperty->position().y()
                              , begin, duration);
                }
                if (property->propertyChanged() & AnimationProperty::ROTATION)
                {
                    rotate(property->rotation() - prevProperty->rotation()
                           , begin, duration);
                }
                if (property->propertyChanged() & AnimationProperty::SCALE)
                {
                    scale(property->scale() - prevProperty->scale()
                          , begin, duration);
                }
            }
            
            // Stub property for pause
            // Jump said that just use rotate with 0
            rotate(property->rotation(), property->time()/1000.0, (block->endTime() - property->time()) / 1000.0);
            visible(block->startTime() / 1000.0, block->endTime() / 1000.0);
        }
        
    }
}

void SvgGenerator::addGroup()
{
    list.insert(list.count()-1, "<g>");
}

void SvgGenerator::endGroup()
{
    list.insert(list.count()-1, "</g>");
}

void SvgGenerator::path(QList<BPoint> point,int x,int y,const char* fill,const char* stroke,int strokeWidth, double strokeOpa, double fillOpa)
{
    QString svgTag;
    QString buffer;

    //if path is 1, create </path>
    if(bPath==1)
    {
        list.insert(list.count()-1,"</path>");
    }
    bPath=1;
    buffer.sprintf("<path d=\"M%lf %lf ",point.at(0).anchor.x()+x,point.at(0).anchor.y()+y);
    svgTag.append(buffer);

    //convert list to svg path
    int i=1;
    for(i=1;i<point.count();i++)
    {
        QPointF startPoint = point.at(i-1).anchor;
        QPointF endPoint = point.at(i).anchor;
        QPointF firstControl = point.at(i-1).rControl;
        QPointF secondControl = point.at(i).lControl;
        buffer.clear();
        //if no control point between two point, create line
        if(firstControl.isNull()&&secondControl.isNull())
        {
            buffer.sprintf("L%lf %lf ",endPoint.rx()+x,endPoint.ry()+y);
            svgTag.append(buffer);
        }
        //if there are two control point between two point, create cubic bezier
        else if(!(firstControl.isNull())&&!(secondControl.isNull()))
        {
            buffer.sprintf("C%lf %lf %lf %lf %lf %lf ",firstControl.rx()+x,firstControl.ry()+y,
                                                secondControl.rx()+x,secondControl.ry()+y,
                                                endPoint.rx()+x,endPoint.ry()+y);
            svgTag.append(buffer);
        }
        //if there is one control point between two point, create quadratic bezier
        else
        {
            if(firstControl.isNull())
            {
                buffer.sprintf("Q%lf %lf %lf %lf ",secondControl.rx()+x,secondControl.ry()+y,
                                               endPoint.rx()+x,endPoint.ry()+y);
            }
            else
            {
                buffer.sprintf("Q%lf %lf %lf %lf ",firstControl.rx()+x,firstControl.ry()+y,
                                               endPoint.rx()+x,endPoint.ry()+y);
            }
            svgTag.append(buffer);
        }
    }
    buffer.clear();
    buffer.sprintf("\" stroke=\"%s\" stroke-opacity=\"%.lf\" stroke-width=\"%d\" fill=\"%s\" fill-opacity=\"%.lf\" visibility=\"hidden\" >",stroke, strokeOpa, strokeWidth, fill, fillOpa);
    svgTag.append(buffer);

    list.insert(list.count()-1,svgTag);
}

void SvgGenerator::translate(double x,double y,double begin,double duration,char* fill)
{
    QString svgTag;
    svgTag.sprintf("<animateTransform attributeName=\"transform\" attributeType=\"XML\" type=\"translate\" from=\"0,0\" to=\"%lf,%lf\" begin=\"%lf\" dur=\"%lf\" additive=\"sum\" fill=\"%s\" />",x,y,begin,duration,fill);
    list.insert(list.count()-1,svgTag);
}

void SvgGenerator::rotate(double degree,double begin,double duration,char* fill)
{
    QString svgTag;
    svgTag.sprintf("<animateTransform attributeName=\"transform\" attributeType=\"XML\" type=\"rotate\" from=\"0\" to=\"%lf\" begin=\"%lf\" dur=\"%lf\" additive=\"sum\" fill=\"%s\" />",degree,begin,duration,fill);
    list.insert(list.count()-1,svgTag);
}

void SvgGenerator::scale(double scale,double begin,double duration,char* fill)
{
    QString svgTag;
    svgTag.sprintf("<animateTransform attributeName=\"transform\" attributeType=\"XML\" type=\"scale\" from=\"1\" to=\"%lf\" begin=\"%lf\" dur=\"%lf\" additive=\"sum\" fill=\"%s\" />",scale,begin,duration,fill);
    list.insert(list.count()-1,svgTag);
}

void SvgGenerator::visible(double begin, double end)
{
    QString svgTag;
    svgTag.sprintf("<set attributeName=\"visibility\" attrivuteType=\"CSS\"  to=\"visible\" begin=\"%lf\" end=\"%lf\" />" ,begin, end);
    list.insert(list.count()-1,svgTag);
}

void SvgGenerator::color(char* type,QColor color,double begin,double duration,char* fill)
{
    QString svgTag;
    svgTag.sprintf("<animate attributeName=\"%s\" to=\"%s\" begin=\"%lf\" dur=\"%lf\" additive=\"sum\" fill=\"%s\"/>",type,color.name().toStdString().c_str(),begin,duration,fill);
    list.insert(list.count()-1,svgTag);
}

void SvgGenerator::set(char* attribute,int value,double begin,double duration,char* fill)
{
    QString svgTag;
    svgTag.sprintf("<animate attributeName=\"%s\" to=\"%d\" begin=\"%lf\" dur=\"%lf\" additive=\"sum\" fill=\"%s\"/>",attribute,value,begin,duration,fill);
    list.insert(list.count()-1,svgTag);
}

QString SvgGenerator::exportSVG()
{
    //if path is 1, create </path>
    if(bPath==1)
    {
        list.insert(list.count()-1,"</path>");
    }

    QString output;
    int i=0;
    for(i=0;i<list.count();i++)
    {
        output.append(list.at(i));
        output.append("\n");
    }
    return output;
}

