#ifndef TIMELINE_H
#define TIMELINE_H

#include <QObject>
#include <QList>
#include <QGraphicsItem>

class QParallelAnimationGroup;
class MyAbstractGraphicsObject;
class GroupGraphicsItem;
class MyGraphicsView;
class AnimationScene;
class AnimationItem;
class AnimationProperty;

/*! \class TimeLine 
 * \brief The TimeLine class is a class use to store the animation data.
 *
 * The TimeLine class is used with TimeLineWidget to provide a view for user interaction
 */
class TimeLine : public QObject
{
    Q_OBJECT
    
public:
    explicit TimeLine(MyGraphicsView* currentView, AnimationScene* currentScene);
    
    int getActiveLayer();
    int getCurrentTime();
    QList<QList<AnimationItem*> > animationData();
    
    void addAnimationItem(int layer, AnimationItem* item);
    bool removeAnimationItem(int layer, AnimationItem* item);
    
    bool isInAnimationBlock() const;
    bool isKeyFrame() const;
    
    int endTime() const;

    void invalidateView();
    
signals:
    // Signal the UI that the model has changed
    void invalidate();
    
public slots:
    /*! \brief Add new layer after the current active layer or at
     * the lowest layer if there is no any active layer */
    void addLayer();
    /*! \brief Remove the current active layer */
    void removeLayer();
    
    void setActiveLayer(int layer);
    void setCurrentTime(int time);
    
    void setBlockDuration(int startTime, double x);
    void setBlockStartTime(int startTime, double x);
    
    void graphicsObjectChanged(QList<QGraphicsItem *> selectedObject, QGraphicsItem::GraphicsItemChange change);
    
    void newGraphicsObjectCreated(MyAbstractGraphicsObject* object);
    
    void moveLayerUp();
    void moveLayerDown();
    
private:
    // Block is add automatically
    //void addAnimationBlock();
    void removeAnimationBlock();
    // May be use for implement interpolate changed
    //void addAnimationFrame();
    //void removeAnimationFrame();
    
    QParallelAnimationGroup* createAnimationGroup();
    
    MyGraphicsView* mGraphicsView;
    AnimationScene* mCurrentAnimationScene;
    
    int mActiveLayer; // -1 mean not select any
    int mCurrentTime; // in mili-sec
    
    //AnimationProperty* mCurrentProperty;
    
    // Store animation group create from createAnimationGroup
    QParallelAnimationGroup* mAnimationGroupCreated;
    
    // Use as a stub global parent
    //GroupGraphicsItem* mParentGraphicsObject;
    //GroupGraphicsItem* mReuseParentGraphicsObj;
    AnimationItem* mCurrentItem;
    QList<QList<AnimationItem*> > mAnimation;
};

#endif // TIMELINE_H
