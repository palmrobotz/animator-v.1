#include <QDebug>
#include "timelineoperation.h"

#include "timeline.h"
#include "animationitem.h"
#include "animateproperty.h"

AddNewBockCommand::AddNewBockCommand(TimeLine* timeline/*, QList<AnimationItem*> * layerData*/, AnimationItem* item)
    : mTimeline(timeline)
    , mItem(item)
{
    mLayer = timeline->getActiveLayer();
    setText("Add new animation block");
}

void AddNewBockCommand::undo()
{
    if (!mTimeline->removeAnimationItem(mLayer, mItem))
    {
        qDebug() << "Class - TimeLineOperation Func - Undo :: Can't undo because can't find item in layer";
    }
    mTimeline->invalidateView();
    mTimeline->setCurrentTime(mTimeline->getCurrentTime());
}

void AddNewBockCommand::redo()
{
    mTimeline->addAnimationItem(mLayer, mItem);
    mTimeline->invalidateView();
    mTimeline->setCurrentTime(mTimeline->getCurrentTime());
}


AddFrameCommand::AddFrameCommand(TimeLine* timeline, AnimationItem* item, AnimationProperty* property)
    : mTimeline(timeline)
    , mItem(item)
    , mProperty(property)
{
    setText(QString("Add new frame at %1 sec").arg(mTimeline->getCurrentTime()));
}

void AddFrameCommand::undo()
{
    mItem->removeProperty(mProperty);
    mTimeline->invalidateView();
}

void AddFrameCommand::redo()
{
    mItem->addProperty(mProperty);
    mTimeline->invalidateView();
}

int AddFrameCommand::id() const
{
    return 1;
}

bool AddFrameCommand::mergeWith(const QUndoCommand * command)
{
    if (command->id() == id() && (mProperty == ((AddFrameCommand *)command)->mProperty))
    {
        return true;
    }
    return false;
}

ChangeBlockSize::ChangeBlockSize(TimeLine* timeline, AnimationItem* item, int oldEndTime, int newEndTime)
    : mTimeline(timeline)
    , mItem(item)
    , mOldEndTime(oldEndTime)
    , mNewEndTime(newEndTime)
{
    setText(QString("Set block end time from %1 to %2").arg(mOldEndTime).arg(mNewEndTime));
}

void ChangeBlockSize::undo()
{
    mItem->setEndTime(mOldEndTime);
    mTimeline->invalidateView();
}

void ChangeBlockSize::redo()
{
    mItem->setEndTime(mNewEndTime);
    mTimeline->invalidateView();
}

int ChangeBlockSize::id() const
{
    return 2;
}

bool ChangeBlockSize::mergeWith(const QUndoCommand * command)
{
    if (command->id() == 2 && (mItem == ((ChangeBlockSize*) command)->mItem))
    {
        return true;
    }
    return false;
}

ChangeBlockStartTime::ChangeBlockStartTime(TimeLine* timeline, AnimationItem* item, int oldStartTime, int newStartTime)
    : mTimeline(timeline)
    , mItem(item)
    , mOldStartTime(oldStartTime)
    , mNewStartTime(newStartTime)
{
    setText(QString("Set block start time from %1 to %2").arg(mOldStartTime).arg(mNewStartTime));
}

void ChangeBlockStartTime::undo()
{
    mItem->setStartTime(mOldStartTime);
    mTimeline->invalidateView();
}

void ChangeBlockStartTime::redo()
{
    mItem->setStartTime(mNewStartTime);
    mTimeline->invalidateView();
}
