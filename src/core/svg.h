#ifndef SVG_H
#define SVG_H

#include <QList>
#include <QString>

#include "../graphicsutil/bezierpoint.h"

class QParallelAnimationGroup;
class AnimationItem;

// TODO: Consider changed to QStringList and char* -> QString
// TODO: Might be faster to write a file while convert
class SvgGenerator
{
    
public:
    SvgGenerator(const QRectF& size);
    SvgGenerator(int width, int height, int x, int y);

    /* Get the data source and then called each method */
    void setAnimationData(QList<QList<AnimationItem*> > mAnimation);
    
    /* s */
    void addGroup();
    void endGroup();
    
    void path(QList<BPoint> point,int x,int y,const char* fill="none",const char* stroke="black",int strokeWidth=1, double strokeOpa=1, double fillOpa=1);
    void translate(double x,double y,double begin,double duration,char* fill="freeze");
    void rotate(double degree,double begin,double duration,char* fill="freeze");
    void scale(double scale,double begin,double duration,char* fill="freeze");
    void visible(double begin, double end);
    
    /*type - stroke or fill*/
    void color(char* type,QColor color,double begin,double duration,char* fill="freeze");

    void set(char* attribute,int value,double begin,double duration,char* fill="freeze");

    QString exportSVG();

private:
    void init(int width, int height, int x, int y);
    
    QList<QString> list;//list of svg tags
    int bPath;//(0 or 1)if path is 0, cannot create animate,
            //if path is 1,when add another path </path> will auto generate before create new path
    int group;// number of unclosed group
    
    QParallelAnimationGroup* mAnimationGroup;
    
};

#endif // SVG_H
