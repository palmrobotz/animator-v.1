#ifndef ANIMATEPROPERTY_H
#define ANIMATEPROPERTY_H

// TODO: Allow changing of the easing curve

#include <QObject>
#include <QEasingCurve>
#include <QPointF>
#include <QList>

class MyAbstractGraphicsObject;
class QSequentialAnimationGroup;

/*!
 \class AnimateProperty
 \brief The AnimateProperty class is a class use to store the property of each animation
 */
class AnimationProperty : public QObject
{
    Q_OBJECT
    
public:
    /*! Supported Animation Type. */
    enum AnimationType
    {
        TWEEN,/*!< Motion tween animation */
        SHAPE /*!< Shape tween (Still not supported) */
    };
    
    //Q_FLAGS(PropertyChanged PropertiesChanged)
    
    /*! Property that changed in the animation. (Only for Tween animation) */
    enum PropertyChanged
    {
        NONE = 0,
        POSITION = 0x01, /*!< Changed position */
        ROTATION = 0x02, /*!< Changed rotation */
        SCALE = 0x04,    /*!< Changed scale (Not supported yet) */
        OPACITY = 0x08   /*!< Changed opacity (Not supported yet) */
    };
    Q_DECLARE_FLAGS(PropertiesChanged, PropertyChanged)
    
    explicit AnimationProperty();
    
    AnimationType animationType() const;
    QEasingCurve::Type easingCurve() const;
    PropertiesChanged propertyChanged() const;
    int time() const;
    QPointF position() const;
    double rotation() const;
    double scale() const;

public slots:
    void setEasingCurve(QEasingCurve::Type curve);
    void setTime(int time);
    void setPosition(QPointF position);
    void setRotation(double rotation);
    void setScale(double scale);

private:
    AnimationType mAnimationType;
    QEasingCurve::Type mEasingCurve;
    PropertiesChanged mPropertyChanged;
    
    int mTime;
    QPointF mPosition;
    double mRotation;
    double mScale;
};

#endif // ANIMATEPROPERTY_H
