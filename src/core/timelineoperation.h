#ifndef TIMELINEOPERATION_H
#define TIMELINEOPERATION_H

#include <QUndoCommand>
#include <QList>

class TimeLine;
class AnimationItem;
class AnimationProperty;

class AddNewBockCommand : public QUndoCommand
{
public:
    AddNewBockCommand(TimeLine* timeline, AnimationItem* item);
    
    void undo();
    void redo();
    
private:
    TimeLine* mTimeline;
    int mLayer;
    AnimationItem* mItem;
};

class AddFrameCommand : public QUndoCommand
{
public:
    AddFrameCommand(TimeLine* timeline, AnimationItem* item, AnimationProperty* property);
    
    void undo();
    void redo();
    
    int id() const;
    bool mergeWith(const QUndoCommand * command);
    
private:
    TimeLine* mTimeline;
    AnimationItem* mItem;
    AnimationProperty* mProperty;
};

class ChangeBlockSize : public QUndoCommand
{
public:
    ChangeBlockSize(TimeLine* timeline, AnimationItem* item, int oldEndTime, int newEndTime);
    
    void undo();
    void redo();
    
    int id() const;
    bool mergeWith(const QUndoCommand * command);

private:
    TimeLine* mTimeline;
    AnimationItem* mItem;
    int mOldEndTime;
    int mNewEndTime;

};

class ChangeBlockStartTime : public QUndoCommand
{
public:
    ChangeBlockStartTime(TimeLine* timeline, AnimationItem* item, int oldStartTime, int newStartTime);
    
    void undo();
    void redo();
    
private:
    TimeLine* mTimeline;
    AnimationItem* mItem;
    int mOldStartTime;
    int mNewStartTime;
    
};

#endif // TIMELINEOPERATION_H
