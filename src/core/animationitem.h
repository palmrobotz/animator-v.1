#ifndef ANIMATIONITEM_H
#define ANIMATIONITEM_H

#include <QObject>
#include <QList>
#include <QGraphicsItem>

#include "../graphicsitem/myabstractgraphicsobject.h"

class QSequentialAnimationGroup;
class MyAbstractGraphicsObject;
class AnimationProperty;

class AnimationItem : public QObject
{
    Q_OBJECT
    
public:
    /*! Construct an empty AnimateItem */
    explicit AnimationItem(MyAbstractGraphicsObject* graphicsItem, double startTime, double endTime);
    
    /*!
     * \brief Get this animation QParallelAnimationGroup instance
     * \return an instance of QParallelAnimationGroup
     */
    // The graphicsobject is a clone of the item set for this object
    QSequentialAnimationGroup* animationGroup(MyAbstractGraphicsObject** graphicsObject = 0, int time = 0);
    
    MyAbstractGraphicsObject* item() const;
    QList<AnimationProperty *> frames() const;
    
    int startTime() const;
    int endTime() const;
    int duration() const;
    
    int mapToItemTime(int t) const; // Map timeline time to this animationitem local time
    // [startTime, endTime)
    bool isContainTime(int t) const; // Check to see if t is between start and end time inclusive
    
    bool isFrame(int t);
    bool isKeyFrame(int t); // Keyframe is a first frame
    
    // auto reuse and append to old property so save to call multiple time
    AnimationProperty* newFrame(int time, QGraphicsItem::GraphicsItemChange change, const QVariant& value);
    void addFrame(int time, QGraphicsItem::GraphicsItemChange change, const QVariant& value);
    
    // override exist if time is equal
    void addProperty(AnimationProperty* property);
    bool removeProperty(AnimationProperty* property);
    
public slots:
    void setItem(MyAbstractGraphicsObject* graphicsItem);
    void setStartTime(int startTime);
    void setEndTime(int endTime);
    
private:
    MyAbstractGraphicsObject* mGraphicsItem;
    QList<AnimationProperty *> mKeyFrame;
    
    int mStartTime;
    int mEndTime;
};


#endif // ANIMATIONITEM_H
