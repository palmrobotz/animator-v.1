
#include <QDebug>
#include <QSequentialAnimationGroup>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QGraphicsItem>

#include "animationitem.h"
#include "animateproperty.h"
#include "../graphicsitem/myabstractgraphicsobject.h"

AnimationItem::AnimationItem(MyAbstractGraphicsObject* graphicsItem, double startTime, double endTime)
    : mGraphicsItem(graphicsItem)
    , mStartTime(startTime)
    , mEndTime(endTime)
{
}

// The sequential animation group IS NOT usable if graphicsObject and/or time is specify
// To use the sequential animation DO NOT set graphicsObject and/or time
QSequentialAnimationGroup* AnimationItem::animationGroup(MyAbstractGraphicsObject** graphicsObject, int time)
{
    QSequentialAnimationGroup* animationGroup = new QSequentialAnimationGroup();
    MyAbstractGraphicsObject* cloneObject = mGraphicsItem->clone();
    if (graphicsObject != 0)
    {
        *graphicsObject = cloneObject;
    }
    //qDebug() << "Clone Child size : " << cloneObject->childItems().size();
    
    bool interestedFrame = false;
    int i = 0;
    QParallelAnimationGroup* subAnimationGroup = 0;
    QPropertyAnimation* propertyAnimation = 0;
    AnimationProperty* currProperty = 0;
    AnimationProperty* nextProperty = 0;
    qDebug() << "Class - AnimationItem Func - animationGroup :: Num of item " << mKeyFrame.size();
    for (i=0; i<mKeyFrame.size(); i++)
    {
        currProperty = mKeyFrame.at(i);
        if (i != mKeyFrame.size()-1)
            nextProperty = mKeyFrame.at(i+1);
        else
            nextProperty = 0;
        
        if (graphicsObject != 0)
        {
            if (nextProperty != 0 && time >= currProperty->time() &&  time < nextProperty->time())
            {
                interestedFrame = true;
            }
            else if (nextProperty == 0 && time >= currProperty->time() && time < mEndTime)
            {
                interestedFrame = true;
            }
            else
            {
                interestedFrame = false;
            }
        }
        
        subAnimationGroup = new QParallelAnimationGroup();
        
        if (currProperty->propertyChanged() & AnimationProperty::POSITION)
        {
            propertyAnimation = new QPropertyAnimation(cloneObject, "pos");
            
            qDebug() << "Class - AnimationItem Func - animationGroup :: Start Value : " << currProperty->position();
            propertyAnimation->setStartValue(currProperty->position());
            
            if (i != mKeyFrame.size()-1)
            {
                qDebug() << "Class - AnimationItem Func - animationGroup :: End Value : " << nextProperty->position();
                propertyAnimation->setEndValue(nextProperty->position());
                qDebug() << "Class - AnimationItem Func - animationGroup :: Duration : " << nextProperty->time() - currProperty->time();
                propertyAnimation->setDuration(nextProperty->time() - currProperty->time());
            }
            else
            {
                propertyAnimation->setEndValue(currProperty->position());
                propertyAnimation->setDuration(endTime() - currProperty->time());
            }
            
            propertyAnimation->setEasingCurve(currProperty->easingCurve());
            
            // Add this animation to the sub animation group
            subAnimationGroup->addAnimation(propertyAnimation);
            
            if (interestedFrame)
            {
                propertyAnimation->setCurrentTime(time - currProperty->time());
                cloneObject->setProperty("pos", propertyAnimation->currentValue());
            }
        }
        if (currProperty->propertyChanged() & AnimationProperty::ROTATION)
        {
            propertyAnimation = new QPropertyAnimation(cloneObject, "rotation");
            
            propertyAnimation->setStartValue(currProperty->rotation());
            
            if (i != mKeyFrame.size()-1)
            {
                propertyAnimation->setEndValue(nextProperty->rotation());
                propertyAnimation->setDuration(nextProperty->time() - currProperty->time());
            }
            else
            {
                propertyAnimation->setEndValue(currProperty->rotation());
                propertyAnimation->setDuration(endTime() - currProperty->time());
            }
            
            propertyAnimation->setEasingCurve(currProperty->easingCurve());
            
            // Add this animation to the sub animation group
            subAnimationGroup->addAnimation(propertyAnimation);
            
            if (interestedFrame)
            {
                propertyAnimation->setCurrentTime(time - currProperty->time());
                cloneObject->setProperty("rotation", propertyAnimation->currentValue());
            }
        }
        if (currProperty->propertyChanged() & AnimationProperty::SCALE)
        {
            propertyAnimation = new QPropertyAnimation(cloneObject, "scale");
            
            propertyAnimation->setStartValue(currProperty->scale());
            
            if (i != mKeyFrame.size()-1)
            {
                propertyAnimation->setEndValue(nextProperty->scale());
                propertyAnimation->setDuration(nextProperty->time() - currProperty->time());
            }
            else
            {
                propertyAnimation->setEndValue(currProperty->scale());
                propertyAnimation->setDuration(endTime() - currProperty->time());
            }
            
            propertyAnimation->setEasingCurve(currProperty->easingCurve());
            
            // Add this animation to the sub animation group
            subAnimationGroup->addAnimation(propertyAnimation);
            
            if (interestedFrame)
            {
                propertyAnimation->setCurrentTime(time - currProperty->time());
                cloneObject->setProperty("scale", propertyAnimation->currentValue());
            }
        }
        if (currProperty->propertyChanged() == 0)
        {
            qDebug() << "Class - AnimationItem Func - animationGroup :: Frame has no property!!!";
        }
        
        animationGroup->addAnimation(subAnimationGroup);
        
        if (interestedFrame)
        {
            delete animationGroup;
            return 0;
        }
    }
    
    return animationGroup;
}

MyAbstractGraphicsObject* AnimationItem::item() const
{
    return mGraphicsItem;
}

QList<AnimationProperty *> AnimationItem::frames() const
{
    return mKeyFrame;
}

int AnimationItem::startTime() const
{
    return mStartTime;
}

int AnimationItem::endTime() const
{
    return mEndTime;
}

int AnimationItem::duration() const
{
    return mEndTime - mStartTime;
}

int AnimationItem::mapToItemTime(int t) const
{
    if (t<mStartTime || t>=mEndTime)
        qDebug() << "Class - AnimationItem Func - mapToItemTime :: t is not in this item range";
    return t-mStartTime;
}

bool AnimationItem::isContainTime(int t) const
{
    return (mStartTime <= t) && (t < mEndTime);
}

bool AnimationItem::isFrame(int t)
{
    foreach (AnimationProperty* property, mKeyFrame)
    {
        if (property->time() == t)
            return true;
    }
    return false;
}

bool AnimationItem::isKeyFrame(int t)
{
    return (t==mStartTime);
}

void AnimationItem::setItem(MyAbstractGraphicsObject* graphicsItem)
{
    mGraphicsItem = graphicsItem;
}

// Shift time of all frame and end time
void AnimationItem::setStartTime(int startTime)
{
    int delta = startTime - mStartTime;
    
    mStartTime += delta;
    mEndTime += delta;
    
    foreach (AnimationProperty* prop, mKeyFrame)
    {
        prop->setTime(prop->time()+delta);
    }
}

void AnimationItem::setEndTime(int endTime)
{
    mEndTime = endTime;
}

AnimationProperty* AnimationItem::newFrame(int time, QGraphicsItem::GraphicsItemChange change, const QVariant& value)
{
    if (time < mStartTime || time >= mEndTime)
    {
        qDebug() << "Class - AnimationItem Func - addFrame :: Time used to add frame is invalid";
        return 0;
    }
    
    // Check if the frame at that time exist and then get it
    AnimationProperty* animationProperty = 0;
    foreach (AnimationProperty* property , mKeyFrame)
    {
        if (property->time() == time)
        {
            qDebug() << "Class - AnimationItem Func - addFrame :: Use exist Property";
            animationProperty = property;
        }
    }
    if (animationProperty == 0)
    {
        qDebug() << "Class - AnimationItem Func - addFrame :: Creating New Property";
        animationProperty = new AnimationProperty();
        animationProperty->setTime(time);
        //addProperty(animationProperty);
    }
    
    if (change == MyAbstractGraphicsObject::ItemPositionHasChanged)
    {
        animationProperty->setPosition(value.toPointF());
    }
    if (change == MyAbstractGraphicsObject::ItemRotationHasChanged)
    {
        animationProperty->setRotation(value.toDouble());
    }
    if (change == MyAbstractGraphicsObject::ItemScaleHasChanged)
    {
        animationProperty->setScale(value.toDouble());
    }
    
    return animationProperty;
}

void AnimationItem::addFrame(int time, QGraphicsItem::GraphicsItemChange change, const QVariant& value)
{
    addProperty(newFrame(time, change, value));
}

void AnimationItem::addProperty(AnimationProperty* property)
{
    int i=0;
    for (i=0; i<mKeyFrame.size(); i++)
    {
        AnimationProperty* ap = mKeyFrame.at(i);
        
        if (ap->time() == property->time())
        {
            return;
        }
        
        // Find the first property that its time is later than
        // the new one. If can't find append at the tail.
        if (ap->time() > property->time())
        {
            break;
        }
    }
    mKeyFrame.insert(i, property);
 }

bool AnimationItem::removeProperty(AnimationProperty* property)
{
    return mKeyFrame.removeOne(property);
}

